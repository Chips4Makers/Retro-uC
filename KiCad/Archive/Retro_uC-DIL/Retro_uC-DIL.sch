EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Retro_uC
LIBS:Retro_uC-DIL-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "jeu. 02 avril 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_1 P3
U 1 1 5A0EC201
P 10800 650
F 0 "P3" H 10880 650 40  0000 L CNN
F 1 "CONN_1" H 10800 705 30  0001 C CNN
F 2 "Retro_uC:MOUNTHOLE" H 10800 650 60  0001 C CNN
F 3 "" H 10800 650 60  0000 C CNN
	1    10800 650 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P4
U 1 1 5A0EC202
P 10900 650
F 0 "P4" H 10980 650 40  0000 L CNN
F 1 "CONN_1" H 10900 705 30  0001 C CNN
F 2 "Retro_uC:MOUNTHOLE" H 10900 650 60  0001 C CNN
F 3 "" H 10900 650 60  0000 C CNN
	1    10900 650 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P5
U 1 1 5A0EC203
P 11000 650
F 0 "P5" H 11080 650 40  0000 L CNN
F 1 "CONN_1" H 11000 705 30  0001 C CNN
F 2 "Retro_uC:MOUNTHOLE" H 11000 650 60  0001 C CNN
F 3 "" H 11000 650 60  0000 C CNN
	1    11000 650 
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P6
U 1 1 5A0EC204
P 11100 650
F 0 "P6" H 11180 650 40  0000 L CNN
F 1 "CONN_1" H 11100 705 30  0001 C CNN
F 2 "Retro_uC:MOUNTHOLE" H 11100 650 60  0001 C CNN
F 3 "" H 11100 650 60  0000 C CNN
	1    11100 650 
	0    -1   -1   0   
$EndComp
$Comp
L VIAS_01X15_TOPPOW P1
U 1 1 5A0EC205
P 9700 2000
F 0 "P1" V 9670 2050 60  0000 C CNN
F 1 "Left" V 9780 2050 60  0000 C CNN
F 2 "Socket_Arduino_Nano:Socket_Strip_Arduino_1x15" H 9700 2050 60  0001 C CNN
F 3 "" H 9700 2050 60  0000 C CNN
	1    9700 2000
	1    0    0    -1  
$EndComp
$Comp
L VIAS_01X15_TOPPOW P2
U 1 1 5A0EC206
P 10075 2000
F 0 "P2" V 10045 2050 60  0000 C CNN
F 1 "Right" V 10155 2050 60  0000 C CNN
F 2 "Socket_Arduino_Nano:Socket_Strip_Arduino_1x15" H 10075 2050 60  0001 C CNN
F 3 "" H 10075 2050 60  0000 C CNN
	1    10075 2000
	-1   0    0    -1  
$EndComp
NoConn ~ 10800 800 
NoConn ~ 10900 800 
NoConn ~ 11000 800 
NoConn ~ 11100 800 
Text Label 8800 1600 0    60   ~ 0
IO2
Text Label 8800 1500 0    60   ~ 0
IO1
Text Label 8800 1400 0    60   ~ 0
IO0
Text Label 8800 1700 0    60   ~ 0
IO3
Text Label 8800 1800 0    60   ~ 0
IO4
Text Label 8800 1900 0    60   ~ 0
IO5
Text Label 8800 2000 0    60   ~ 0
IO6
Text Label 8800 2100 0    60   ~ 0
IO7
Text Label 8800 2200 0    60   ~ 0
IO8
Text Label 8800 2300 0    60   ~ 0
IO9
Text Label 8800 2400 0    60   ~ 0
IO10
Text Label 8800 2500 0    60   ~ 0
IO11
Text Label 8800 2600 0    60   ~ 0
IO12
Text Label 8800 2700 0    60   ~ 0
IO13
Text Label 10675 2700 0    60   ~ 0
IO18
Text Label 10675 2600 0    60   ~ 0
IO19
Text Label 10675 2500 0    60   ~ 0
IO20
Text Label 10675 2400 0    60   ~ 0
IO21
Text Label 10675 2300 0    60   ~ 0
IO22
Text Label 10675 2200 0    60   ~ 0
IO23
Text Label 10675 2100 0    60   ~ 0
IO24
Text Label 10675 2000 0    60   ~ 0
IO25
Text Label 10675 1900 0    60   ~ 0
IO26
Text Notes 10850 950  0    60   ~ 0
Holes
$Comp
L +5V #PWR01
U 1 1 5A0EC207
P 10350 1125
F 0 "#PWR01" H 10350 975 50  0001 C CNN
F 1 "+5V" H 10350 1265 50  0000 C CNN
F 2 "" H 10350 1125 50  0000 C CNN
F 3 "" H 10350 1125 50  0000 C CNN
	1    10350 1125
	1    0    0    -1  
$EndComp
Text Label 10675 1500 0    60   ~ 0
IO30
Text Label 10675 1600 0    60   ~ 0
IO29
Text Label 10675 1700 0    60   ~ 0
IO28
Text Label 10675 1800 0    60   ~ 0
IO27
Text Notes 3475 1775 0    60   ~ 0
Bare die to be wirebonded on the PCB
$Comp
L LD1117S33TR U1
U 1 1 5A0EC215
P 7125 3900
F 0 "U1" H 6825 4150 50  0000 C CNN
F 1 "LD1117S33TR" H 7125 4100 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 7125 4000 50  0000 C CIN
F 3 "" H 7125 3900 50  0000 C CNN
	1    7125 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5A0EC216
P 7125 4425
F 0 "#PWR02" H 7125 4175 50  0001 C CNN
F 1 "GND" H 7125 4275 50  0000 C CNN
F 2 "" H 7125 4425 50  0000 C CNN
F 3 "" H 7125 4425 50  0000 C CNN
	1    7125 4425
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 5A0EC217
P 6425 3825
F 0 "#PWR03" H 6425 3675 50  0001 C CNN
F 1 "+5V" H 6425 3965 50  0000 C CNN
F 2 "" H 6425 3825 50  0000 C CNN
F 3 "" H 6425 3825 50  0000 C CNN
	1    6425 3825
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR04
U 1 1 5A0EC218
P 7875 3825
F 0 "#PWR04" H 7875 3675 50  0001 C CNN
F 1 "+3.3V" H 7875 3965 50  0000 C CNN
F 2 "" H 7875 3825 50  0000 C CNN
F 3 "" H 7875 3825 50  0000 C CNN
	1    7875 3825
	1    0    0    -1  
$EndComp
$Comp
L Retro_uC-mini DIE1
U 1 1 5A0EE51A
P 4475 3525
F 0 "DIE1" H 4475 3450 60  0000 C CNN
F 1 "Retro_uC-mini" H 4475 3575 60  0000 C CNN
F 2 "Retro_uC:COBWB_48_60_90" H 4800 3525 60  0000 C CNN
F 3 "" H 4800 3525 60  0000 C CNN
	1    4475 3525
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5A0F0235
P 4475 5150
F 0 "#PWR05" H 4475 4900 50  0001 C CNN
F 1 "GND" H 4475 5000 50  0000 C CNN
F 2 "" H 4475 5150 50  0000 C CNN
F 3 "" H 4475 5150 50  0000 C CNN
	1    4475 5150
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 5A0F03C3
P 4325 5150
F 0 "C1" H 4335 5220 50  0000 L CNN
F 1 "0.1uF" H 4335 5070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4325 5150 50  0001 C CNN
F 3 "" H 4325 5150 50  0000 C CNN
F 4 "CL10B104KB8NNNL" H 4325 5150 60  0001 C CNN "PartNumber"
	1    4325 5150
	0    1    1    0   
$EndComp
$Comp
L C_Small C4
U 1 1 5A0F04DC
P 4625 5150
F 0 "C4" H 4635 5220 50  0000 L CNN
F 1 "0.1uF" H 4635 5070 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4625 5150 50  0001 C CNN
F 3 "" H 4625 5150 50  0000 C CNN
F 4 "CL10B104KB8NNNL" H 4625 5150 60  0001 C CNN "PartNumber"
	1    4625 5150
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR06
U 1 1 5A0F05E8
P 4225 5000
F 0 "#PWR06" H 4225 4850 50  0001 C CNN
F 1 "+5V" H 4225 5140 50  0000 C CNN
F 2 "" H 4225 5000 50  0000 C CNN
F 3 "" H 4225 5000 50  0000 C CNN
	1    4225 5000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR07
U 1 1 5A0F0620
P 4725 5000
F 0 "#PWR07" H 4725 4850 50  0001 C CNN
F 1 "+3.3V" H 4725 5140 50  0000 C CNN
F 2 "" H 4725 5000 50  0000 C CNN
F 3 "" H 4725 5000 50  0000 C CNN
	1    4725 5000
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 5A0F0B7A
P 4375 2075
F 0 "C2" H 4385 2145 50  0000 L CNN
F 1 "0.1uF" H 4385 1995 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4375 2075 50  0001 C CNN
F 3 "" H 4375 2075 50  0000 C CNN
F 4 "CL10B104KB8NNNL" H 4375 2075 60  0001 C CNN "PartNumber"
	1    4375 2075
	0    1    1    0   
$EndComp
$Comp
L C_Small C3
U 1 1 5A0F0DFC
P 4575 2075
F 0 "C3" H 4585 2145 50  0000 L CNN
F 1 "0.1uF" H 4585 1995 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4575 2075 50  0001 C CNN
F 3 "" H 4575 2075 50  0000 C CNN
F 4 "CL10B104KB8NNNL" H 4575 2075 60  0001 C CNN "PartNumber"
	1    4575 2075
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR08
U 1 1 5A0F0F16
P 4475 2275
F 0 "#PWR08" H 4475 2025 50  0001 C CNN
F 1 "GND" H 4475 2125 50  0000 C CNN
F 2 "" H 4475 2275 50  0000 C CNN
F 3 "" H 4475 2275 50  0000 C CNN
	1    4475 2275
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 5A0F1172
P 3125 3375
F 0 "#PWR09" H 3125 3225 50  0001 C CNN
F 1 "+5V" H 3125 3515 50  0000 C CNN
F 2 "" H 3125 3375 50  0000 C CNN
F 3 "" H 3125 3375 50  0000 C CNN
	1    3125 3375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5A0F1330
P 3125 3475
F 0 "#PWR010" H 3125 3225 50  0001 C CNN
F 1 "GND" H 3125 3325 50  0000 C CNN
F 2 "" H 3125 3475 50  0000 C CNN
F 3 "" H 3125 3475 50  0000 C CNN
	1    3125 3475
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR011
U 1 1 5A0F1457
P 2975 3675
F 0 "#PWR011" H 2975 3525 50  0001 C CNN
F 1 "+3.3V" H 2975 3815 50  0000 C CNN
F 2 "" H 2975 3675 50  0000 C CNN
F 3 "" H 2975 3675 50  0000 C CNN
	1    2975 3675
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR012
U 1 1 5A0F2711
P 4275 2025
F 0 "#PWR012" H 4275 1875 50  0001 C CNN
F 1 "+5V" H 4275 2165 50  0000 C CNN
F 2 "" H 4275 2025 50  0000 C CNN
F 3 "" H 4275 2025 50  0000 C CNN
	1    4275 2025
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR013
U 1 1 5A0F2770
P 4675 2025
F 0 "#PWR013" H 4675 1875 50  0001 C CNN
F 1 "+3.3V" H 4675 2165 50  0000 C CNN
F 2 "" H 4675 2025 50  0000 C CNN
F 3 "" H 4675 2025 50  0000 C CNN
	1    4675 2025
	1    0    0    -1  
$EndComp
$Comp
L C_Small C6
U 1 1 5A0F27FC
P 7875 4200
F 0 "C6" H 7885 4270 50  0000 L CNN
F 1 "10uF" H 7885 4120 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 7875 4200 50  0000 C CNN
F 3 "" H 7875 4200 50  0000 C CNN
F 4 "CL31B106KBHNNNE" H 7875 4200 60  0001 C CNN "PartNumber"
	1    7875 4200
	1    0    0    -1  
$EndComp
$Comp
L C_Small C5
U 1 1 5A0F2C06
P 6425 4275
F 0 "C5" H 6435 4345 50  0000 L CNN
F 1 "0.1uF" H 6435 4195 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206" H 6425 4275 50  0001 C CNN
F 3 "" H 6425 4275 50  0000 C CNN
F 4 "CL31B104KBCNNNC" H 6425 4275 60  0001 C CNN "PartNumber"
	1    6425 4275
	1    0    0    -1  
$EndComp
$Comp
L IKH0404000 DIP1
U 1 1 5A0F2F13
P 4475 6200
F 0 "DIP1" H 4475 6150 60  0000 C CNN
F 1 "IKH0404000" H 4475 6250 60  0000 C CNN
F 2 "Retro_uC:IKH0404000" H 4450 6175 60  0000 C CNN
F 3 "" H 4450 6175 60  0000 C CNN
	1    4475 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8775 2700 9500 2700
Wire Wire Line
	8775 2600 9500 2600
Wire Wire Line
	8775 2500 9500 2500
Wire Wire Line
	8775 2400 9500 2400
Wire Wire Line
	8775 2300 9500 2300
Wire Wire Line
	8775 2200 9500 2200
Wire Wire Line
	8775 2100 9500 2100
Wire Wire Line
	8775 2000 9500 2000
Wire Wire Line
	8775 1900 9500 1900
Wire Wire Line
	8775 1800 9500 1800
Wire Wire Line
	8775 1700 9500 1700
Wire Wire Line
	8775 1500 9500 1500
Wire Wire Line
	8775 1400 9500 1400
Wire Wire Line
	8600 1300 9500 1300
Wire Wire Line
	8775 1600 9500 1600
Wire Wire Line
	10350 1125 10350 1300
Wire Wire Line
	10350 1300 10275 1300
Wire Wire Line
	10275 2600 11050 2600
Wire Wire Line
	10275 1600 11050 1600
Wire Notes Line
	10700 475  10700 975 
Wire Notes Line
	10700 975  11225 975 
Wire Wire Line
	10275 1400 11050 1400
Wire Notes Line
	11225 3000 8525 3000
Wire Wire Line
	10275 1500 11050 1500
Wire Wire Line
	10275 1700 11050 1700
Wire Wire Line
	10275 1800 11050 1800
Wire Wire Line
	10275 1900 11050 1900
Wire Wire Line
	10275 2000 11050 2000
Wire Wire Line
	10275 2100 11050 2100
Wire Wire Line
	10275 2200 11050 2200
Wire Wire Line
	10275 2300 11050 2300
Wire Wire Line
	10275 2400 11050 2400
Wire Wire Line
	10275 2500 11050 2500
Wire Wire Line
	10275 2700 11050 2700
Wire Notes Line
	8550 3000 8475 3000
Wire Notes Line
	8450 3000 8450 475 
Wire Wire Line
	7125 4150 7125 4425
Connection ~ 7125 4425
Wire Wire Line
	6425 3850 6725 3850
Connection ~ 6425 3850
Wire Wire Line
	7525 3850 7875 3850
Connection ~ 7875 3850
Wire Wire Line
	3925 2525 3925 2275
Wire Wire Line
	4025 2275 4025 2525
Wire Wire Line
	4125 2525 4125 2275
Wire Wire Line
	4225 2275 4225 2525
Wire Wire Line
	4325 2225 4325 2525
Wire Wire Line
	4425 2275 4425 2525
Wire Wire Line
	4525 2275 4525 2525
Wire Wire Line
	4625 2225 4625 2525
Wire Wire Line
	4725 2525 4725 2275
Wire Wire Line
	4825 2275 4825 2525
Wire Wire Line
	4925 2525 4925 2275
Wire Wire Line
	5025 2275 5025 2525
Wire Wire Line
	5450 2975 5725 2975
Wire Wire Line
	5725 3075 5450 3075
Wire Wire Line
	5450 3175 5725 3175
Wire Wire Line
	5725 3275 5450 3275
Wire Wire Line
	5450 3375 5825 3375
Wire Wire Line
	5450 3475 5825 3475
Wire Wire Line
	5450 3575 5725 3575
Wire Wire Line
	5450 3675 5975 3675
Wire Wire Line
	5450 3775 5725 3775
Wire Wire Line
	5725 3875 5450 3875
Wire Wire Line
	5450 3975 5725 3975
Wire Wire Line
	5725 4075 5450 4075
Wire Wire Line
	5025 4500 5025 4775
Wire Wire Line
	4925 4775 4925 4500
Wire Wire Line
	4825 4500 4825 4775
Wire Wire Line
	4725 4775 4725 4500
Wire Wire Line
	4625 4500 4625 5000
Wire Wire Line
	4525 5150 4525 4500
Wire Wire Line
	4425 4500 4425 5150
Wire Wire Line
	4325 4500 4325 5000
Wire Wire Line
	4225 4500 4225 4775
Wire Wire Line
	4125 4775 4125 4500
Wire Wire Line
	4025 4500 4025 4775
Wire Wire Line
	3925 4775 3925 4500
Wire Wire Line
	3500 4075 3225 4075
Wire Wire Line
	3225 3975 3500 3975
Wire Wire Line
	3500 3875 3225 3875
Wire Wire Line
	3225 3775 3500 3775
Wire Wire Line
	2975 3675 3500 3675
Wire Wire Line
	3225 3575 3500 3575
Wire Wire Line
	3125 3475 3500 3475
Wire Wire Line
	3125 3375 3500 3375
Wire Wire Line
	3500 3275 3225 3275
Wire Wire Line
	3225 3175 3500 3175
Wire Wire Line
	3500 3075 3225 3075
Wire Wire Line
	3225 2975 3500 2975
Wire Wire Line
	4425 5150 4525 5150
Connection ~ 4475 5150
Wire Wire Line
	4325 5000 4225 5000
Wire Wire Line
	4225 5000 4225 5150
Wire Wire Line
	4625 5000 4725 5000
Wire Wire Line
	4725 5000 4725 5150
Wire Wire Line
	4425 2275 4525 2275
Wire Wire Line
	4475 2075 4475 2275
Connection ~ 4475 2275
Wire Wire Line
	4275 2025 4275 2225
Wire Wire Line
	4275 2225 4325 2225
Wire Wire Line
	4675 2025 4675 2225
Wire Wire Line
	4675 2225 4625 2225
Connection ~ 4275 2075
Connection ~ 4675 2075
Wire Wire Line
	3225 3475 3225 3575
Connection ~ 3225 3475
Wire Wire Line
	6425 4425 7875 4425
Wire Wire Line
	7875 3825 7875 4100
Wire Wire Line
	7875 4425 7875 4300
Wire Wire Line
	6425 3825 6425 4175
Wire Wire Line
	6425 4375 6425 4425
Wire Wire Line
	4325 5775 4325 5500
Wire Wire Line
	4425 5500 4425 5775
Wire Wire Line
	4525 5775 4525 5500
Wire Wire Line
	4625 5500 4625 5775
Wire Wire Line
	4325 6625 4750 6625
Connection ~ 4425 6625
Connection ~ 4525 6625
Connection ~ 4625 6625
$Comp
L GND #PWR014
U 1 1 5A0F3652
P 4750 6625
F 0 "#PWR014" H 4750 6375 50  0001 C CNN
F 1 "GND" H 4750 6475 50  0000 C CNN
F 2 "" H 4750 6625 50  0000 C CNN
F 3 "" H 4750 6625 50  0000 C CNN
	1    4750 6625
	1    0    0    -1  
$EndComp
Text Label 4325 5500 3    60   ~ 0
IO14
Text Label 4425 5500 3    60   ~ 0
IO15
Text Label 4525 5500 3    60   ~ 0
IO16
Text Label 4625 5500 3    60   ~ 0
IO17
$Comp
L GND #PWR015
U 1 1 5A0F3752
P 8600 1300
F 0 "#PWR015" H 8600 1050 50  0001 C CNN
F 1 "GND" H 8600 1150 50  0000 C CNN
F 2 "" H 8600 1300 50  0000 C CNN
F 3 "" H 8600 1300 50  0000 C CNN
	1    8600 1300
	1    0    0    -1  
$EndComp
Text Label 10675 1400 0    60   ~ 0
IO31
Text Label 4225 2275 3    60   ~ 0
IO0
Text Label 4125 2275 3    60   ~ 0
IO1
Text Label 4025 2275 3    60   ~ 0
IO2
Text Label 3925 2275 3    60   ~ 0
IO3
Text Label 3225 2975 0    60   ~ 0
IO4
Text Label 3225 3075 0    60   ~ 0
IO5
Text Label 3225 3175 0    60   ~ 0
IO6
Text Label 3225 3275 0    60   ~ 0
IO7
Text Label 3225 3775 0    60   ~ 0
IO8
Text Label 3225 3875 0    60   ~ 0
IO9
Text Label 3225 3975 0    60   ~ 0
IO10
Text Label 3225 4075 0    60   ~ 0
IO11
Text Label 3925 4775 1    60   ~ 0
IO12
Text Label 4025 4775 1    60   ~ 0
IO13
Text Label 4125 4775 1    60   ~ 0
IO14
Text Label 4225 4775 1    60   ~ 0
IO15
Text Label 4725 4775 1    60   ~ 0
IO16
Text Label 4825 4775 1    60   ~ 0
IO17
Text Label 4925 4775 1    60   ~ 0
IO18
Text Label 5025 4775 1    60   ~ 0
IO19
Text Label 5725 4075 2    60   ~ 0
IO20
Text Label 5725 3975 2    60   ~ 0
IO21
Text Label 5725 3875 2    60   ~ 0
IO22
Text Label 5725 3775 2    60   ~ 0
IO23
Text Label 5725 3275 2    60   ~ 0
IO24
Text Label 5725 3175 2    60   ~ 0
IO25
Text Label 5725 3075 2    60   ~ 0
IO26
Text Label 5725 2975 2    60   ~ 0
IO27
Text Label 5025 2275 3    60   ~ 0
IO28
Text Label 4925 2275 3    60   ~ 0
IO29
Text Label 4825 2275 3    60   ~ 0
IO30
Text Label 4725 2275 3    60   ~ 0
IO31
$Comp
L +5V #PWR016
U 1 1 5A0F4220
P 5825 3375
F 0 "#PWR016" H 5825 3225 50  0001 C CNN
F 1 "+5V" H 5825 3515 50  0000 C CNN
F 2 "" H 5825 3375 50  0000 C CNN
F 3 "" H 5825 3375 50  0000 C CNN
	1    5825 3375
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 5A0F4285
P 5825 3475
F 0 "#PWR017" H 5825 3225 50  0001 C CNN
F 1 "GND" H 5825 3325 50  0000 C CNN
F 2 "" H 5825 3475 50  0000 C CNN
F 3 "" H 5825 3475 50  0000 C CNN
	1    5825 3475
	1    0    0    -1  
$EndComp
Wire Wire Line
	5725 3575 5725 3475
Connection ~ 5725 3475
$Comp
L +3.3V #PWR018
U 1 1 5A0F445B
P 5975 3675
F 0 "#PWR018" H 5975 3525 50  0001 C CNN
F 1 "+3.3V" H 5975 3815 50  0000 C CNN
F 2 "" H 5975 3675 50  0000 C CNN
F 3 "" H 5975 3675 50  0000 C CNN
	1    5975 3675
	1    0    0    -1  
$EndComp
$Comp
L Fiducial F1
U 1 1 5A10539C
P 8725 650
F 0 "F1" H 8725 600 60  0000 C CNN
F 1 "Fiducial" H 8725 675 60  0000 C CNN
F 2 "Retro_uC:Fiducial" H 8725 600 60  0000 C CNN
F 3 "" H 8725 600 60  0000 C CNN
	1    8725 650 
	1    0    0    -1  
$EndComp
$Comp
L Fiducial F2
U 1 1 5A105405
P 9125 650
F 0 "F2" H 9125 600 60  0000 C CNN
F 1 "Fiducial" H 9125 675 60  0000 C CNN
F 2 "Retro_uC:Fiducial" H 9125 600 60  0000 C CNN
F 3 "" H 9125 600 60  0000 C CNN
	1    9125 650 
	1    0    0    -1  
$EndComp
$Comp
L Fiducial F3
U 1 1 5A10543C
P 9600 650
F 0 "F3" H 9600 600 60  0000 C CNN
F 1 "Fiducial" H 9600 675 60  0000 C CNN
F 2 "Retro_uC:Fiducial" H 9600 600 60  0000 C CNN
F 3 "" H 9600 600 60  0000 C CNN
	1    9600 650 
	1    0    0    -1  
$EndComp
$EndSCHEMATC
