EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Retro_uC
LIBS:C4M
LIBS:Retro_uC-Breadboard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x20 J1
U 1 1 5A5E1F5B
P 1100 3450
F 0 "J1" H 1100 4450 50  0000 C CNN
F 1 "Conn_01x20" H 1100 2350 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x20" H 1100 3450 50  0001 C CNN
F 3 "" H 1100 3450 50  0001 C CNN
F 4 "M20-9992046" H 1100 3450 60  0001 C CNN "MPN"
	1    1100 3450
	-1   0    0    1   
$EndComp
$Comp
L VIAS_01X20_POWBB J2
U 1 1 5A5E209F
P 7925 4275
F 0 "J2" H 7925 5275 50  0000 C CNN
F 1 "VIAS_01X20_POWBB" H 7925 3175 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x20" H 7925 4275 50  0001 C CNN
F 3 "" H 7925 4275 50  0001 C CNN
F 4 "M20-9992046" H 7925 4275 60  0001 C CNN "MPN"
	1    7925 4275
	1    0    0    1   
$EndComp
$Comp
L Retro_uC-QFN100 RUC1
U 1 1 5A5E21CF
P 2775 3450
F 0 "RUC1" H 4875 2325 60  0000 R CNN
F 1 "Retro_uC-QFN100" H 5125 2200 60  0000 R CNN
F 2 "Retro_uC:QFN_100_12x12_0.4" H 2775 3450 60  0001 C CNN
F 3 "" H 2775 3450 60  0001 C CNN
F 4 "C4M2018RUC" H 2775 3450 60  0001 C CNN "MPN"
	1    2775 3450
	1    0    0    -1  
$EndComp
$Comp
L IKH0404000 DIP1
U 1 1 5A5E2279
P 3475 7150
F 0 "DIP1" H 3475 7100 60  0000 C CNN
F 1 "DIP switch 4 SPST" H 3475 7200 60  0000 C CNN
F 2 "Housings_SOIC:SO-8_5.3x6.2mm_Pitch1.27mm" H 3450 7125 60  0001 C CNN
F 3 "" H 3450 7125 60  0000 C CNN
F 4 "218-4LPST" H 3475 7150 60  0001 C CNN "MPN"
	1    3475 7150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 7000 2675 7000
Wire Wire Line
	2675 7000 2675 5850
Wire Wire Line
	2675 5850 2775 5850
Wire Wire Line
	2775 5750 2575 5750
Wire Wire Line
	2575 5750 2575 7100
Wire Wire Line
	2575 7100 3050 7100
Wire Wire Line
	3050 7200 2475 7200
Wire Wire Line
	2475 7200 2475 5650
Wire Wire Line
	2475 5650 2775 5650
Wire Wire Line
	2775 5550 2375 5550
Wire Wire Line
	2375 5550 2375 7300
Wire Wire Line
	2375 7300 3050 7300
Wire Wire Line
	3900 7000 4075 7000
Wire Wire Line
	4075 7000 4075 7450
$Comp
L GND #PWR01
U 1 1 5A5E6CF5
P 4075 7450
F 0 "#PWR01" H 4075 7200 50  0001 C CNN
F 1 "GND" H 4075 7300 50  0000 C CNN
F 2 "" H 4075 7450 50  0001 C CNN
F 3 "" H 4075 7450 50  0001 C CNN
	1    4075 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 7300 4075 7300
Connection ~ 4075 7300
Wire Wire Line
	4075 7200 3900 7200
Connection ~ 4075 7200
Wire Wire Line
	3900 7100 4075 7100
Connection ~ 4075 7100
Wire Wire Line
	7725 5175 7675 5175
$Comp
L GND #PWR02
U 1 1 5A5E70B2
P 7675 5175
F 0 "#PWR02" H 7675 4925 50  0001 C CNN
F 1 "GND" H 7675 5025 50  0000 C CNN
F 2 "" H 7675 5175 50  0001 C CNN
F 3 "" H 7675 5175 50  0001 C CNN
	1    7675 5175
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 5A5E70D2
P 7500 5125
F 0 "#PWR03" H 7500 4975 50  0001 C CNN
F 1 "+5V" H 7500 5265 50  0000 C CNN
F 2 "" H 7500 5125 50  0001 C CNN
F 3 "" H 7500 5125 50  0001 C CNN
	1    7500 5125
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR04
U 1 1 5A5E70F2
P 7600 5025
F 0 "#PWR04" H 7600 4875 50  0001 C CNN
F 1 "+3V3" H 7600 5100 50  0000 C CNN
F 2 "" H 7600 5025 50  0001 C CNN
F 3 "" H 7600 5025 50  0001 C CNN
	1    7600 5025
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 5025 7725 5025
Wire Wire Line
	7725 5025 7725 4975
Wire Wire Line
	7725 5075 7725 5125
Wire Wire Line
	7725 5125 7500 5125
$Comp
L GND #PWR05
U 1 1 5A5E72A5
P 6950 4650
F 0 "#PWR05" H 6950 4400 50  0001 C CNN
F 1 "GND" H 6950 4550 50  0000 C CNN
F 2 "" H 6950 4650 50  0001 C CNN
F 3 "" H 6950 4650 50  0001 C CNN
	1    6950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4650 6775 4650
Wire Wire Line
	6775 4550 6950 4550
Wire Wire Line
	6950 4550 6950 4650
$Comp
L +3V3 #PWR06
U 1 1 5A5E7309
P 6950 4500
F 0 "#PWR06" H 6950 4350 50  0001 C CNN
F 1 "+3V3" H 6950 4575 50  0000 C CNN
F 2 "" H 6950 4500 50  0001 C CNN
F 3 "" H 6950 4500 50  0001 C CNN
	1    6950 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4500 6775 4500
Wire Wire Line
	6775 4500 6775 4450
$Comp
L +5V #PWR07
U 1 1 5A5E734A
P 7050 4800
F 0 "#PWR07" H 7050 4650 50  0001 C CNN
F 1 "+5V" H 7050 4940 50  0000 C CNN
F 2 "" H 7050 4800 50  0001 C CNN
F 3 "" H 7050 4800 50  0001 C CNN
	1    7050 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4800 6825 4800
Wire Wire Line
	6825 4800 6825 4750
Wire Wire Line
	6825 4750 6775 4750
$Comp
L GND #PWR08
U 1 1 5A5E73CA
P 4725 6725
F 0 "#PWR08" H 4725 6475 50  0001 C CNN
F 1 "GND" H 4725 6575 50  0000 C CNN
F 2 "" H 4725 6725 50  0001 C CNN
F 3 "" H 4725 6725 50  0001 C CNN
	1    4725 6725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4675 6725 4775 6725
Wire Wire Line
	4675 6725 4675 6650
Wire Wire Line
	4775 6725 4775 6650
Connection ~ 4725 6725
$Comp
L +3V3 #PWR09
U 1 1 5A5E743F
P 4525 6900
F 0 "#PWR09" H 4525 6750 50  0001 C CNN
F 1 "+3V3" H 4525 7040 50  0000 C CNN
F 2 "" H 4525 6900 50  0001 C CNN
F 3 "" H 4525 6900 50  0001 C CNN
	1    4525 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 6900 4575 6900
Wire Wire Line
	4575 6900 4575 6650
$Comp
L +5V #PWR010
U 1 1 5A5E7496
P 4925 6900
F 0 "#PWR010" H 4925 6750 50  0001 C CNN
F 1 "+5V" H 4925 7040 50  0000 C CNN
F 2 "" H 4925 6900 50  0001 C CNN
F 3 "" H 4925 6900 50  0001 C CNN
	1    4925 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 6900 4875 6900
Wire Wire Line
	4875 6900 4875 6650
$Comp
L GND #PWR011
U 1 1 5A5E750D
P 2700 4650
F 0 "#PWR011" H 2700 4400 50  0001 C CNN
F 1 "GND" H 2700 4600 50  0000 C CNN
F 2 "" H 2700 4650 50  0001 C CNN
F 3 "" H 2700 4650 50  0001 C CNN
	1    2700 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 4650 2700 4550
Wire Wire Line
	2700 4550 2775 4550
Wire Wire Line
	2775 4650 2700 4650
$Comp
L +3V3 #PWR012
U 1 1 5A5E75AD
P 2575 4500
F 0 "#PWR012" H 2575 4350 50  0001 C CNN
F 1 "+3V3" H 2575 4600 50  0000 C CNN
F 2 "" H 2575 4500 50  0001 C CNN
F 3 "" H 2575 4500 50  0001 C CNN
	1    2575 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2575 4500 2775 4500
Wire Wire Line
	2775 4500 2775 4450
$Comp
L +5V #PWR013
U 1 1 5A5E761D
P 2575 4775
F 0 "#PWR013" H 2575 4625 50  0001 C CNN
F 1 "+5V" H 2575 4915 50  0000 C CNN
F 2 "" H 2575 4775 50  0001 C CNN
F 3 "" H 2575 4775 50  0001 C CNN
	1    2575 4775
	1    0    0    -1  
$EndComp
Wire Wire Line
	2575 4775 2775 4775
Wire Wire Line
	2775 4775 2775 4750
$Comp
L GND #PWR014
U 1 1 5A5E76A7
P 4725 2500
F 0 "#PWR014" H 4725 2250 50  0001 C CNN
F 1 "GND" H 4725 2400 50  0000 C CNN
F 2 "" H 4725 2500 50  0001 C CNN
F 3 "" H 4725 2500 50  0001 C CNN
	1    4725 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4675 2500 4775 2500
Wire Wire Line
	4675 2500 4675 2650
Wire Wire Line
	4775 2500 4775 2650
Connection ~ 4725 2500
$Comp
L +3V3 #PWR015
U 1 1 5A5E775A
P 4575 2500
F 0 "#PWR015" H 4575 2350 50  0001 C CNN
F 1 "+3V3" H 4575 2640 50  0000 C CNN
F 2 "" H 4575 2500 50  0001 C CNN
F 3 "" H 4575 2500 50  0001 C CNN
	1    4575 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4575 2500 4575 2650
$Comp
L +5V #PWR016
U 1 1 5A5E77B6
P 4875 2500
F 0 "#PWR016" H 4875 2350 50  0001 C CNN
F 1 "+5V" H 4875 2640 50  0000 C CNN
F 2 "" H 4875 2500 50  0001 C CNN
F 3 "" H 4875 2500 50  0001 C CNN
	1    4875 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 2500 4875 2650
Wire Wire Line
	2775 5450 1300 5450
Wire Wire Line
	1300 5450 1300 4350
Wire Wire Line
	1300 4250 1400 4250
Wire Wire Line
	1400 4250 1400 5350
Wire Wire Line
	1400 5350 2775 5350
Wire Wire Line
	2775 5250 1500 5250
Wire Wire Line
	1500 5250 1500 4150
Wire Wire Line
	1500 4150 1300 4150
Wire Wire Line
	2775 5150 1600 5150
Wire Wire Line
	1600 5150 1600 4050
Wire Wire Line
	1600 4050 1300 4050
Wire Wire Line
	2775 5050 1700 5050
Wire Wire Line
	1700 5050 1700 3950
Wire Wire Line
	1700 3950 1300 3950
Wire Wire Line
	2775 4950 1800 4950
Wire Wire Line
	1800 4950 1800 3850
Wire Wire Line
	1800 3850 1300 3850
Text Label 1975 5450 0    60   ~ 0
CLK
Text Label 1925 5350 0    60   ~ 0
RESET
Text Label 1950 5250 0    60   ~ 0
TRST
Text Label 1975 5150 0    60   ~ 0
TMS
Text Label 1975 5050 0    60   ~ 0
TCK
Text Label 1975 4950 0    60   ~ 0
TDO
Wire Wire Line
	2775 4850 1900 4850
Wire Wire Line
	1900 4850 1900 3750
Wire Wire Line
	1900 3750 1300 3750
Text Label 2000 4850 0    60   ~ 0
TDI
Wire Wire Line
	2775 4350 2000 4350
Wire Wire Line
	2000 4350 2000 3650
Wire Wire Line
	2000 3650 1300 3650
Wire Wire Line
	2775 4250 2100 4250
Wire Wire Line
	2100 4250 2100 3550
Wire Wire Line
	2100 3550 1300 3550
Wire Wire Line
	2775 4150 2200 4150
Wire Wire Line
	2200 4150 2200 3450
Wire Wire Line
	2200 3450 1300 3450
Wire Wire Line
	2775 4050 2300 4050
Wire Wire Line
	2300 4050 2300 3350
Wire Wire Line
	2300 3350 1300 3350
Wire Wire Line
	2775 3950 2400 3950
Wire Wire Line
	2400 3950 2400 3250
Wire Wire Line
	2400 3250 1300 3250
Wire Wire Line
	2775 3850 2500 3850
Wire Wire Line
	2500 3850 2500 3150
Wire Wire Line
	2500 3150 1300 3150
Wire Wire Line
	2775 3750 2575 3750
Wire Wire Line
	2575 3750 2575 3050
Wire Wire Line
	2575 3050 1300 3050
Wire Wire Line
	2775 3650 2650 3650
Wire Wire Line
	2650 3650 2650 2950
Wire Wire Line
	2650 2950 1300 2950
Wire Wire Line
	2775 3550 2725 3550
Wire Wire Line
	2725 3550 2725 2850
Wire Wire Line
	2725 2850 1300 2850
Wire Wire Line
	2775 3450 2775 2750
Wire Wire Line
	2775 2750 1300 2750
Wire Wire Line
	3575 2650 1300 2650
Wire Wire Line
	1300 2550 3675 2550
Wire Wire Line
	3675 2550 3675 2650
Wire Wire Line
	1300 2450 3775 2450
Wire Wire Line
	3775 2450 3775 2650
$Comp
L Conn_02x10_Odd_Even J3
U 1 1 5A60FF2E
P 4725 1050
F 0 "J3" H 4775 1550 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 4775 450 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x10" H 4725 1050 50  0001 C CNN
F 3 "" H 4725 1050 50  0001 C CNN
F 4 "6-534206-0" H 4725 1050 60  0001 C CNN "MPN"
	1    4725 1050
	1    0    0    1   
$EndComp
Wire Wire Line
	3875 2650 3875 2350
Wire Wire Line
	3875 2350 3500 2350
Wire Wire Line
	3500 2350 3500 550 
Wire Wire Line
	3500 550  4525 550 
Wire Wire Line
	4525 650  3600 650 
Wire Wire Line
	3600 650  3600 2250
Wire Wire Line
	3600 2250 3975 2250
Wire Wire Line
	3975 2250 3975 2650
Wire Wire Line
	4525 750  3700 750 
Wire Wire Line
	3700 750  3700 2150
Wire Wire Line
	3700 2150 4075 2150
Wire Wire Line
	4075 2150 4075 2650
Wire Wire Line
	4175 2650 4175 2050
Wire Wire Line
	4175 2050 3800 2050
Wire Wire Line
	3800 2050 3800 850 
Wire Wire Line
	3800 850  4525 850 
Wire Wire Line
	4525 950  3900 950 
Wire Wire Line
	3900 950  3900 1950
Wire Wire Line
	3900 1950 4275 1950
Wire Wire Line
	4275 1950 4275 2650
Wire Wire Line
	4375 2650 4375 1850
Wire Wire Line
	4375 1850 4000 1850
Wire Wire Line
	4000 1850 4000 1050
Wire Wire Line
	4000 1050 4525 1050
Wire Wire Line
	4525 1150 4100 1150
Wire Wire Line
	4100 1150 4100 1750
Wire Wire Line
	4100 1750 4475 1750
Wire Wire Line
	4475 1750 4475 2650
Wire Wire Line
	4975 2650 4975 1700
Wire Wire Line
	4975 1700 4200 1700
Wire Wire Line
	4200 1700 4200 1250
Wire Wire Line
	4200 1250 4525 1250
Wire Wire Line
	4525 1350 4250 1350
Wire Wire Line
	4250 1350 4250 1650
Wire Wire Line
	4250 1650 5075 1650
Wire Wire Line
	5075 1650 5075 2650
Wire Wire Line
	5175 2650 5175 1600
Wire Wire Line
	5175 1600 4300 1600
Wire Wire Line
	4300 1600 4300 1450
Wire Wire Line
	4300 1450 4525 1450
Text Label 1600 3650 0    60   ~ 0
IO0
Text Label 1600 3550 0    60   ~ 0
IO1
Text Label 1600 3450 0    60   ~ 0
IO2
Text Label 1600 3350 0    60   ~ 0
IO3
Text Label 1600 3250 0    60   ~ 0
IO4
Text Label 1600 3150 0    60   ~ 0
IO5
Text Label 1600 3050 0    60   ~ 0
IO6
Text Label 1600 2950 0    60   ~ 0
IO7
Text Label 1600 2850 0    60   ~ 0
IO8
Text Label 1600 2750 0    60   ~ 0
IO9
Text Label 1600 2650 0    60   ~ 0
IO10
Text Label 1600 2550 0    60   ~ 0
IO11
Text Label 1600 2450 0    60   ~ 0
IO12
Text Label 4250 550  0    60   ~ 0
IO13
Text Label 4250 650  0    60   ~ 0
IO14
Text Label 4250 750  0    60   ~ 0
IO15
Text Label 4250 850  0    60   ~ 0
IO16
Text Label 4250 950  0    60   ~ 0
IO17
Text Label 4250 1050 0    60   ~ 0
IO18
Text Label 4250 1150 0    60   ~ 0
IO19
Text Label 4250 1250 0    60   ~ 0
IO20
Text Label 4250 1350 0    60   ~ 0
IO21
Text Label 4300 1450 0    60   ~ 0
IO22
Wire Wire Line
	5275 550  5275 2650
Wire Wire Line
	5275 550  5025 550 
$Comp
L Conn_02x10_Odd_Even J4
U 1 1 5A610FF8
P 6150 1050
F 0 "J4" H 6200 1550 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 6200 450 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x10" H 6150 1050 50  0001 C CNN
F 3 "" H 6150 1050 50  0001 C CNN
F 4 "6-534206-0" H 6150 1050 60  0001 C CNN "MPN"
	1    6150 1050
	1    0    0    1   
$EndComp
Wire Wire Line
	5025 1450 5375 1450
Wire Wire Line
	5025 1350 5475 1350
Wire Wire Line
	5025 1250 5250 1250
Wire Wire Line
	5250 1150 5025 1150
Wire Wire Line
	5025 1050 5250 1050
Wire Wire Line
	5250 950  5025 950 
Wire Wire Line
	5025 850  5250 850 
Wire Wire Line
	5250 750  5025 750 
Wire Wire Line
	5025 650  5250 650 
Text Label 5250 1450 2    60   ~ 0
IO23
Text Label 5250 1350 2    60   ~ 0
IO24
Text Label 5250 1250 2    60   ~ 0
IO25
Text Label 5250 1150 2    60   ~ 0
IO26
Text Label 5250 1050 2    60   ~ 0
IO27
Text Label 5250 950  2    60   ~ 0
IO28
Text Label 5250 850  2    60   ~ 0
IO29
Text Label 5250 750  2    60   ~ 0
IO30
Text Label 5250 650  2    60   ~ 0
IO31
Text Label 5250 550  2    60   ~ 0
IO32
Wire Wire Line
	4475 6650 4475 6900
Wire Wire Line
	4375 6900 4375 6650
Wire Wire Line
	4275 6650 4275 6900
Wire Wire Line
	4175 6900 4175 6650
Wire Wire Line
	4075 6650 4075 6900
Text Label 4475 6900 1    60   ~ 0
IO31
Text Label 4375 6900 1    60   ~ 0
IO30
Text Label 4275 6900 1    60   ~ 0
IO29
Text Label 4175 6900 1    60   ~ 0
IO28
Text Label 4075 6900 1    60   ~ 0
IO27
Wire Wire Line
	3975 6650 3975 6900
Text Label 3975 6900 1    60   ~ 0
IO26
Wire Wire Line
	3875 6650 3875 6900
Text Label 3875 6900 1    60   ~ 0
IO25
Wire Wire Line
	5375 1450 5375 2650
Wire Wire Line
	5475 1350 5475 2650
Wire Wire Line
	5950 1450 5700 1450
Wire Wire Line
	5700 1350 5950 1350
Wire Wire Line
	5950 1250 5700 1250
Wire Wire Line
	5700 1150 5950 1150
Wire Wire Line
	5950 1050 5700 1050
Wire Wire Line
	5700 950  5950 950 
Wire Wire Line
	5950 850  5700 850 
Wire Wire Line
	5650 750  5950 750 
Wire Wire Line
	5625 650  5950 650 
Wire Wire Line
	5575 550  5950 550 
Text Label 5700 550  0    60   ~ 0
IO33
Text Label 5700 650  0    60   ~ 0
IO34
Text Label 5700 750  0    60   ~ 0
IO35
Text Label 5700 850  0    60   ~ 0
IO36
Text Label 5700 950  0    60   ~ 0
IO37
Text Label 5700 1050 0    60   ~ 0
IO38
Text Label 5700 1150 0    60   ~ 0
IO39
Text Label 5700 1250 0    60   ~ 0
IO40
Text Label 5700 1350 0    60   ~ 0
IO41
Text Label 5700 1450 0    60   ~ 0
IO42
Wire Wire Line
	5575 550  5575 2650
Wire Wire Line
	5675 2650 5675 1550
Wire Wire Line
	5675 1550 5625 1550
Wire Wire Line
	5625 1550 5625 650 
Wire Wire Line
	5775 2650 5775 1525
Wire Wire Line
	5775 1525 5650 1525
Wire Wire Line
	5650 1525 5650 750 
Wire Wire Line
	5875 2650 5875 2400
Wire Wire Line
	5975 2400 5975 2650
Text Label 5975 2400 3    60   ~ 0
IO41
Text Label 5875 2400 3    60   ~ 0
IO42
Wire Wire Line
	6775 3450 7050 3450
Wire Wire Line
	7050 3550 6775 3550
Wire Wire Line
	6775 3650 7050 3650
Wire Wire Line
	7050 3750 6775 3750
Wire Wire Line
	6775 3850 7050 3850
Text Label 7050 3450 2    60   ~ 0
IO40
Wire Wire Line
	4975 6650 4975 6900
Wire Wire Line
	5075 6900 5075 6650
Text Label 4975 6900 1    60   ~ 0
IO37
Text Label 5075 6900 1    60   ~ 0
IO36
Wire Wire Line
	6450 1450 6750 1450
Wire Wire Line
	6750 1350 6450 1350
Wire Wire Line
	6450 1250 6750 1250
Wire Wire Line
	6750 1150 6450 1150
Wire Wire Line
	6450 1050 6750 1050
Wire Wire Line
	6750 950  6450 950 
Wire Wire Line
	6450 850  6750 850 
Wire Wire Line
	6750 750  6450 750 
Wire Wire Line
	6450 650  6750 650 
Wire Wire Line
	6450 550  6750 550 
Text Label 6750 1450 2    60   ~ 0
IO43
Text Label 6750 1350 2    60   ~ 0
IO44
Text Label 6750 1250 2    60   ~ 0
IO45
Text Label 6750 1150 2    60   ~ 0
IO46
Text Label 6750 1050 2    60   ~ 0
IO47
Text Label 6750 950  2    60   ~ 0
IO48
Text Label 6750 850  2    60   ~ 0
IO49
Text Label 6750 750  2    60   ~ 0
IO50
Text Label 6750 650  2    60   ~ 0
IO51
Text Label 6750 550  2    60   ~ 0
IO52
Wire Wire Line
	7725 3275 7450 3275
Wire Wire Line
	7450 3375 7725 3375
Wire Wire Line
	7725 3475 7450 3475
Wire Wire Line
	7450 3575 7725 3575
Wire Wire Line
	7725 3675 7450 3675
Wire Wire Line
	7450 3775 7725 3775
Wire Wire Line
	7725 3875 7450 3875
Wire Wire Line
	7450 3975 7725 3975
Wire Wire Line
	7725 4075 7450 4075
Wire Wire Line
	7450 4175 7725 4175
Wire Wire Line
	7725 4275 7450 4275
Wire Wire Line
	7450 4375 7725 4375
Wire Wire Line
	7725 4475 7450 4475
Wire Wire Line
	7450 4575 7725 4575
Wire Wire Line
	7725 4675 7450 4675
Wire Wire Line
	7450 4775 7725 4775
Text Label 7450 3275 0    60   ~ 0
IO53
Text Label 7450 3375 0    60   ~ 0
IO54
Text Label 7450 3475 0    60   ~ 0
IO55
Text Label 7450 3575 0    60   ~ 0
IO56
Text Label 7450 3675 0    60   ~ 0
IO57
Text Label 7450 3775 0    60   ~ 0
IO58
Text Label 7450 3875 0    60   ~ 0
IO59
Text Label 7450 3975 0    60   ~ 0
IO60
Text Label 7450 4075 0    60   ~ 0
IO61
Text Label 7450 4175 0    60   ~ 0
IO62
Text Label 7450 4275 0    60   ~ 0
IO63
Text Label 7450 4375 0    60   ~ 0
IO64
Text Label 7450 4475 0    60   ~ 0
IO65
Text Label 7450 4575 0    60   ~ 0
IO66
Text Label 7450 4675 0    60   ~ 0
IO67
Text Label 7450 4775 0    60   ~ 0
IO68
Wire Wire Line
	5975 6650 5975 6950
Text Label 5975 6950 1    60   ~ 0
IO69
Wire Wire Line
	6775 5850 7050 5850
Wire Wire Line
	7050 5750 6775 5750
Wire Wire Line
	6775 5650 7050 5650
Wire Wire Line
	7050 5550 6775 5550
Wire Wire Line
	6775 5450 7050 5450
Wire Wire Line
	7050 5350 6775 5350
Wire Wire Line
	6775 5250 7050 5250
Wire Wire Line
	7050 5150 6775 5150
Wire Wire Line
	6775 5050 7050 5050
Wire Wire Line
	7050 4950 6775 4950
Wire Wire Line
	6775 4850 7050 4850
Text Label 7050 5650 2    60   ~ 0
IO68
Text Label 7050 5550 2    60   ~ 0
IO67
Text Label 7050 5250 2    60   ~ 0
IO66
Wire Wire Line
	7725 4875 7450 4875
Text Label 7450 4875 0    60   ~ 0
IO69
Text Label 7050 5450 2    60   ~ 0
IO53
Text Label 7050 5350 2    60   ~ 0
IO54
Text Label 7050 5150 2    60   ~ 0
IO55
Text Label 7050 5050 2    60   ~ 0
IO56
Text Label 7050 4950 2    60   ~ 0
IO65
Text Label 7050 4850 2    60   ~ 0
IO64
Text Label 7050 4350 2    60   ~ 0
IO63
Text Label 7050 4250 2    60   ~ 0
IO62
Wire Wire Line
	6775 3950 7050 3950
Wire Wire Line
	7050 4050 6775 4050
Wire Wire Line
	6775 4150 7050 4150
Wire Wire Line
	7050 4250 6775 4250
Wire Wire Line
	6775 4350 7050 4350
Text Label 7050 3550 2    60   ~ 0
IO43
Text Label 7050 3650 2    60   ~ 0
IO44
Text Label 7050 3750 2    60   ~ 0
IO39
Text Label 7050 3850 2    60   ~ 0
IO38
Text Label 7050 3950 2    60   ~ 0
IO45
Text Label 7050 4050 2    60   ~ 0
IO46
Text Label 7050 4150 2    60   ~ 0
IO47
Wire Wire Line
	5175 6650 5175 6900
Wire Wire Line
	5275 6900 5275 6650
Wire Wire Line
	5375 6650 5375 6900
Wire Wire Line
	5475 6900 5475 6650
Wire Wire Line
	5575 6650 5575 6900
Wire Wire Line
	5675 6900 5675 6650
Wire Wire Line
	5775 6650 5775 6900
Wire Wire Line
	5875 6650 5875 6900
Text Label 5175 6900 1    60   ~ 0
IO48
Text Label 5275 6900 1    60   ~ 0
IO49
Text Label 5375 6900 1    60   ~ 0
IO50
Text Label 5475 6900 1    60   ~ 0
IO51
Text Label 5575 6900 1    60   ~ 0
IO52
Text Label 7050 5850 2    60   ~ 0
IO57
Text Label 7050 5750 2    60   ~ 0
IO58
Text Label 5675 6900 1    60   ~ 0
IO59
Text Label 5775 6900 1    60   ~ 0
IO60
Text Label 5875 6900 1    60   ~ 0
IO61
NoConn ~ 3575 6650
NoConn ~ 3675 6650
NoConn ~ 3775 6650
$EndSCHEMATC
