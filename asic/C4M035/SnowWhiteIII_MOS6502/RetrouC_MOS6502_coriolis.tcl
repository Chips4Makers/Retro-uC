set libertyfile $::env(LIBERTY_FILE)
set top $::env(TOP_CELL)

# Synthesis script for yosys created by qflow
yosys read_liberty -lib -ignore_miss_dir -setattr blackbox $libertyfile

yosys ghdl --std=08 --ieee=synopsys t65
yosys read_ilang RetrouC_MOS6502.il

# High-level synthesis
if {$::env(FLATTEN)} {
    yosys synth -flatten -top RetrouC_MOS6502
} else {
    yosys synth -top RetrouC_MOS6502
}

# Map register flops
yosys dfflibmap -liberty $libertyfile
yosys opt

# Map combinatorial cells, standard script
yosys abc -liberty $libertyfile
yosys opt
yosys clean -purge

yosys write_blif $::env(BLIF_FILE)
yosys stat
