import os
import CRL, Hurricane as Hur, Katana, Etesian, Anabatic, Cfg
from helpers import u, l
from helpers.overlay import Configuration, UpdateSession
from plugins.cts.clocktree import HTree
from plugins.chip.configuration import ChipConf

import setup
setup.init()

import common.technology as Tech

from ioring import IORing

af = CRL.AllianceFramework.get()
env = af.getEnvironment()
print env.getPrint()

cellsTop = os.environ["CELLSDIR"]

env.setCLOCK('clk_0')
env.addSYSTEM_LIBRARY( library=cellsTop+'/nsxlib', mode=CRL.Environment.Prepend )
env.addSYSTEM_LIBRARY( library=cellsTop+'/mpxlib', mode=CRL.Environment.Prepend )

# P&R
core_name = os.environ["CELL"]

# IO Ring
ioring = IORing()
io_cell = ioring.createCell(name=core_name + "_io")
io_bb = io_cell.getAbutmentBox()
af.saveCell(io_cell, CRL.Catalog.State.Logical|CRL.Catalog.State.Physical)

# Core block
core_cell = CRL.Blif.load(core_name)
core_cell.setName(core_name)
af.saveCell(core_cell, CRL.Catalog.State.Logical)

# Compute boundingbox
cg = af.getCellGauge()
slicestep = cg.getSliceStep()
sliceheight = cg.getSliceHeight()
io_core_space = l(10)
assert (io_bb.getXMin() == 0) and (io_bb.getYMin() == 0)

core_width = io_bb.getXMax() - 2*IORing.io_height - 2*io_core_space
core_width = slicestep*(core_width//slicestep - 1)
core_height = io_bb.getYMax() - 2*IORing.io_height - 2*io_core_space
core_height = sliceheight*(core_height//sliceheight - 1)
core_left = slicestep*((IORing.io_height +  io_core_space - 1)//slicestep + 1)
core_bottom = sliceheight*((IORing.io_height + io_core_space - 1)//sliceheight + 1)
core_bb = Hur.Box(0, 0, core_width, core_height)
core_cell.setAbutmentBox(core_bb)

# Add external pins
with UpdateSession():
    convdir = {
        Hur.Pin.Direction.NORTH: Hur.Pin.Direction.SOUTH,
        Hur.Pin.Direction.SOUTH: Hur.Pin.Direction.NORTH,
        Hur.Pin.Direction.WEST: Hur.Pin.Direction.EAST,
        Hur.Pin.Direction.EAST: Hur.Pin.Direction.WEST,
    }
    print("  o Retro-uC: adding IO pins to core")
    for io_net in io_cell.getExternalNets():
        net_name = io_net.getName()
        core_net = core_cell.getNet(net_name)
        if not core_net:
            raise LookupError("Could not find net {}".format(net_name))
        for io_pin in io_net.getPins():
            pin_bb = io_pin.getBoundingBox()
            m2_offset = l(0)
            m2_width = l(3)
            m2_pitch = l(10)
            m3_offset = l(0)
            m3_width = l(3)
            m3_pitch = l(8)
            direction = io_pin.getAccessDirection()

            if direction in (Hur.Pin.Direction.NORTH, Hur.Pin.Direction.SOUTH):
                x = m3_offset + m3_pitch*((pin_bb.getXCenter() - core_left - m3_offset + m3_pitch//2)//m3_pitch)
                y = m2_pitch if direction == Hur.Pin.Direction.NORTH else core_height - m2_pitch
                width = m2_width
                height = m2_width
            elif direction in (Hur.Pin.Direction.WEST, Hur.Pin.Direction.EAST):
                x = m3_pitch if direction == Hur.Pin.Direction.EAST else core_width - m3_pitch
                y = m2_offset + m2_pitch*((pin_bb.getYCenter() - core_bottom - m2_offset + m2_pitch//2)//m2_pitch)
                width = m2_width
                height = m2_width
            else:
                raise Exception("Internal error, need more of 42")

            core_pin = Hur.Pin.create(
                core_net, core_net.getName(),
                convdir[direction], Hur.Pin.PlacementStatus.FIXED,
                Tech.METAL2, x, y, width, height,
            )
            print("      - Pin {}: ({}, {})".format(core_net.getName(), x/float(l(1)), y/float(l(1))))
            Hur.NetExternalComponents.setExternal(core_pin)

# Place-and-route
chipconf = ChipConf( {}, core_cell, None )

et = Etesian.EtesianEngine.create(core_cell)
ht = HTree.create(chipconf, core_cell, None, core_bb)
et.place()
ht.connectLeaf()
ht.route()
et.destroy()

kat = Katana.KatanaEngine.create(core_cell)
kat.digitalInit()
kat.runGlobalRouter(Katana.Flags.NoFlags)
kat.loadGlobalRouting(Anabatic.EngineLoadGrByNet)
kat.layerAssign(Anabatic.EngineNoNetLayerAssign)
kat.runNegociate(Katana.Flags.NoFlags)
route_success = kat.isDetailedRoutingSuccess()
kat.finalizeLayout()
kat.destroy()

af.saveCell(core_cell, CRL.Catalog.State.Logical|CRL.Catalog.State.Physical)

# Top cell
chip_cell = af.createCell(core_name + "_chip")

with UpdateSession():
    core_inst = Hur.Instance.create(
        chip_cell, "core", core_cell,
        Hur.Transformation(core_left, core_bottom, Hur.Transformation.Orientation.ID),
        Hur.Instance.PlacementStatus.FIXED,
    )
    io_inst = Hur.Instance.create(
        chip_cell, "io", io_cell,
        Hur.Transformation(0, 0, Hur.Transformation.Orientation.ID),
        Hur.Instance.PlacementStatus.FIXED,
    )

    for core_net in core_cell.getExternalNets():
        net_name = core_net.getName()
        chip_net = Hur.Net.create(chip_cell, net_name)
        chip_net.setType(core_net.getType())
        chip_net.setGlobal(core_net.isGlobal())
        core_plug = core_inst.getPlug(core_net)
        core_plug.setNet(chip_net)

    for io_net in io_cell.getExternalNets():
        net_name = io_net.getName()
        chip_net = chip_cell.getNet(net_name)
        assert chip_net
        io_plug = io_inst.getPlug(io_net)
        io_plug.setNet(chip_net)
        for io_pin in io_net.getPins():
            pin_bb = io_pin.getBoundingBox()
            direction = io_pin.getAccessDirection()
            if direction in (Hur.Pin.Direction.NORTH, Hur.Pin.Direction.SOUTH):
                left = pin_bb.getXMin()
                right = pin_bb.getXMax()
                if direction == Hur.Pin.Direction.NORTH:
                    bottom = pin_bb.getYMax()
                    top = core_bottom + m2_pitch
                else:
                    bottom = core_bottom + core_height - m2_pitch
                    top = pin_bb.getYMin()
            elif direction in (Hur.Pin.Direction.WEST, Hur.Pin.Direction.EAST):
                bottom = pin_bb.getYMin()
                top = pin_bb.getYMax()
                if direction == Hur.Pin.Direction.EAST:
                    left = pin_bb.getXMax()
                    right = core_left + m3_pitch
                else:
                    left = core_left + core_width - m3_pitch
                    right = pin_bb.getXMin()
            else:
                raise Exception("Internal error, need more of 42")

            Hur.Contact.create(chip_net, Tech.METAL2, Hur.Box(left, bottom, right, top)
            )

chip_cell.setAbutmentBox(io_bb)

af.saveCell(chip_cell, CRL.Catalog.State.Physical|CRL.Catalog.State.Logical)
CRL.Gds.save(chip_cell)

assert route_success
