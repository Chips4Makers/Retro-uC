#!/usr/bin/env python2
import common.display
common.display.createStyles(scale=1.0)

import setup
setup.init()

import Unicorn, Viewer

# Run in graphic mode.
ha = Viewer.HApplication.create(["Retro-uC Viewer"])
Viewer.Graphics.enable()

unicorn = Unicorn.UnicornGui.create()
unicorn.setApplicationName  ('cgt')

banner = unicorn.getBanner()
banner.setName("Viewer")
banner.setPurpose("Retro-uC viewer")

unicorn.show()
ha.qtExec()
