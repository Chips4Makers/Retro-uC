import os
import CRL, Hurricane as Hur
from helpers import u, l
from helpers.overlay import UpdateSession
import common.technology as Tech

from ring import Ring

class IORing:
    io_pins = {
        "south": ( # From left to right
            (), # GNDCORE
            (), # VDDCORE
            ("clk_0",),
            ("rst_0",),
            ("mem_1_addr(0)",),
            ("mem_1_addr(1)",),
            ("mem_1_addr(2)",),
            ("mem_1_addr(3)",),
            ("mem_1_addr(4)",),
            ("mem_1_addr(5)",),
            ("mem_1_addr(6)",),
            ("mem_1_addr(7)",),
            ("mem_1_addr(8)",),
            ("mem_1_addr(9)",),
            ("mem_1_addr(10)",),
            ("mem_1_addr(11)",),
            ("mem_1_addr(12)",),
            ("mem_1_addr(13)",),
            ("mem_1_di(0)",),
            ("mem_1_di(1)",),
            ("mem_1_di(2)",),
            ("mem_1_di(3)",),
            ("mem_1_di(4)",),
            ("mem_1_di(5)",),
            ("mem_1_di(6)",),
            ("mem_1_di(7)",),
            (), # VDDIO
            (), # GNDIO
        ),
        "east": ( # From bottom to top
            (), # GNDCORE
            (), # VDDCORE
            ("mem_1_do(0)",),
            ("mem_1_do(1)",),
            ("mem_1_do(2)",),
            ("mem_1_do(3)",),
            ("mem_1_do(4)",),
            ("mem_1_do(5)",),
            ("mem_1_do(6)",),
            ("mem_1_do(7)",),
            ("mem_1_we",),
            ("mem_1_en",),
            ("io_0_oe", "io_0_o", "io_0_i"),
            ("io_1_oe", "io_1_o", "io_1_i"),
            ("io_2_oe", "io_2_o", "io_2_i"),
            ("io_3_oe", "io_3_o", "io_3_i"),
            ("io_4_oe", "io_4_o", "io_4_i"),
            ("io_5_oe", "io_5_o", "io_5_i"),
            ("io_6_oe", "io_6_o", "io_6_i"),
            ("io_7_oe", "io_7_o", "io_7_i"),
            ("io_8_oe", "io_8_o", "io_8_i"),
            ("io_9_oe", "io_9_o", "io_9_i"),
            ("io_10_oe", "io_10_o", "io_10_i"),
            ("io_11_oe", "io_11_o", "io_11_i"),
            ("io_12_oe", "io_12_o", "io_12_i"),
            ("io_13_oe", "io_13_o", "io_13_i"),
            (), # VDDIO
            (), # GNDIO
        ),
        "north": ( # From left to right
            (), # GNDIO
            (), # VDDIO
            ("io_37_oe", "io_37_o", "io_37_i"),
            ("io_36_oe", "io_36_o", "io_36_i"),
            ("io_35_oe", "io_35_o", "io_35_i"),
            ("io_34_oe", "io_34_o", "io_34_i"),
            ("io_33_oe", "io_33_o", "io_33_i"),
            ("io_32_oe", "io_32_o", "io_32_i"),
            ("io_31_oe", "io_31_o", "io_31_i"),
            ("io_30_oe", "io_30_o", "io_30_i"),
            ("io_29_oe", "io_29_o", "io_29_i"),
            ("io_28_oe", "io_28_o", "io_28_i"),
            ("io_27_oe", "io_27_o", "io_27_i"),
            ("io_26_oe", "io_26_o", "io_26_i"),
            ("io_25_oe", "io_25_o", "io_25_i"),
            ("io_24_oe", "io_24_o", "io_24_i"),
            ("io_23_oe", "io_23_o", "io_23_i"),
            ("io_22_oe", "io_22_o", "io_22_i"),
            ("io_21_oe", "io_21_o", "io_21_i"),
            ("io_20_oe", "io_20_o", "io_20_i"),
            ("io_19_oe", "io_19_o", "io_19_i"),
            ("io_18_oe", "io_18_o", "io_18_i"),
            ("io_17_oe", "io_17_o", "io_17_i"),
            ("io_16_oe", "io_16_o", "io_16_i"),
            ("io_15_oe", "io_15_o", "io_15_i"),
            ("io_14_oe", "io_14_o", "io_14_i"),
            (), # VDDCORE
            (), # GNDCORE
        ),
        "west": ( # From bottom to top
            (), # GNDIO
            (), # VDDIO
            ("jtag_0_tdi",),
            ("jtag_0_tdo",),
            ("jtag_0_tms",),
            ("jtag_0_tck",),
            ("halt_0",),
            ("io_56_oe", "io_56_o", "io_56_i"),
            ("io_55_oe", "io_55_o", "io_55_i"),
            ("io_54_oe", "io_54_o", "io_54_i"),
            ("io_53_oe", "io_53_o", "io_53_i"),
            ("io_52_oe", "io_52_o", "io_52_i"),
            ("io_51_oe", "io_51_o", "io_51_i"),
            ("io_50_oe", "io_50_o", "io_50_i"),
            ("io_49_oe", "io_49_o", "io_49_i"),
            ("io_48_oe", "io_48_o", "io_48_i"),
            ("io_47_oe", "io_47_o", "io_47_i"),
            ("io_46_oe", "io_46_o", "io_46_i"),
            ("io_45_oe", "io_45_o", "io_45_i"),
            ("io_44_oe", "io_44_o", "io_44_i"),
            ("io_43_oe", "io_43_o", "io_43_i"),
            ("io_42_oe", "io_42_o", "io_42_i"),
            ("io_41_oe", "io_41_o", "io_41_i"),
            ("io_40_oe", "io_40_o", "io_40_i"),
            ("io_39_oe", "io_39_o", "io_39_i"),
            ("io_38_oe", "io_38_o", "io_38_i"),
            (), # GNDCORE
            (), # VDDCORE
        ),
    }
    die_width = die_height = u(2500)
    io_height = u(200)
    io_width = u(80)
    pin_width = u(1)
    pin_height = u(1)

    def createCell(self, name="IORing"):
        af = CRL.AllianceFramework.get()

        cell = af.createCell(name)

        cell.setAbutmentBox(Hur.Box(0, 0, self.die_width, self.die_height))

        with UpdateSession():
            fused_net = Hur.Net.create(cell, "*")

            for metal in (Tech.METAL1, Tech.METAL2, Tech.METAL3, Tech.METAL4):
                Ring(
                    net=fused_net, layer=metal,
                    left=u(100), bottom=u(100), right=u(2400), top=u(2400), width=u(200),
                )

            pin_dir = {
                "north": Hur.Pin.Direction.SOUTH,
                "east": Hur.Pin.Direction.WEST,
                "south": Hur.Pin.Direction.NORTH,
                "west": Hur.Pin.Direction.EAST,
            }
            for side, io_pins in self.io_pins.items():
                is_ns = side in ("north", "south")
                io_n = len(io_pins)
                if is_ns:
                    io0_left = (self.die_width - io_n*self.io_width)//2
                else:
                    io0_bottom = (self.die_height - io_n*self.io_width)//2
                for pins_i, pins in enumerate(io_pins):
                    pins_n = len(pins)
                    if pins_n == 0:
                        continue

                    pin_pitch = self.io_width//pins_n
                    pin_pitch = l(2)*((pin_pitch + l(1))//l(2))
                    if is_ns:
                        pin0_x = io0_left + pins_i*self.io_width + pin_pitch//2
                    else:
                        pin0_y = io0_bottom + pins_i*self.io_width + pin_pitch//2
                    
                    for pin_i, pin_name in enumerate(pins):
                        pin_x = (
                            pin0_x + pin_i*pin_pitch if is_ns
                            else self.io_height if side == "west"
                            else self.die_width - self.io_height
                        )
                        pin_y = (
                            pin0_y + pin_i*pin_pitch if not is_ns
                            else self.io_height if side == "south"
                            else self.die_height - self.io_height
                        )
                        pin_width = self.pin_width if is_ns else self.pin_height
                        pin_height = self.pin_height if is_ns else self.pin_width
                        pin_net = Hur.Net.create(cell, pin_name)
                        pin_net.setExternal(True)
                        Hur.Pin.create(
                            pin_net, pin_name,
                            pin_dir[side], Hur.Pin.PlacementStatus.FIXED,
                            Tech.METAL2, pin_x, pin_y, pin_width, pin_height,
                        )
        return cell
