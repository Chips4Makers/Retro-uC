import Hurricane as Hur

class Ring(object):
    def __init__(self, **kwargs):
        """Ring(net=, layer=, box=, width=)
        Ring(net=, layer=, left=, bottom=, right=, top=, width=)"""

        assert set(kwargs.keys()) in (
            {"net", "layer", "box", "width"},
            {"net", "layer", "left", "bottom", "right", "top", "width"},
        )
        net = kwargs["net"]
        layer = kwargs["layer"]
        width = kwargs["width"]
        try:
            box = kwargs["box"]
        except KeyError:
            left_mid = kwargs["left"]
            bottom_mid = kwargs["bottom"]
            right_mid = kwargs["right"]
            top_mid = kwargs["top"]
        else:
            left_mid = box.getXMin()
            bottom_mid = box.getYMin()
            right_mid = box.getXMax()
            top_mid = box.getYMax()

        hwidth = width//2
        left_left = left_mid - hwidth
        left_right = left_mid + hwidth
        bottom_bottom = bottom_mid - hwidth
        bottom_top = bottom_mid + hwidth
        right_left = right_mid - hwidth
        right_right = right_mid + hwidth
        top_bottom = top_mid - hwidth
        top_top = top_mid + hwidth

        self.shape = Hur.Rectilinear.create(net, layer, [
            Hur.Point(bottom_bottom, left_left),
            Hur.Point(bottom_bottom, right_right),
            Hur.Point(top_top, right_right),
            Hur.Point(top_top, left_left),
            Hur.Point(bottom_top, left_left),
            Hur.Point(bottom_top, left_right),
            Hur.Point(top_bottom, left_right),
            Hur.Point(top_bottom, right_left),
            Hur.Point(bottom_top, right_left),
            Hur.Point(bottom_top, left_left),
            Hur.Point(bottom_bottom, left_left),
        ])

