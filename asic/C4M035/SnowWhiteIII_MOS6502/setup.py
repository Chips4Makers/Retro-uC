import os
import CRL, Hurricane as Hur
from Hurricane import DbU
from helpers import l
from helpers.overlay import Configuration

def init():
    # Set up technology
    db = Hur.DataBase.create()
    CRL.System.get()

    Hur.Technology.create(db, "TSMC_CL035G")

    DbU.setPrecision(2)
    DbU.setPhysicalsPerGrid(0.0125, Hur.DbU.UnitPowerMicro)
    DbU.setGridsPerLambda(14) # Using 2 lambda nsxlib for 0.35um
    DbU.setSymbolicSnapGridStep(DbU.fromLambda(1.0))
    DbU.setPolygonStep(DbU.fromGrid(14.0))

    import common.technology as Tech

    Tech.METAL1.setMinimalSize   (           l( 2.0) )
    Tech.METAL1.setExtentionCap  ( Tech.metal1  , l( 2.0) )
    Tech.METAL1.setExtentionWidth( Tech.metal1  , l( 1.0) )
    Tech.METAL1.setMinimalSpacing(           l( 6.0) )
    Tech.METAL2.setMinimalSize   (           l( 4.0) )
    Tech.METAL2.setExtentionCap  ( Tech.metal2  , l( 2.0) )
    Tech.METAL2.setMinimalSpacing(           l( 6.0) )
    Tech.METAL3.setMinimalSize   (           l( 4.0) )
    Tech.METAL3.setExtentionCap  ( Tech.metal3  , l( 2.0) )
    Tech.METAL3.setMinimalSpacing(           l( 6.0) )
    Tech.METAL4.setMinimalSize   (           l( 4.0) )
    Tech.METAL4.setExtentionCap  ( Tech.metal4  , l( 2.0) )
    Tech.METAL4.setMinimalSpacing(           l( 6.0) )

    Tech.VIA12.setMinimalSize(           l( 2.0) )
    Tech.VIA12.setEnclosure  ( Tech.metal1  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )
    Tech.VIA12.setEnclosure  ( Tech.metal2  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )
    Tech.VIA23.setMinimalSize(           l( 2.0) )
    Tech.VIA23.setEnclosure  ( Tech.metal2  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )
    Tech.VIA23.setEnclosure  ( Tech.metal3  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )
    Tech.VIA34.setMinimalSize(           l( 2.0) )
    Tech.VIA34.setEnclosure  ( Tech.metal3  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )
    Tech.VIA34.setEnclosure  ( Tech.metal4  , l( 1.0), Hur.Layer.EnclosureH|Hur.Layer.EnclosureV )

    # Set up Alliance frameowrk
    af  = CRL.AllianceFramework.create(0)
    env = af.getEnvironment()

    env.setSCALE_X        ( 100 )
    env.setCATALOG        ( 'CATAL' )
    env.setIN_LO          ( 'vst'   )
    env.setIN_PH          ( 'ap'    )
    env.setOUT_LO         ( 'vst'   )
    env.setOUT_PH         ( 'ap'    )
    env.setPOWER          ( 'vdd'   )
    env.setGROUND         ( 'vss'   )
    env.setBLOCKAGE       ( 'blockage[Nn]et.*' )
    env.setPad            ( '.*_mpx$'          )

    # Alliance top
    env.setWORKING_LIBRARY( '.' )

    rg = CRL.RoutingGauge.create( 'msxlib4' )

    for args in (
        # (metal, direction, usage,
        #  depth, density (deprecated), track offset, track pitch, wire width, VIA size, obstacle dW
        # )
        (Tech.METAL1, CRL.RoutingLayerGauge.Vertical, CRL.RoutingLayerGauge.PinOnly,
        0, 0.0, l(0), l(10), l(3), l(2), l(7)),
        (Tech.METAL2, CRL.RoutingLayerGauge.Horizontal, CRL.RoutingLayerGauge.Default,
        1, 0.0, l(0), l(10), l(3), l(2), l(8)),
        (Tech.METAL3, CRL.RoutingLayerGauge.Vertical, CRL.RoutingLayerGauge.Default,
        2, 0.0, l(0), l(8), l(3), l(2), l(8)),
        (Tech.METAL4, CRL.RoutingLayerGauge.Horizontal, CRL.RoutingLayerGauge.Default,
        3, 0.0, l(0), l(10), l(3), l(2), l(8)),
    ):
        rg.addLayerGauge(CRL.RoutingLayerGauge.create(*args))

    af.addRoutingGauge( rg )

    cg = CRL.CellGauge.create("msxlib4", 'metal2', l(10.0), l(100.0), l(10.0))
    af.addCellGauge( cg )

    with Configuration(Configuration.PRIORITY_USER_FILE) as conf:
        conf.misc_catchCore = False
        conf.misc_info = False
        conf.misc_paranoid = False
        conf.misc_bug = False
        conf.misc_logMode = True
        conf.misc_verboseLevel1 = True
        conf.misc_verboseLevel2 = True
        conf.misc_showConf = True
        
        conf.etesian_uniformDensity = True
        conf.etesian_routingDriven = False
        conf.etesian_feedNames = "tie_x0,rowend_x0"
        conf.etesian_cell_zero = "zero_x0"
        conf.etesian_cell_one = "one_x0"
        conf.etesian_effort = 2
        conf.etesian_spaceMargin = "7.0%"
        conf.etesian_aspectRatio = "100.0%"
        conf.etesian_bloat = "nsxlib"

        conf.anabatic_saturateRatio = "80%"
        conf.anabatic_saturateRp = 8
        conf.anabatic_edgeLength = 24
        conf.anabatic_edgeWidth = 8
        conf.anabatic_edgeCostH = 19.0
        conf.anabatic_edgeCostK = -60.0
        conf.anabatic_edgeHInc = 1.0
        conf.anabatic_edgeHScaling = 1.0
        conf.anabatic_globalIterations = 10
        conf.anabatic_routingGauge = "msxlib4"
        conf.anabatic_topRoutingLayer = "METAL4"

        conf.katana_searchHalo = 1
        conf.katana_hTracksReservedLocal = 6
        conf.katana_vTracksReservedLocal = 3
        conf.katana_termSatReservedLocal = 8
        conf.katana_termSatThreshold = 9
        conf.katana_eventsLimit = 1000000
        conf.katana_ripupCost = 3
        conf.katana_strapRipupLimit = 16
        conf.katana_localRipupLimit = 9
        conf.katana_globalRipupLimit = 5
        conf.katana_longGlobalRipupLimit = 5
        conf.katana_useGlobalEstimate = False

        conf.chip_block_rails_count = 5
        conf.chip_block_rails_hWidth = int(l(24))
        conf.chip_block_rails_vWidth = int(l(24))
        conf.chip_block_rails_hSpacing = int(l(12))
        conf.chip_block_rails_vSpacing = int(l(12))
        conf.clockTree_minimumSide = int(l(1200))
        conf.clockTree_buffer = 'buf_x2'
        conf.clockTree_placerEngine = 'Etesian'
        
        conf.gdsDriver_metricDbu = 1e-9
