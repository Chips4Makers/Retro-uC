#!/bin/env python3
import os

from nmigen import *
from nmigen.build import *

from nmigen_soc.wishbone.bus import Interface
from c4m_repo.nmigen.lib import RTLPlatform
from c4m.nmigen.jtag import TAP, IOType
from t80.nmigen import Z80_WB

from retro_uc import Arbiter8, MemoryMap8
from lib import Wishbone2SRAM


class Top(Elaboratable):
    """Top with only the Z80"""

    def __init__(self, ios, io_int_mem, io_ext_mem, io_jtag, io_halt):
        self.ios = ios
        self.io_int_mem = io_int_mem
        self.io_ext_mem = io_ext_mem
        self.io_jtag = io_jtag
        self.io_halt = io_halt

    def elaborate(self, platform):
        m = Module()

        io_count = len(self.ios)

        m.submodules.z80 = z80 = Z80_WB()

        m.submodules.int_mem_port = int_mem_port = Wishbone2SRAM(data_width=8, addr_width=10)
        int_mem_wb = int_mem_port.bus
        m.submodules.ext_mem_port = ext_mem_port = Wishbone2SRAM(data_width=8, addr_width=14)
        ext_mem_wb = ext_mem_port.bus
        mem_wb = Interface(data_width=8, addr_width=15, features=("stall", "lock"))

        m.submodules.jtag = jtag = TAP(ir_width=3, part_number=Const(0b1000, 16))
        conns = [jtag.add_io(iotype=IOType.InTriOut) for _ in range(io_count)]
        jtag_wb = jtag.add_wishbone(ircodes=[3, 4, 5], address_width=16, data_width=8)

        m.submodules.memmap = memmap = MemoryMap8([conn.core for conn in conns], mem_wb=mem_wb)
        m.submodules.arbiter = arbiter = Arbiter8(jtag_wb, z80.bus, memmap.bus)

        # Connect halt
        m.d.comb += z80.ce.eq(self.io_halt.i)

        # Connect ios
        for i in range(io_count):
            io = self.ios[i]
            conn = conns[i]
            m.d.comb += [
                conn.pad.i.eq(io.i.i),
                io.o.o.eq(conn.pad.o),
                io.oe.o.eq(conn.pad.oe),
            ]
                
        # Connect int/ext_mem_wb tp mem_wb; we ack always a cycle; no error/retry support
        int_mem_cycle = Signal()
        ext_mem_cycle = Signal()
        m.d.comb += [
            int_mem_cycle.eq(mem_wb.adr[10:] == Const(0b10000,5)),
            ext_mem_cycle.eq(~mem_wb.adr[14:]),

            int_mem_wb.adr.eq(mem_wb.adr[:10]),
            ext_mem_wb.adr.eq(mem_wb.adr[:14]),
            int_mem_wb.dat_w.eq(mem_wb.dat_w),
            ext_mem_wb.dat_w.eq(mem_wb.dat_w),
            int_mem_wb.we.eq(mem_wb.we & int_mem_cycle),
            ext_mem_wb.we.eq(mem_wb.we & ext_mem_cycle),
            int_mem_wb.cyc.eq(mem_wb.cyc & int_mem_cycle),
            ext_mem_wb.cyc.eq(mem_wb.cyc & ext_mem_cycle),
            int_mem_wb.stb.eq(mem_wb.stb & int_mem_cycle),
            ext_mem_wb.stb.eq(mem_wb.stb & ext_mem_cycle),
            int_mem_wb.lock.eq(mem_wb.lock & int_mem_cycle),
            ext_mem_wb.lock.eq(mem_wb.lock & ext_mem_cycle),
            mem_wb.stall.eq(0),
        ]
        int_mem_cycle_hold = Signal()
        m.d.sync += [
            mem_wb.ack.eq(mem_wb.cyc & mem_wb.ack),
            int_mem_cycle_hold.eq(int_mem_cycle),
        ]
        with m.If(int_mem_cycle_hold):
            m.d.comb += mem_wb.dat_r.eq(int_mem_wb.dat_r)
        with m.Else():
            m.d.comb += mem_wb.dat_r.eq(ext_mem_wb.dat_r)

        # Connect int_mem_wb
        m.d.comb += [
            self.io_int_mem.addr.o.eq(int_mem_port.addr),
            int_mem_port.d.i.eq(self.io_int_mem.di.i),
            self.io_int_mem.do.o.eq(int_mem_port.d.o),
            self.io_int_mem.we.o.eq(int_mem_port.we),
            self.io_int_mem.en.o.eq(int_mem_port.ce),
        ]

        # Connect ext_mem_wb
        m.d.comb += [
            self.io_ext_mem.addr.o.eq(ext_mem_port.addr),
            ext_mem_port.d.i.eq(self.io_ext_mem.di.i),
            self.io_ext_mem.do.o.eq(ext_mem_port.d.o),
            self.io_ext_mem.we.o.eq(ext_mem_port.we),
            self.io_ext_mem.en.o.eq(ext_mem_port.ce),
        ]

        # Connect jtag
        m.d.comb += [
            jtag.bus.tck.eq(self.io_jtag.tck.i),
            jtag.bus.tms.eq(self.io_jtag.tms.i),
            self.io_jtag.tdo.o.eq(jtag.bus.tdo),
            jtag.bus.tdi.eq(self.io_jtag.tdi.i),
        ]

        return m

p = RTLPlatform()
io_count = 57

int_a_pins = ["int_a_{}".format(i) for i in range(10)]
int_a_pins.reverse()
int_di_pins = ["int_di_{}".format(i) for i in range(8)]
int_di_pins.reverse()
int_do_pins = ["int_do_{}".format(i) for i in range(8)]
int_do_pins.reverse()

ext_a_pins = ["ext_a_{}".format(i) for i in range(14)]
ext_a_pins.reverse()
ext_di_pins = ["ext_di_{}".format(i) for i in range(8)]
ext_di_pins.reverse()
ext_do_pins = ["ext_do_{}".format(i) for i in range(8)]
ext_do_pins.reverse()

p.add_resources([
    *[
        Resource("io", i,
            Subsignal("i", Pins("io_{}_i".format(i), dir="i")),
            Subsignal("o", Pins("io_{}_o".format(i), dir="o")),
            Subsignal("oe", Pins("io_{}_oe".format(i), dir="o")),
        ) for i in range(io_count)
    ],
    Resource("mem", 0,
        Subsignal("addr", Pins(" ".join(int_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(int_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(int_do_pins), dir="o")),
        Subsignal("we", PinsN("int_we", dir="o")),
        Subsignal("en", Pins("int_en", dir="o")),
    ),
    Resource("mem", 1,
        Subsignal("addr", Pins(" ".join(ext_a_pins), dir="o")),
        Subsignal("di", Pins(" ".join(ext_di_pins), dir="i")),
        Subsignal("do", Pins(" ".join(ext_do_pins), dir="o")),
        Subsignal("we", Pins("ext_we", dir="o")),
        Subsignal("en", Pins("ext_en", dir="o")),
    ),
    Resource("jtag", 0,
        Subsignal("tck", Pins("tck", dir="i")),
        Subsignal("tms", Pins("tms", dir="i")),
        Subsignal("tdo", Pins("tdo", dir="o")),
        Subsignal("tdi", Pins("tdi", dir="i")),
    ),
    Resource("halt", 0, Pins("halt", dir="i")),
])
ios = [p.request("io", i) for i in range(io_count)]
int_mem = p.request("mem", 0)
ext_mem = p.request("mem", 1)
jtag = p.request("jtag", 0)
halt = p.request("halt", 0)

f = Top(ios, int_mem, ext_mem, jtag, halt)

plan = p.prepare(f, os.environ["TOP_CELL"])
for filename in plan.files:
    f = open("source/"+filename, "w")
    f.write(plan.files[filename])
    f.close()
