#!/bin/env python
import sys

head = \
"""TRST OFF;

ENDIR IDLE;
ENDDR IDLE;

!Disable cores
SIR 3 TDI (4);
SDR 2 TDI (0);

!Load program from address 0
!MEMADDR => 0
SIR 3 TDI (3);
SDR 16 TDI (0);
!WRITE
SIR 3 TDI (6);
"""

tail = \
"""
!Enable Z80
SIR 3 TDI (4);
SDR 3 TDI (2);

STATE RESET;
"""

# Read input file
binfile = open(sys.argv[1], 'r')
blob = binfile.read()
blob_length = len(blob)
blob_words = blob_length/4
blob_mod = blob_length%4
binfile.close()

# Write output file
svffile = open(sys.argv[2], 'w')
svffile.write(head)
for i in range(blob_words):
    svffile.write("SDR 32 TDI ({:02X}{:02X}{:02X}{:02X});\n".format(
        ord(blob[i*4]), ord(blob[i*4+1]), ord(blob[i*4+2]), ord(blob[i*4+3])
    ))
# Write remaining bytes
if blob_mod > 0:
    svffile.write("SDR 32 TDI (00{:02X}{:02X}{:02X});\n".format(
        ord(blob[blob_words*4+2]) if blob_mod == 3 else 0,
        ord(blob[blob_words*4+1]) if blob_mod >= 2 else 0,
        ord(blob[blob_words*4])
    ))
svffile.write(tail)
svffile.close()
