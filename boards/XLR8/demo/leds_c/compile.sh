#!/bin/sh
SDCC_PREFIX=sdcc-
SDCC=${SDCC_PREFIX}sdcc
MAKEBIN=${SDCC_PREFIX}makebin

CL65=cl65

M68K_PREFIX=m68k-linux-gnu-
M68K_CC=${M68K_PREFIX}gcc
M68K_AS=${M68K_PREFIX}as
M68K_LD=${M68K_PREFIX}ld
M68K_OBJCOPY=${M68K_PREFIX}objcopy

rm -fr .compile
mkdir .compile
cd .compile
ln -s ../setleds.c .

$SDCC -mz80 --no-std-crt0 --code-loc 0 -DZ80 setleds.c
$MAKEBIN -p setleds.ihx setledsz80.bin

$CL65 -C ../retrino6502.cfg -t none -Oi -o setleds6502.bin setleds.c

$M68K_AS -mcpu=68000 ../m68k_start.s -o m68k_start.o
$M68K_CC -ffreestanding -nostdlib -mcpu=68000 -O -c setleds.c -o setledsm68k.o
$M68K_LD -Ttext 0 m68k_start.o setledsm68k.o -o setledsm68k.elf
$M68K_OBJCOPY -O binary setledsm68k.elf setledsm68k.bin

cd ..
./bin2svf_z80.py .compile/setledsz80.bin blink_z80.svf
./bin2svf_mos6502.py .compile/setleds6502.bin blink_mos6502.svf
./bin2svf_m68k.py .compile/setledsm68k.bin blink_m68k.svf
