#include <stdint.h>

// Try to inline.
// SP does not work yet.

void main(void);
void wait(void);

static volatile char * const ioout = (char *)0x8000;
static volatile char * const ioen = (char *)0x8080;

void main(void)
{
    // Initialize stack pointer
#if defined(Z80)
__asm
    LD SP, #0x2000
__endasm;
#endif

    // Enable the outputs
    ioen[0] = 0xFF;
    ioen[1] = 0xFF;

    while(1)
    {
        // Put D0-D15 to 0
        ioout[0] = 0x00;
        ioout[1] = 0x00;

        wait();

        // Turn leds D2-D9 on
        ioout[0] = 0xFC;
        ioout[1] = 0x03;

        wait();
    }
}

void wait(void)
{
    volatile uint32_t value;

    value = (uint32_t)70000;

    while(--value != 0)
        ;
    
    return;
}
