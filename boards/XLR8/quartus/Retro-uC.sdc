## Generated SDC file "uc_top_vlog.out.sdc"

## Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus II License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 14.1.1 Build 190 01/19/2015 SJ Full Version"

## DATE    "Wed Apr 29 13:24:20 2015"

##
## DEVICE  "10M08SAU169C8G"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************
if {[info exists base_clk_period]} {
  ## already set by another sdc file
} else {
  set base_clk_period 22.5
}

# core clock
create_clock -name {Clock} -period $base_clk_period -waveform [list 0.000 [expr $base_clk_period / 2 ] ] [get_ports {Clock}]

# for IO
create_clock -name clk_virtual -period $base_clk_period

# JTAG clock
set jtag_clk_period $base_clk_period
create_clock -name {JT1} -period $jtag_clk_period -waveform [list 0.000 [expr $jtag_clk_period / 2 ] ] [get_ports {JT1}]
create_clock -name clk_virtual_jtag -period $jtag_clk_period

set_clock_groups -exclusive -group {Clock clk_virtual} -group {JT1 clk_virtual_jtag}

#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

set_clock_uncertainty -to [get_clocks {Clock}] 0.5
set_clock_uncertainty -to [get_clocks {clk_virtual}] 0.5
set_clock_uncertainty -rise_from [get_clocks {Clock}] -rise_to [get_clocks {Clock}]  0.500  
set_clock_uncertainty -rise_from [get_clocks {Clock}] -fall_to [get_clocks {Clock}]  0.500  
set_clock_uncertainty -fall_from [get_clocks {Clock}] -rise_to [get_clocks {Clock}]  0.500  
set_clock_uncertainty -fall_from [get_clocks {Clock}] -fall_to [get_clocks {Clock}]  0.500  

set_clock_uncertainty -to [get_clocks {JT1}] 0.5
set_clock_uncertainty -to [get_clocks {clk_virtual_jtag}] 0.5

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************
# limited by the ext_int path (combinational logic in the path)
#set dig_inp_dly 56.0
set dig_inp_dly [expr $base_clk_period/2.0 - 6.5]
set dig10_inp_dly $dig_inp_dly
#set dig11_inp_dly 52.0
set dig11_inp_dly [expr $base_clk_period/2.0 - 10.5]
set dig12_inp_dly $dig10_inp_dly

set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {RX}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {TX}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D2}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D3}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D4}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D5}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D6}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D7}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D8}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D9}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D10}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D11}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D12}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {D13}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[0]}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[1]}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[2]}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[3]}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[4]}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {DIG_IO_OE[5]}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JTAGEN}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT9}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT7}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT6}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT5}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT3}]
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {JT1}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC7}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC6}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC5}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC3}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC2}]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports {SOIC1}]


set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A0]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A1]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A2]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A3]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A4]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports A5]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports RESET_N]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports SCL]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_inp_dly [get_ports SDA]

#JTAG
#set_input_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_inp_dly [get_ports JT3]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_inp_dly [get_ports JT5]
set_input_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_inp_dly [get_ports JT9]

#**************************************************************
# Set Output Delay
#**************************************************************
# default digital input delay
set dig_out_dly [expr $base_clk_period*0.25]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {RX}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {TX}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D2}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D3}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D4}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D5}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D6}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D7}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D8}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D9}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D10}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D11}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D12}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {D13}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {PIN13LED}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[0]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[1]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[2]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[3]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[4]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {DIG_IO_OE[5]}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {ANA_UP}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {I2C_ENABLE}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JTAGEN}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT9}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT7}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT6}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT5}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT3}]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {JT1}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC7}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC6}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC5}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC3}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC2}]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports {SOIC1}]

set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A0]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A1]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A2]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A3]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A4]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports A5]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports SCL]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual}]  $dig_out_dly [get_ports SDA]

set dig_out_dly_jtag [expr $jtag_clk_period*0.25]
set_output_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_out_dly_jtag [get_ports JT3]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_out_dly_jtag [get_ports JT5]
#set_output_delay -add_delay  -clock [get_clocks {clk_virtual_jtag}]  $dig_out_dly_jtag [get_ports JT9]


#**************************************************************
# Set Clock Groups
#**************************************************************


#**************************************************************
# Set False Path
#**************************************************************
set_false_path -from {D13} -to {PIN13LED}
# Will need to change when we put real logic behind the JTAG pins
set_false_path -from {Clock} -to {JT1}
set_false_path -from {RESET_N} -to {JT9}

#**************************************************************
# Set Multicycle Path
#**************************************************************

#**************************************************************
# Set Maximum Delay
#**************************************************************

#**************************************************************
# Set Minimum Delay
#**************************************************************


#**************************************************************
# Set Input Transition
#**************************************************************

