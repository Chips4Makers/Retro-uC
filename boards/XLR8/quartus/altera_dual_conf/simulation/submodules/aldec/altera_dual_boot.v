// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent= "Aldec protectip", encrypt_agent_info= "Riviera-PRO 2015.06.92"
`pragma protect data_method= "aes128-cbc"
`pragma protect key_keyowner= "Aldec", key_keyname= "ALDEC15_001", key_method= "rsa"
`pragma protect key_block encoding= (enctype="base64", line_length= 76, bytes= 256)
c7A1R0/eOKNQEPg++5yz2MKlDv827nDCaYwnhZ4Xb1fWCQuUpUDbS/sHeYJEJnZP8joAER8fLoVG
cf3OOb6w/tjYZQpSp+CGPBOwZ2TfW6qewud+GH/XWewJk3snQMJdigTqNdC/9FbBQLkxzn/6fNuR
Bz0ohU59gmMKDcZUl21r5gYW54Hr745MiZOSKdD+3paQouCDWdwUQwFA3tpOTGMleO4YRPqqhUI9
pNBojaVb3E/IEwq14PgPffW938A1MNJmLXxSRrGfUZlWMjQEnL7OcKr9cSSeOJJSjbYd7CQSpHyf
FYrRFmMa7SSJoaWqn29DFIZ++XnVZQIssjjEUw==
`pragma protect data_keyowner= "altera", data_keyname= "altera"
`pragma protect data_block encoding= (enctype="base64", line_length= 76, bytes= 2160)
hPsY4CxiuHSATnjvoh7BrHxw7eRpYIonMQlZdWpibHPgtWuhVO+P3QOKynRTXBCoVrAHjcLcT6UW
OEVzfpuuCP5EjoS+sgHogBAkJb4mu6YzcJUQqGOoTqMPgIjttednxOPW9Eucctv7tWrZCr1pm0kh
aQSQo/pwj2cDRw1BNYJmdC8D8mpPC1df9cjaw6jQfp8Qc9kHm4r87XxOXIqaDy6YvdINRhTR6iwK
EpYKUyY7IT8eyK/6FXM1fTgg9cSOgvtl8tFqOlydy2moIFQTMJkB3rmbgsi96RGro3xdmbgbby25
baCxH2ylwU8D/enShn1n7G+/YgTd3Ep4+R2T0Ln1X0EsLCwkcDWcN7Uog8WNAplLaBvRlpxuntni
mLOCqibtvfiSluZxG0O/daBFHUbSRkAXnGSrw4FTFd3NGrDSqFu+q733Y2g3mB5y7GkVqzZQMJMH
tWPW4miCEiVLi7rN2oeyXIKUXkZhHxseZQL9fjFthwToa9/v//+6puXBCNhlcJK4gRTcCXSDEhV5
OvjFzRp4sY8b9WflyJhginJDmWHiyngith/OWDHgh+8KFtC9+gs0gvmdKTZ4R3+wYlns3OiBjsjy
pPbS0LH1TXSEa4hcpGO4g298435JKHrT1qE5GKay+xjgucbz5ldWK7qTFDIS9z3GHKuSXkqvfJbc
6X8uAVfBgxt1+hYKbb8m5GKbDkttGvECKkwCbFa7FJTOGSwL/aVg7897YloGP+lQj5K6WduaOTbi
6Q5Err57iT+TOSn1uX3R1hqJL4ml2k/+pWUYeF9LrjoESYvKZBbF99q1dUlIQNQHfmv8catBgD5K
v2bL4DAcgF97gCpzdRksKa9x2wgCwVdo5DEgV6pAojyZmLC1kubLL5f4wpBy1o4407uEo1EvA+WT
9x7k39zyl1flxauwcPS0aXYiaYLCiGxTTVO56M1L/BEm85DZqbpwduH3iP0OHDXYCkzNmwLDkuv3
6SF1/ZLyGcmkDeNEbDlUT6U9HB4CvFpeD13ZA0gBkhFpmCmKVfq1PNTnYHtcWQN3UoeS9SKdJ1tg
h3Wz9wUGSH0RGxEpCq6kLe370tz/EDDulxovnV4tKUqr3FmBEbicGAn3wqwAG71YbxoakYhz+1/D
46OMgS7kS0VLjAJ0dahwB5e9VJd6iu4NGEtiir+0pgYiQ2fJUqaFaGdOg6enGT34/C9jFEp/FmZz
d9SjTjAgqnDZdCVIjSHqyMcYVIBcta/QXUMkUQdr7Aqv0VidKj81DsAAuc5tgJfEUXKQLsGo+S+0
zAIpt7ufVAYdoGYFQyEjrkaiL0yr7TDCHZqluGG4T+geZ0EjYHpeOpR+qkOp1Xpp17dUCmqBjzQe
bFMerjGWonzVwlnaeIFPPaSEBdX6pi0IcdFr4h8mCD42PLgJ9lanBUj2HU/Fk1yeDpMY16xf+G6h
jY3MDyoCAJAlPzGGbA2lF8c1ZgmotAYunIaWBcDy7zC4w0TcCQ3Ne9r2fElCycxHL9G8QKz++XXs
ei42CG+3zwaMSm+ewjKHbrVaNa4v9cm9FYva3o0aSvlOEsCrHSY9BMV4DNnfHe9PoC8nA2VOObtO
a6KYb79+jeFWl6At3KTJ+uK66FguRiJxbmkS39T0UaEtesphcpqd34YTC7RRksiVTDpT3xvyIS6I
62l0vMNQyB0g9q0ZBvK6zVapOg2JSpd+4CWNtoE6N1+c0Oi2/daDSCNvlktP+TlCnhEBNAFOBufe
44I+7xG+CC9wjzxW99ZlBb0NFO04DhwgMNvPYm+tjBqjeCCGuJ6VzQ1+MChqH87YDzmwc+9euzb7
tYwZoBOWq2+QWBUwNyOcL33dq65nYhCNPlus1xxQa/fzzRRMrTCKIVtcrVqcK8l0LZ6Bntm+OMhy
E/gyQU4nW7VYNvuaSy5Xxw3ghRcXBd02kB3Piqh/3KAgLmTXllAL7pUUVd4VIrThS2d+1C+f0FjE
10OYNKJ6k4+c9BbfVXYxmVDCm/VogwkDHjsc4kMFI6aH986t6pbx1xTbF50578y+Ep2JFJa+PY+q
K57NttZgMNpQbTO7v2QSIdJIRLAFIR7mfcP5O6K/QL/Ou7UqSoWUA/SL4hnOil700zAppc/+mrqX
hrcuqiFk4bOj1fD2gWJA0arclvjO6+z/sIYjd49c8QJ5nRbDpZEtZHbQiCGZSUY0prqNu/Oxt/6+
eRfiwfVT0greIBKOYYwUR+8h3oK4FJDiKXHhHPN59MguI0vc+LOxvmQGDf9wAZE1p0aaFbaCpqpW
34WBP3ey/ZBxscbPRWcctBBvi3SJpXSgkZ/0h0ef5YivfhaKGKwBhnN5yWy5fqIauu4XR0cdpigh
ptgekVlrLa9V/QM2W3s3DxgU/f1PY36Mcxf/EbQZvk3NHU8+7tQJSEVJRx4hkh0qAiJccDTGQSa2
RlJz+US0a7cls0ycAyLF1XRhYq/x4XAFaB0Ma2lNls8WbaR9ZQiE78wB+nN+ORLfPXThz+G7VA/M
02Ux3kMaKQwkAFDqI2CzfU+y9Fc8OXnyUoQO2syE6zz31k6BBRWK6ncp6/Lrvh4d37ql6NhMMXWO
M7qt4DVwMtuiPFUG0JyD0z6h85oEpEeUDruf0Y20uiEcwlRhoIeys9PZFAkxTUiIXJh1U2BsRLVM
X7Z05dEuzKaq6GcfHmkFnJUOXS0OQyjUBErtDWUdqO59xZpUBCLAEGYVz3HDH2ccuafYzcUPXVrk
6xCm9WTZrM50sQ/uZrVMRCeQN9er5bSBJ5+R9AbKl+FAtW6T/GKi0FonTVRBmF918u4TA1IgdOPe
Fc7FSH1FrnMS1cOwlOb3ujtuhLE9uTwQ/hEyKO9RpMwBRzW+KgGkk3OoQiOU+vkQw+ny
`pragma protect end_protected
