// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
//pragma protect begin_protected
//pragma protect key_keyowner=Cadence Design Systems.
//pragma protect key_keyname=CDS_KEY
//pragma protect key_method=RC5
//pragma protect key_block
A36QsuZnf0eabIHjDd62roN+OpjqAj4AWHjLzryg96RlE1NkRopYR6kMVqC+Tilw
jnGnh5jI9z4/WSZPg2fmciVxH9o1wFVJ5QhDtsOdneeegFNbDeyEJuFncdcc5lPu
KSkaaKSEkZ8+QojCffe+2+ybTN73ITtvq/EorM7/TqztkCjnJ5396Q==
//pragma protect end_key_block
//pragma protect digest_block
nYBn0orLr7VyONgJO4n6Nl8xM/s=
//pragma protect end_digest_block
//pragma protect data_block
7f3//cPeFNZ+g0LkYz66iVghtHQ7UL5qzvEIFV5NymqP/B9R2fwiKzskFoOFTo/I
knb1khIPPE2OHfDezA7A2ar7xMJEX/1vgLm/0bv97A4cyzF0pJZ2Mb2L/0nI5I4U
Nb1+Wcchu8Z8CQ/oAMSX/nUl5joRu9TCR34jOhraLJKEuv3DBJYi4El2X2FELerw
332iTgbt8/nBs5GfD8y00DEv616FSmQ4LfHAll6qoHyDKtUgPTESs4lreISpPAw3
SmH1ImMzN6ycjAP3kkqTCOytdMAqaEDBmEYLBb/figoL06sjuRMuwPnCXkT7ZCEP
NE1ciyCvU8PMG1avdOtnaZvSPxkJhgEAlz1exMN9BCZPyTDgeSUgAB6ztbxOaMRF
MCikW3AgBDsfK3bIw4oenjDqDZU9Bw48Z4MifhGjDhBPz2hcgEtHAdsx+pnnVSvr
I0s+97q3HEGoxamvsucgbywR4Aql6z0Kl78bqiXmQrDW1EYyhFBSNMQCrby9lXqj
A1Jpk8gSaA/2gR8+de6J28fIhFxLk9TZOaRB7FpzXfIWeZ8UmKMzmzeeo2R+uKZw
ezF5Q0wxAij/mxy3fAJJB8Ww1XaJFFx8B8EMzNbOFMAuEcykZJF34QVDM+baDuVh
8qWMkO5XVkCFgntCNVTG/U5TQm45yMlCNpo+UfoRtqvmkXqFqwCQdNSqZ9v8GSHo
0qHk8RcdU0jZ+c7gKzCZ/333aJw8AmQLezFGAp8yrkdNJkh9OUL2v86UgDPuOBUQ
TZL0W+/W6QYYeJvcpvqz2HC896Yi3VMhlAOdvpw9TNt87mnc3nTzJ1ArEh+jjnpg
XtOlyb0mmNAqrOwpSDZRfZ8F8skdCoi1pwg0DbhI/fF+9fZe67pc+8HILRXRkq0Q
orEDu0/PGXjJR1IPSXMzABSTOPo9vUb09CcgYO6J/d/J7uNyN1JLjz0Yxlpy3yb6
cfZW/dnf1I4vdZCm09RCjaOd8aGB9suegOi9JCovDEr/F8+PTVVwsE1iGXBvLkH9
M7KQS3S4Aqn57wGCynwXCP9vHb082bX+ygUuh9RvtxSE74yUBN0npoD9ub7SACCX
/2nI4+uZUqHiy8DJeCmXpB9PZmemsKuaTRsFO1pQReNw2dtgGVoIAn/Tw40LDnXM
7MGbohBn/mcD3etXbrVCIkCP9bex0D9MBYJpjQn7SIgKz+9E/PQfyTfsCAyMTb1f
2BI7rJLhc9iVLPS85N4ibBw8rCFiOYjsVKZNRpfqPwqDwfIW+ukaL3rpj3sN2d/D
4iSpkFZkLVXYoXqoziR7W0dCKuWX9ZdGQVahy5A100MBDYtCGelfaY6CAHbgqfzJ
qVsHEyHfQdKBQbPgj08cBxh8eeiaTqvczCNUwxFXkX+NAzA8APovEhLh82xcYQ6e
vuczBy0GcQJqzr4iCO6tJ1UGex3ZdjV7zHwBROaEVVwBMurA0MyuZWnqk5TDT0TV
amNABSdxffZcFdYmKCaKPCEb8FRJL4XygaebY03RZh0IusV7dSknIgJ0LZ5xo+77
Y7Ev2gNzVVSGnbwqb5tDjkEY84g6wT9EGHYT0n8vnEQHDdzV2b7go0jYSQo6kD1y
LVDMDoC3/r2z6Ym1ipea0qUROAZFEUIGvfXKp/sSu9U2Cccp+GFh/CKLcfYYkrl8
NH7VwVIok4A2QaA7aDFEgHiblBuq/BPQz18uPBXZ/+r6rgXbaf+WYUJ3GEM2oVwe
GdwwZJaOjPJK/TG3a1DNhIFwjcsXKAy61c0UrSRSueMgb452BfS9N1A9P3ZbwbOh
2ySXcNIchzZqvtiGbmyddZwEPLEOT85meZZ/Ru128n+epkEKaMbyTgvOC+7YOFvJ
7GYnm/w/qqPum60qI4N8q+OHJbYT8aL7eDtI8nSSPdQ5O0fJqvWgXN/2FTLA42e4
XWw+2x8SfQ6nTof3QZclhVlQ0TBArLPA8RwsBb4bYUgJ+nKn7XPzc0EMpnKMWTB0
INWMifYZz08N1y9CUC0+CAmG5id8dLXD7s6kRO3LVVS5/QHD6HTlnx1VMwSmq+d7
MYG4rkLCHDVZmeIq/4wt7y80tUM+DLBBho2PygJbR6bhWPiwUPDSNZ/oV6D/5JXh
geFJxlaZb8E8uxX6+CZG3rW4FmWUNu4OnHwcv+DUOmy1OwVY0qiwjVHS4YYi4sI/
6FYIjzM39YMifTFTqAdfxZWwKUWTX3IQSLHnmYxgPDgB0ddvf8zaGdtWShM1t08B
qNvKuqA7teZ0JD8bHbbM/lXzYHnGPTpYWkJXMnMTZQV21F5CysQAmBQLakPESWAd
pM77zY6ML9t+Sjh3hyUakigbBUHk4zSmpCKiws1+5SWP8XTjeq1zN5U8vTluAe11
vvKMHf+zIPi5+ebN7cmZIAUhRq+p8V7BlghGcnGng/4zBOOb4+MDJnruickAjRkY
MCowro9JcFVhMxmY2pnyMuMx+fCzANT08N6kpQtFHj/tUkXLPNYlUwWQCPbG6Xdy
hmBUMerAGyOukDP57azeydcaVxS15tpSSaCrbvVcchSfDVYsMDzRd9FtKyeRVS6i
/o1PyrRPbwP8RArlFovfUV3Beoa6DmQ6STT26XCAWXEH+7TluWcdsdeWSEiWEcj2
en20IzFoO1V4oE/NjDGAjZwwzZDZHuIAvnmuKaIa7zScfRMk4kthZB0ZcWv9f7es
qxO56JyvRvEmfn6rgCInHQ6mOu8ztzNHb34hxVBRs4ZeA/KOgIOwXou41YK0Sonv
OpRO2239mcnGeu5SHiHgGpw6d3dnVrdV3FE2Aw8zelidCMTa7C1LPKUzko/JX03a
v4JEVvEn7e5ii3PhiDkSBHU3ZrpHWy5h1lxT3gyheiuxpb3KmJn1Vs78dREowZD6
QgIrzkKntaRKdfW/28j/cNsPgo2oFq7E42OaZDzZKbM1LvWdNtX+VJI2GEB7dk+o
pt06cYIhdKSlY5PbzWa86cEfPjaFxDHCnGU5b3Z2tb4i/05DBbQi/eDPbY+/ykKs
IesHWH+FdN8Qxb+u0TUBsA++u6T9T/IF1TQERA2/RxkDugcAy5lUtiulZJNQhDhh
LSAtkteSGLVITNTUjTUw8axPZYDyG8yZIHa9XFtDDq2NRGa5HdhsMwfwGuXw40D9
OqonRrnB9OP1FsE0HewMif+nT/Vk6qx3/zpPon8qdAc=
//pragma protect end_data_block
//pragma protect digest_block
O18DNzFhIg3UkDPVVz7snbHMa50=
//pragma protect end_digest_block
//pragma protect end_protected
