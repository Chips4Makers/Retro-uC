// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1.2
// ALTERA_TIMESTAMP:Thu Jan 19 03:42:24 PST 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Iu0wf7zysNPmezaqWaYiCg27pj0UszGfU+nfK1MjkbaenH2qqL2gWsXJ43Z3iryP
94NfPjJ5E7McXJv9qkbSykbCNa6A+HuNFSdpgevrpfz9jP4/b6VY79w/jdJM2SN7
z/t0w4MOssn2SwZCeoOBZTyK7yLLct+/gvElPbzgJTo=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2240)
EKmss+uJ0NYmczYapTZyCmLgiEWvqpi4JY3aPvFmMxw/8q1JybZxZTCDdBEBu2Az
XRwP/DakQhxGmgyHvfRAx5qvM2qNjnOd8mvS22iqZh3Fz8Keq0Wh/AidYXqo64Zb
TvdvhiDb4Z4Abasn3aqgsj2YPXb+f1VIUJ+RjvFgQGxbzhhGUzAgaj3G7arHgnsK
ITB86Lb4Oht+g+8CH5WHysQsLrlIWBsWgLVr1msApE9QXY1tO454e4Sth9hL2P9v
p2ObJWKXxPnFcFXoZKJiJyC2PxoDO4Upf9TdamPwi7jE/+Po15nQuX59NFEqlxdU
flmpSjDX58rfBRTXf7FrM9ueVnfbRmG9aT0ISgom/G530iDxZWWrXp77wuqx3/Ip
p60ZlOpSy1pJU6jQC1U0/9DDMXIk8OxzNlDBQQmKvUeb2gLnjFdVxBiyGdg0kUKQ
i/hE5wB5BwoPWGjZTiSSoZbWYlXXTq2kdzvey3RBciAV+XjQ7zyLvqKFLHMZUC0h
Sv9nJM2h23zylZL5CMsY3taZkSRRYyJB0tHThgRnGCGNtSWnpCtSfP4+IxYvEGBM
VOVc2bLwX9IdJt96ESqf5ejYTSvJgTe2EAwciJQTzzr4H7vt+/9kQPKgd+/bIPtp
IX5FVNofOVSzHythmoKMQdHhZhlY+WxYDZVnhM3PkR9IQaK24xyor3vlueTR/dLb
JBxDJ+ArxT8ctYZ05ygprDByMpnLBFEOg5C7ZuDKUUX25bIQnCwmqEx3rjoq2wzi
+KgmLN3YPE4g0sd8apzU3xaL8pPy61qapiC7Z4bmv3bizBPq8tkydCzzLeBIod9n
pnZCqXlHMum4gN0ypLKzLLCmt25tQS5hWsjGEohLb7a7eMgpym2BmYSuuAXFlBlu
a+xVhiId5BfP1t4R6cCZgqfKNsqi7T7krZcx+lXczVdN5lhBy3P2FyYoZ+nMWoZt
1GTB9AsiCKyP+tIASraAz8NHQ6k3VRDwzsBXhSE2lu4gnPonIqBAIt0S/6cXK9IY
D16Azlh/8FwOKQ1RrpFRNOdHYXvQtlhP1zxDze1aTFwicDnDXHqWoRcEc+EyxeFM
6VNIE9WWi8k8AqP/KyOoY9/Tfeu8bVa4Wg/sbaUx+c3myrNObu59JCkoQbUq8Ku0
TQHROL918gDmPT82kbgSo4QdsMrpan7w79nv9ZM2KIqss2j374OMdfrKuAxtYnaJ
Rg1fdwlAKlAMNCIBKggeJYW5IzLPeFkcmPcmAGmM3pv5fXrsGtQE2rjm2zhb1w6F
8LHRFuqyDqHWfmJFiPs7KT7HauI9nBVafyN7MA9WDPjUgQnyhNE8p5cVVWrcf81n
1mMPxjMDsPjAl4EC1Bagq39CNY/97b78gYbUyr1bwL9mA7Vc1WxMR1ggqOCCV1BQ
I1HXHrYMniv33syF9wFq/NiAh5qA8iAXG9hhjrJn/VLjl+MZ/MFDo0VO7baK7rtW
UmCjn1EsiWCL2YyVpBqTTZQ+/wksrN8LAN3VeDJL1rsXmwWN+1NhGNK4XQPHNqF2
Zf8CsUS48aaCJkfVEPQK3RMAQYJc47Na6iKvLx7kpDDM/DlXRw0IjyUOy8Z/+ifM
cNosKR1uci73J4LDDpb7nJw29Eteq3uHu6N/5sZhze1Hapy9tWOv5f+FosaSRKle
iCqlrcLorIEhdvoPSRpMvr8Ohwm/2lqp78bJCBs1XKdGhQQpZxJkt8UJ8c8hiGhm
L7qht5JrRWBCMd3eVDoznEBc0K95HXMJuhnE2D4+xwKfZDf1EKrJ+j3IwwxmmflR
Jp0BbQ3BpAZUxa1kIIDnW3Gr9k89L1XspbpxJwCHl861u67atxujWcqQUoz/FRaz
wadna4Vy30ePA/d78ouqhfWNFE6dKaHrivfCTeoFoTbcuw7LZftZGM4+C94bcu1s
i40ejzCjDe6viqn4QURB3zGxD2A/Dm68d7/VEeENDaUTt1vBQcevMj1H2jD5rkeb
mJsecyNtjmHgdMfqvbrzlMa8uNIOdGZiJWZQ3H7p7YX2vWs9iLeGqePL8SOB4yU5
08IdjxFsFRbzPDAiL0MMcW3ddiFizVRNEqHfaUlks58EpekQ1IX3VRd4Cv53zGpw
WvZaTQhOsJqQuBzqQL8ZUkDRpmdf35PhdYfJ/aEc/HEiDRt3zhKYOPPJWsMSehiv
j5qkfv3FOr53lY10uU5SrcIodmXyh3p0IByKW9CgO3n/oc2u+/7Q55ueZvlMrgxQ
+Phf02lD9yvdQzLX8PlqW2hJqCd/Gidb03fpqFyXNTDKxo5qjuoiBCVtO9TfoHr2
yFVX7ZSngw9lXy1vjrdASk8GUKDHq1nxl/E4Rjwi5WPccgWcTK6nSed5dz9XQsM1
3vluPwZwjyshax1sVHch/bvpvrRaqi6nF0cqPDofHGLXM4r2Ln3XbiZTEfFGiPOx
c0AFwsbXQDRF5Kygh+usceUl8uY+pKer7XWIQN0ea57PsEuAV02HwLtOUHlPfW5q
kM/zJ5yQZAekDBapmnEPWeEbfKsj87ZJI5okntYlrV8IHx/BrQ7vUZYPPUd6s9Rg
nx8xbLT7Eq1Cv9+9xG+gZ2j4m3KXYfQ5bzM+ToEOxhIycf3HKkjU540CSyCTQVXV
sSgRDW7Xj6kJt/8F+p7BbVcwm1TTrzdIOoPH5Pg2aZh5CmwyVUoPW+xhMCMtew1U
qcPNmj5NkiaejNReVC+rfuwb+ZuKnNX2uMhLn2Jld9ePjBlEgaBVeQFIR3Ku2QfJ
cUwbLD+0EPh7w67+MzVTkJytfE3nYp7SgjH3jjVatFVpbzdRJEgqBsetSSKyv0fu
bk/eOTvo1HtQbSrK9Ux66wgpxBAG+KGj9Q0KoX2BDp51XYU4bTlIerpRaYh8T9z0
JEqhtZ33dJ1P4luyTXOGDjVCy5TAQbukX689RriiD95lekN8CE5QpvOcct5GsllD
nbk+T7OJpL/SJNPHfkdsHG02wwW6kFfmLylbaaefLLU=
`pragma protect end_protected
