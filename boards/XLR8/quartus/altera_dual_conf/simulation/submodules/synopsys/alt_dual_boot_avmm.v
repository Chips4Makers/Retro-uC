// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
HWQNJVH>]WVQ"=PE(OBQ ZZHY:XE.T&TR"+_?\[+D'DSK_#LPU)[IY   
HHB#XMC<<"1X7&#'&#$_-D\B8<5]N_T\Z&XYOU:E;.6&Y"7[A-#&.4P  
HC!>R2_=ADU3W1F@?UF-K43FIA-2+>';P'T\\&!':&9V-1=''@ 2$>   
HVG0  P:Y+/'P[>'"?\]J"J#:0-6[Q*IWA^_U&:77C"" H)I]&_DVFP  
H J3YD7(L&BVS>[13#"J[B+;"5$(Z)_>CT^)WT7%4;5_1N <)SW2] P  
`pragma protect encoding=(enctype="uuencode",bytes=3888        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@4:X9F1M^*G= 1BNC)V>2>_RJTP)K4'SIW2Z([B,/1^0 
@!N84OLF/*N]G?+A+(9&SEE5P@];D\@\J31QHMN_/X6T 
@$ V,NX-B47YG(^+.W!JDM$VL-6_'V6/D";J7K76WR(\ 
@* A.O(RK<44:=$48K$, -3&TG@FYB&B)C1XU"5T47>P 
@7=[!KNA[\(;?G=S,\I9)]<5ZS!-] 14A@[RCYRCAP,( 
@MI33)QU81UHZNAF2N]?YXN5+AZHQA,W(Q"2C.=E@;K  
@7#GRL9.@(1A6^FJIJ,Q,PD!(L-A6$>I3<#*J[ (-G&0 
@0OWN>_ZE3/2BZFM[K10& K--N$"E=3*:1/I<'@^X:4H 
@@A<,\P__3) %&&C=S1,34I@%'H5L5K&G \=&>C*V?:P 
@&(\,J]$A?FB68<Q,)8$A-[>\,%XHJ5ITCTR^+3?CXY4 
@Z-=A/V>$'10NLD^1)<GV<*)"CC@*//T,)Y%17IWZ'WX 
@>4;.4G1T0*W77+AP&:_'M%-GL+L^0:<3T<=8;_R66'D 
@YY\*@;'2#&J5QO5!73\UROP1V!,PR"@KO!\*[']'^?L 
@HT+\_WG]EHK:&6B!#RUWKN1P&DZ0!HI,8R"N<,ZE"FX 
@B,)"U[=U##=RV!-#8H6Z:E*/<96H'/@G@[X. J5VZ)X 
@TEUX]2S&/]VQSY]T;[88TC1&Y[614T728NX\X&&3A1< 
@.+ I) ",I1>K_,^V:EK$BXXRTI?+ Q>?OTI-(?$W@04 
@G.:)- LR5U^*N>UM=11<.S:VB-39)?V]+++DEZ>N3?$ 
@&KQI6E^P";+ \H<?BMK,5P:2E8X9T*6O]D/*'-N$]^4 
@5/][3WV@@5*&J#XE91;4MD=PF8A/RW"K\Q5 Z!8P1\0 
@X2!U6+M!U8@ +KMP+*2R*\8VHH(2TF08;CBF/)$P[[  
@-4:]<W]GK(#N#!;K8Z;"EL2^HFT8(E;KMLUW2Z/^R8P 
@;(H&AET=&?:"$(^N' ZN(]I2!W6C ]GY22A#DCCI.&T 
@!<05F$65F2[=@0![;7.RZ;IWP.ZI;IK4W:RTBX<+4(\ 
@HW7_N$0]I^U(%/B[DWHO\GW'ZS"[2/L.4*Z!1TIM1(  
@Q)GLX"P I>4<1'I=R\0SXX'5YU]Y:&B#BGDG5SX#NO  
@?J+(C1&S?\QR[XIZC3WWOJ6+X>4)=/%\+QS^,YK +WP 
@;=KW@*[$J2H/#)U>S<6G<]^X3&VS0M9_'M+MH9,(+OD 
@GGA"KRRY%% %\R-MV1M?G9VH#<K<>5-H\"&*FSK*'1\ 
@\)8"H((=<G+1 D96W@4-0=5FDN.K_/5UP]L!XL6'O^, 
@D[\&T<:%9)J<\8=GYCEYZU[AF98HIN51GEAER]6IIR@ 
@VK 9;>TL>3!%S1M76@\;K$)<V8:NN&F$S:E"54"8[]< 
@WBCG,6E1&^EZ*3'F\/02R. @:HV25Y.UUWE.Q/L1>30 
@TKKY&+A9P/S6S;^E9K.A9*)<.12KY@5I0(^YK7II99T 
@3LW>6RR\HM.#LOPF%P6%YXRD T0]3WE1V*3YVA'$@-D 
@L@\AE1]?+/N@9H5+$W\"%1.KJ'_3=_7(?F+Z[SQ ^8L 
@M). .2O?^:S2F(NR(JI1<3)'/G8D",R3K="G.;(2 3$ 
@R&5&\)Y2*'U<RQD(2-2$>2.< [I%OR3CJQ!LQW?:R-H 
@5Q:&[>DQ6D,$QT>:Q/OIV01*\IJ,P%F0,OLTH-O>5S, 
@_ZM6H#%(8 %JQ09^6)KM25;BSC?]G\O.=-3HA4?$1OP 
@^HU L3:C-U R>\N<7J)'$NH[]ZKWO989Y8TAQ ?PI7L 
@3>*H*^4Q3;GF26'N5DD2.3:"A\J0+5&9*/UEM_G!V<D 
@W^1;5?HK@*20JGD#%\$[(+.XE5(BA)&2U=R<<N^,;_$ 
@@:[C?B[?!*S*51?//Z342WEEB2(NIA9PD^);A.%"F#D 
@S;=QSJH"&,\_,)G;_D_;1_3R7/*IBS$[NLP08?NDR-, 
@#.$6MU-]$%*H+/2ORT/_7P_;FF9-QH-<<*;)[H&O'!T 
@Y>#&0NY.!OC_S;"4N!U"8O<;>C=2'ES1Q/)\!A/J23, 
@C_HH7DY^J-V?^/M[#=JV4. KW/+H&]CCS" .<KX-',P 
@GP+<CZZSZJA=&D*GV^+/GNX/ ]?H6%R)%<<T82YX$>8 
@&MFP9E;723+,V:K&/P>MFRY+U43;8;?R.66,LUC4?H( 
@QML/WQV1ZD/:=P@H8Q2),YH 6CJ*U<LYA7GE#0$%T?X 
@CZKRIM)2=HY:KQ\ALS>PDE*D-.@T'5CJ#9B(XS-80WH 
@,$$1DM62M')1%I_*)*&5PA+CZ9/J&FNOYWJ*/C5 QPX 
@;3R*4 [: Z#"_!**6W9-W4+K8O.T"%M$0;)5.:1#%G( 
@M:2#L^*$+?96PBJ>/CX]VHN^.T-NDP"K'O.0,BNARRL 
@UYM%<:K@[C^ZX9P$^SVB\"JS(SJ<D&'D$HK@OC0%*>T 
@,W-?)S50@30\Q\>[WB0KZ/CR=@6C6-@4F@JSP0L8:U@ 
@1$"IS^2\+R:IU\DMVHM&<[,L]ZXKXTQ2H7BY"@XE/L< 
@2JT.+:<H/>CDR4Q;=96C\V4')>]*8'$6,(#>'6Y9]=D 
@:?Y.1X7K+M]EW@844'+P=R!\2&$9_#7EB^8L5$9SGX\ 
@$<LQ$CAN_0%2IZ6)OYO8>#8307 =X.*:->2]<&#*31X 
@8J^.A:;0Y+5\:\!I,RG%3:GKIAT:-FTD10_&7+C!+N  
@[&Y],M=!6KU'=>!S4L_4[6OFH6=)TMDO%(NVO=B--$( 
@ED$"_SNW7U@ 5(3)9/!M%VVLO'V^9E;[@GP[XN>=EK< 
@1+JO1TG9C"0*D:<' /:A&2LZ>N<K;RAR_4,&#I37Y9H 
@IM3V&%*V-N)-G(T":&PN'BB(L+HDM1>Q-(TL!32[ 0( 
@?Y0MWV:0-;WG__WQX1GALW^_>J\WT7LVV7$XX)5%-)$ 
@SI\#/G-.H2,VI.P9%6D)D)V7RM4,K7T/T8P,^CG6I,H 
@Z:YQ4.AE<XGFV\SGF&$7<_/-V9Q<9W2;_+3G8#GCLPD 
@M_*9\_::#XOB&GI9QU2M#MTV3AMT2H39/^LB(C/J$K< 
@&UF:'K@"MO.SI^ R6J-7U1PI,[M<#*6 M\)>KHD5S6$ 
@NK;>BKA./3LD9'"8D7M/='8=!WQA.D23!&=JDWHTOJH 
@Y98C3!417&!.$\4*9UY('M#562D_^(MBA;(5K .!HT( 
@ A$ES?U<]S:CFJ;+ MKH7EO7/1:0%IA8<4+8><>6J]4 
@'/VZEM9<5)8YWT;6;QFCA@YZ)$EL;OO7LN Q7@PZ%]0 
@<"AR@@$>?C46'-DU!:=GN<Z Z[.W%.J38$[W*.--4*< 
@DS;=[3$G0R$A_X5W3B%6\XUV9 EG3V=M "H@&(T-Y@4 
@YA99B-0^U ]@T\+3U(DX),/]<R*GX2J9%F6F7@C7PE$ 
@*_6D%0 M8(RML.M0QEK=A18<@R8Z"52EPQQ0L ["Y_8 
@KSZE"VD#E$2/J<7=@M(M.ZA'.]OP7/JZ@8DT85CC:F@ 
@$/-Q,O@LUM"H5=!?T&C*K?O(HQ1?6I,O4!T$O).$@I( 
@+PP@;M6=45A/)+6\M="T@:79R$'JRX3)3/%PO>,_ST\ 
@7C(ASQ"_X(UR_IO<$'QUUE*3GTW_U&7G/3*.67NKVK\ 
@=L2Z"5EI']!3H3JOI:?Q'WFZ0\H/GI=SJS+;XRR\(5( 
@P87<2-4Z^[)BC_:6VYUM>CB T,:Q8<P'"7KG&A#^#YT 
@&S]LP-!JKZ_2JV-5BFAJ'IWBL139/"CL\S_0Z9!_S>0 
@,/ 2\+V[6F!*<B,D?(ZQ."/WHHHO0W16L]>RNL8KX@$ 
@UFA/+3A?H6B:?KM;0*Y%H=C::3+!H,#1%X;C-4]J;9, 
@3Z*Y'=*V=[Z/%#'!!=L\'X,D9IPEP:L?#W1V#955H*, 
@DP.]JZAWF@B<)Y3.,^)02]4K_78@2_.B"\YJM BE]Q( 
@JC^=AT+:,_0 93H8OM3A8[$B< ZS8I@3!&.+#!W)=I< 
@E4^L<OSNL43E>RF#=W8I6::U]<H:E8WNIS\J&[(3*3X 
@[)JS4K#OL08 >"\OWTDJ3"D994-E+AAF[D;'Q<W;CN8 
@.B*E@(^./V-C+HM=T)5]&1CGTT8]0FGKN>6JT8]ZR[L 
@2#^A3N#GM(,GKW&".J>X3S68@; X*#)?+_O7V".T#7P 
@]<Y)1(;W#>CGSGO&I#'*"]MQ;BY-+!L$W(J0L*@&<KX 
@F]3A)NJ5>VD?MF5Z5IB7)IO_S/IDK/ENX+JM4X=!484 
@@'#-W=JS5_)+9=N5^?@#FC6V<EV<"J3O-2+.PQ68+>8 
@,:A<V;J7W=Y6QRY12'6K'?:C'^ :Y*_)0?%M@(>@?_, 
@0H4JV[,LB6'DM<%&+?%B&[%\K/"K-6V14G+UU!,-M$4 
@W'<NY$8%?,_3L;$_;G/^J#O(?6XW5""O9)3SQN.T%*( 
@C*?TCB%KB=T"8X3"L)F=TY.@ QAP:6TQ+:$-DS^BN@8 
@T?7J>QV]=(B2T=S<H_L:@;B]BZE,#Z^?*(!LA[G; G, 
@";\XKD!7LXJ'JT3=E1;?\#ZS%]((7#\(DN5!'#/F-Z, 
@X/\+%9+&9[0'P_&.R;#SGB,,,(--2+JA9B[,%^B,NX< 
@JVL=(SY>FUO3D2BH@DG5M5JN[U+M*<Y[R')(6)2*!0H 
@/8NI7.$SY3+CL2$7AQZU*+-YS5K^#]#T<NSN1=D*B7( 
@B-#YY?BSWX-2/V'*5].3%/8$6G+M_QRT830I]NU"-;< 
@([SCIVA5]U,5+=X3EM+S&+9:-R+^J;F=8+.NE*5X%LX 
@/Z_EIK&?Q&SPFF@Y*>85L];1]11[\]([M&HWX:E#,_  
@,Z&TH'09I(AVCN4J"9< NSW75S+6/5&(780YE;BMFYD 
@_D[39#M.JB1L/'!]63>=$!?O+T\ % L7D\O2H&VZ4M$ 
@SHL^,;Q,'T:,Y?\:N&2:@#B$U("K=<HW1J,+UAE*0+L 
@991/JM(.!DEQP #AU@3._:%O!0F_!)M$J61)>LB/Q/$ 
@31D<]1^3C@/FV=5#-[3%&4 9=%:@IVB-XJHECG8A4)L 
@6!?NR!"N!;/(V!3ET?>N\LRR>OS-;)DW* $&IWOOEAD 
@D\=&QH7!%[LTKS^$2',;$7C[9(\?:=(CJ1OX1=\R2AP 
@CZ_H4[H4\ZI Q&=ASL0A_O#+Z5?&QL(;?Y+04:$I!7( 
@''C:J<9$.R(9N3J' .AG'/F_*P@7'L/*;'M]MI H]?, 
@V1:0;3_./+#SK5\TF:0M[$^0> TQI]3FQ/3\W"T<&PX 
@'C&BYTJJU\H6] I#R='Q"TQ_KJ05OF/)O2*"$\!L%XT 
01F)498"ST5ZR;KGC2@%\O   
`pragma protect end_protected
