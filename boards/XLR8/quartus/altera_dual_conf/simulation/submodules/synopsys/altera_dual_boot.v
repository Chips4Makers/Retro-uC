// (C) 2001-2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, Intel MegaCore Function License Agreement, or other
// applicable license agreement, including, without limitation,
// that your use is for the sole purpose of simulating designs
// for use exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
`pragma protect begin_protected
`pragma protect author="Altera"
`pragma protect key_keyowner="VCS"
`pragma protect key_keyname="VCS001"
`pragma protect key_method="VCS003"
`pragma protect encoding=(enctype="uuencode",bytes=200         )
`pragma protect key_block
H=4A*81VCI_Q%6O!0CLW?MYG;:&'^0)D5ORU@.(L4S6L*;"/^PW,A[P  
HD-@9;><H%90(H5AI"?B!<^M*7!1B".5%?'($T(Y0['H9#\<]A!SI(   
H-OD,**UK)D+WTO*NK_I?#3>&JD ZR<%7G\7R^U>8EOG.FZB2>3C/<0  
H9R;G2J>2+(*"RF6EWPD#60!OT\(CLPMT6,/>'(,H8*:O]K5^LO8!C@  
H\([&IN,N5EI;ZB+!N(^QQQI;%"1)T;&FPGLY)5DD/CQ?9#V"CRHHZ@  
`pragma protect encoding=(enctype="uuencode",bytes=1376        )
`pragma protect data_method="aes128-cbc"
`pragma protect data_block
@N@V0AS^U-7;])J_(4N2<V_&#5H+WA(HMD7<U#[YY[]D 
@FL/SUS@+K^07VGDWPVOFSNW.A !:TZ18(5)H$(_"P#P 
@3OLYN(M.EVP;\J=J-+(+GHI?03HN\[WVT/Q)+/<S0H, 
@/:SGU.W-:W-J<DF#IPFN#>"M"+\)%[CW>;[WWEE)Y#0 
@-)G)EFLC+:!B0B:ZN+*QY6YL+;',57J+<D"]GKFM%V< 
@VTK6J[8OL0MSBJ'./G%]:ULCKG)F!D:KZR<UM\$,34H 
@A(#V7 V<XJW!=?M6%PJ\1S.4&>UZT/^ '\O:[-/6W3< 
@T"7Y#$0URV\14G*!QNU89[)R.=[R='L-N!1-YH7)$9@ 
@0OLI'9&:(F4H 'JO,@G'L N5?1<?=H1[D?;.J3EXZ<P 
@]7@RO#TJ17%B[ERQS(LH';%,J" \:L^_ U[55 H[#G  
@-$)FML"E5_ #YJ7XR,CS*_A[%%G+#M>+$)W8466ZMPT 
@8V*99B0A)]1N>RV%,=^9/>)S$[^-T&5%(%'^Y.P?6HP 
@1L6M8)5<W'&H8P$8L#UI;:DT@;P$^<X08Z=#'=L7C?8 
@C'4<1^+._TVU\$[TM T9L59H'G-1O &$*0"([VH=;YT 
@_2I$.9_"QD4$P'QE)G<63& !FN MBV/^Z%,E^ Y^%8( 
@0*O<,:&RU,'#U7.F Z\$YLF4FJ>[K_:?Q,,D7-[Y2(L 
@G^]51N[OIX'W1TI5&>800&U>T=4_PFW=6&2&=1+3.#@ 
@L!W*FF7U _]F!KM5,VG;&P.THJ$P\1<:]4 @' 1_AB0 
@>9.SOP@N:,V2$TH"I?$N+R\#3VOPJ/H*3A&)=N>;(18 
@=2K-WVK [KR,?4ZYIP67L%72B]LCS4]D$HLK6J2+Y%P 
@.61>J#^;.GZRQ[I4!1WLG@:7A,SAR!2PC#XIT,GA."\ 
@7$;N0QA2HHR(O6O\+/P(_J1/^O2AW0EI\\G"WX=M8'H 
@A(CNE7Y)55^%%C)Q@N_2;X*S-HT)7CQ%[B[_[L<7Y&0 
@U<&1". AS7NC2^\OO1+I+KOOGB_:*8/*52?#\ Y!OE( 
@O[G\1\(4$:O8$>I^96%".@[XT9ZI#O\DN%1Y;/Q>[#  
@4]X% IU%GQ@^T%CL2E=N5<(79=/_*C52JP\VAY4$BS\ 
@E_(%3Y',=Z0G3%/OZ#Z*X7Q82J$IT$_-R9KR<_SX9=H 
@(<2IN0; <]38D HZ6)%+%), 94=F=4$:1J6C2!UL_'T 
@&6Z8-,>VIO,M\\BXDB/N4Q# [XQ I ^EPWQ3",3&@G< 
@ $-U^-%##S32^V-DSS_Z3MLUD, WM5F/F[??T[?]J[$ 
@:9L<%@C/">1H!0#%M^X-H"9%\O=2B5PKS)8Z#ERSAQ, 
@[!!C_KRAOU=(IZ"*IS=[6$*ZE<K+M1J_![9[4!7:Y^T 
@O?Y=L8U'CXJX<W=!" \Y2KBW*]9D&"J%2"\D R"8BI$ 
@OS! $-L<EZ<;IQQZV:=0Q._=!AP#Z26D\:%9+P_;W3P 
@I<_B8E1&YE2[J \,(9/&XA]'['3 \"U,M?[.T+=3IF8 
@&T;S>@*W6$=9&RLG%9NU?Q7,%A2#>_Y+].D]N_E[PQ4 
@"]BX^>'TX"XUU#3I'SV*; WCU^QX]0?MX.C?M@FZ2W8 
@J:]\:X<KW:NK])/Y)I[LU:KU;C2NAAX@*/*.@I]?, @ 
@]>G3.[HV)(TJENA 0\<"T;,/U['%):=L:I[Q&:<H#X, 
@Y^&C08RIQ?D/=G!:J38EVN7G24:A]%J3F#<E^=4'^]$ 
@KOWG#.5,[%G@X0C4F!S05(@8PD,<@[*SW9PFB'$@1Q< 
@MIW)TZE=?^YY?,R;KDP:Y&>R,J$,;&0!MB1S C(-XV, 
0'&EQ.#3UW)- 6Y%9K=A-L@  
0WNR.>?65J L >',='^MS>P  
`pragma protect end_protected
