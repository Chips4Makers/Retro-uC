# Retro-uC for XLR8 release __version__

This is release __version__ of the Retro-uC for the XLR8 board. It contains bitstream files to program the FPGA on the XLR8 board and demo files to run a small demo.

## Requirements

This release is for the XLR8 board so one needs to have that board. Currently in order to do anything useful with this release one also will need an FPGA programmer that supports .svf files. In order to see the output of the demo, LEDs need to be connected on pins D2-D9. This can be done on a small bread board or on an Arduino prototyping shield.

For loading programs on the Retro-uC or programming the FPGA the JTAG interface on the XLR8 board needs to be used.

The JTAG header on the XLR8 board has the following pin-out:

* JT1: TCK (external pull-up)
* JT2: GND
* JT3: TDO
* JT4: VREF
* JT5: TMS (external pull-up)
* JT6: TRST_N
* JT7: MAX10 JTAG enable
* JT8: 5V
* JT9: TDI (external pull-up)
* JT10: GND

In order to program the MAX10 FPGA JT7 has to be connected to 5V. When this pin is not connected or connected to ground the JTAG interface of the Retro-uC will be accessed.

## Bitstreams

These are located in the Bitstreams directory and are used to configure the XLR8 as a Retro-uC.

### Arduino programming file for XLR8 support in arduino IDE

The provided xlr8_16MHz_cfm1_auto.rpd can be used to program the XLR8 from the Arduino IDE. This has to be done as described by [OpenXLR8 documentation](http://www.aloriumtech.com/openxlr8/). Be aware that the Retro-uC does not implement the Arduino serial communication and therefor the Arduino IDE will not be able to program the XLR8 further after this core has been programmed. The 'connect to ground' method described in the XLR8 user manual has to be used to revert XLR8 to a state where it can be accessed through the Arduino IDE.

This file contains the Z80 and MOS6502 core and not the M68K. The M68K core is not compatible with the dual configuration of
the MAX10 used by the XLR8 to provide the revert to working state feature.

### JTAG Programmable Bitstreams

The FPGA on the XLR8 is not big enough to run a design with all three cores included. Therefor three different versions are provided with a combination of each core:

* Retro-uC: includes Z80 and MOS6502
* Retro-uC_T65M68K: includes MOS6502 and Motorola 68000
* Retro-uC_T80M68K: includes Z80 and Motorola 68000

Each of the versions is provided in four different formats:

* .sof: This a SRAM Object File used by Intel/Altera to program FPGA in a volatile way
* .pof: This is Programming Object File used by Intel/Altera to program FPGA in a non-volatile way
* _sram.svf: This is the equivalent of .sof in SVF format
* _flash.svf: This is the equivalent of .pof in SVF format

The bitstreams have to be programmed through an FPGA programming. The .sof and .pof files are to be used by the Intel/Altera tool included with Quartus and the Altera USB Blaster. The .svf files have to be programmed by an JTAG programmer that support these files. I am using OpenOCD together with a [Dangerous Prototypes BusPirate V3.6](http://dangerousprototypes.com/docs/Bus_Pirate). The playsvf.sh script as discussed in the Demo section could also be used with the JT7 pin put high.

Be aware that using the .pof or the _flash.svf formats for programming the FPGA will overwrite also the clean backup Arduino image and thus the 'connect to ground' method for recovering a working Arduino state will not work anymore. Use thus with care.

### Demo

In the Demo directory some .svf files are available showing of some of the features off the Retro-uC:

* blink_boundaryscan.svf: Blinking the LEDs using the JTAG boundary scan feature
* blink_memmap.svf: Blinking LEDs by Retro-uC JTAG commands that writes directly in the memory locations where the I/Os are mapped
* blink_m68k.svf: Blinking LEDs by running an M68K program
* blink_z80.svf: Blinking LEDs by running a Z80 program
* blink_mos6502.svf: Blinking LEDs by running a MOS6502 program

The ``playsvf.sh`` script I use for programming the Retro-uC is included. This uses a [Dangerous Prototypes BusPirate V3.6](http://dangerousprototypes.com/docs/Bus_Pirate) from OpenOCD (own compiled 0.10.0+dev-00146-g1025be3). The ``buspirate.cfg`` file is the config for OpenOCD to access the Bus Pirate. The script and the .cfg file need to be updated to your JTAG programmer used or your serial device with which the buspirate can be accessed.

After the everything is configured running the demo is very easy: ``./playsvf.sh __svf_file__`` for the selected demo. Of course when using the version for a certain CPU this CPU has to be present in the current configuration used on the XLR8 board.

## Source code

The source of this demo can be found on the [Retro-uC gitlab repo](https://gitlab.com/Chips4Makers/Retro-uC) in the [XLR8_v__version__ tag](https://gitlab.com/Chips4Makers/Retro-uC/tags/XLR8_v__version__). In boards/XLR8/demo the source for the Blink leds is available together with a compile script. This should be used as starting point if one wants to develop own programs for the Retro-uC.
