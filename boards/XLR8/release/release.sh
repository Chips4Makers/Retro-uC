#!/bin/sh

v=20180617
top=Retro-uC_XLR8_v$v

mkdir $top

d=$top/Bitstreams
mkdir $d
s=../quartus/output_files
cp $s/Retro-uC.sof $s/Retro-uC_T65M68K.sof $s/Retro-uC_T80M68K.sof $d
cp $s/Retro-uC.pof $s/Retro-uC_T65M68K.pof $s/Retro-uC_T80M68K.pof $d
cp $s/Retro-uC_sram.svf $s/Retro-uC_T65M68K_sram.svf $s/Retro-uC_T80M68K_sram.svf $d
cp $s/Retro-uC_flash.svf $s/Retro-uC_T65M68K_flash.svf $s/Retro-uC_T80M68K_flash.svf $d
s=../quartus/XLR8Build/extras/quartus/output_files
cp $s/xlr8_16MHz_cfm1_auto.rpd $d

d=$top/Demo
mkdir $d
s=../demo
cp $s/blink_boundaryscan.svf $d
cp $s/blink_memmap.svf $d
cp $s/leds_c/blink_m68k.svf $s/leds_c/blink_z80.svf $s/leds_c/blink_mos6502.svf $d

sed -e "s/__version__/$v/g" < README.md > $top/README.md

o=../../../release/$top.tar.xz
tar cJvf $o $top
rm -fr $top
ls -lh $o
