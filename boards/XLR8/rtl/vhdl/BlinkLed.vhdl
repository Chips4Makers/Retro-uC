-- Debug design that only blinks the LED

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_XLR8 is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;
    
    RX:         inout std_logic;
    TX:         inout std_logic;
    D2:         inout std_logic;
    D3:         inout std_logic;
    D4:         inout std_logic;
    D5:         inout std_logic;
    D6:         inout std_logic;
    D7:         inout std_logic;
    D8:         inout std_logic;
    D9:         inout std_logic;
    D10:        inout std_logic;
    D11:        inout std_logic;
    D12:        inout std_logic;
    D13:        inout std_logic;

    -- On Arduino D13 and LED are the same
    -- XLR8 separates them
    PIN13LED:   out std_logic;

    SDA:        inout std_logic;
    SCL:        inout std_logic;
    -- Enable pullups on sda/scl
    -- '0'=disable, '1'=enable
    I2C_ENABLE: out std_logic;

    A0:         inout std_logic;
    A1:         inout std_logic;
    A2:         inout std_logic;
    A3:         inout std_logic;
    A4:         inout std_logic;
    A5:         inout std_logic;
    -- Disconnect Ana_Dig from ADC input
    -- Put to '0' to disconnect, otherwise put to 'Z'
    DIG_IO_OE:  inout std_logic_vector(5 downto 0);
    -- Choose ADC ref
    -- '1'=AREF, '0'=regulated 3.3V
    ANA_UP:     out std_logic;

    -- XLR8 JTAG header
    JT1:        inout std_logic; -- external pulldown, TCK
    --JT2: GND
    JT3:        inout std_logic; -- TDO
    --JT4: VREF
    JT5:        inout std_logic; -- external pullup, TMS
    JT6:        inout std_logic; -- TRST_N (optional)
    JT7:        inout std_logic; -- NC
    --JT8: 5V
    JT9:        inout std_logic; -- external pullup, TDI
    --JT10: GND

    -- XLR8 SOIC-8 spot
    SOIC1:      inout std_logic;
    SOIC2:      inout std_logic;
    SOIC3:      inout std_logic;
    --SOIC4
    SOIC5:      inout std_logic;
    SOIC6:      inout std_logic;
    SOIC7:      inout std_logic
    --SOIC8
  );
end Retro_uC_XLR8;

architecture rtl of Retro_uC_XLR8 is
  signal S_PIN13LED:    std_logic;
  -- blinking LED
  signal blinkcounter:  unsigned(22 downto 0);
begin
  -- Blink LED13PIN @ 1Hz to indicate custom config
  process (Clock, RESET_N)
  begin
    if RESET_N = '0' then
      blinkcounter <= to_unsigned(1, 23);
      S_PIN13LED <= '0';
    elsif Rising_Edge(Clock) then
      blinkcounter <= blinkcounter + 1;
      if blinkcounter = 0 then
        S_PIN13LED <= not S_PIN13LED;
      end if;
    end if;
  end process;
  PIN13LED <= S_PIN13LED;
end rtl;
