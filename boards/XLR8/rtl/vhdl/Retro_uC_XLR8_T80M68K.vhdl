-- Top Retro_uC block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.c4m_jtag.ALL;

entity Retro_uC_XLR8 is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;
    
    RX:         inout std_logic;
    TX:         inout std_logic;
    D2:         inout std_logic;
    D3:         inout std_logic;
    D4:         inout std_logic;
    D5:         inout std_logic;
    D6:         inout std_logic;
    D7:         inout std_logic;
    D8:         inout std_logic;
    D9:         inout std_logic;
    D10:        inout std_logic;
    D11:        inout std_logic;
    D12:        inout std_logic;
    D13:        inout std_logic;

    -- On Arduino D13 and LED are the same
    -- XLR8 separates them
    PIN13LED:   out std_logic;

    SDA:        inout std_logic;
    SCL:        inout std_logic;
    -- Enable pullups on sda/scl
    -- '0'=disable, '1'=enable
    I2C_ENABLE: out std_logic;

    A0:         inout std_logic;
    A1:         inout std_logic;
    A2:         inout std_logic;
    A3:         inout std_logic;
    A4:         inout std_logic;
    A5:         inout std_logic;
    -- Disconnect Ana_Dig from ADC input
    -- Put to '0' to disconnect, otherwise put to 'Z'
    DIG_IO_OE:  inout std_logic_vector(5 downto 0);
    -- Choose ADC ref
    -- '1'=AREF, '0'=regulated 3.3V
    ANA_UP:     out std_logic;

    -- XLR8 JTAG header
    JT1:        inout std_logic; -- external pulldown, TCK
    --JT2: GND
    JT3:        inout std_logic; -- TDO
    --JT4: VREF
    JT5:        inout std_logic; -- external pullup, TMS
    JT6:        inout std_logic; -- TRST_N (optional)
    JT7:        inout std_logic; -- NC
    --JT8: 5V
    JT9:        inout std_logic; -- external pullup, TDI
    --JT10: GND

    -- XLR8 SOIC-8 spot
    SOIC1:      inout std_logic;
    SOIC2:      inout std_logic;
    SOIC3:      inout std_logic;
    --SOIC4
    SOIC5:      inout std_logic;
    SOIC6:      inout std_logic;
    SOIC7:      inout std_logic
    --SOIC8
  );
end Retro_uC_XLR8;

architecture rtl of Retro_uC_XLR8 is
  -- IO signal for TAP
  -- 0-13: DIG
  -- 14-19: ANA
  -- 20/21: SCL/SDA
  -- other signal not currently handled by JTAG, directly connected
  constant IOS:         integer := 22;
  signal IO_IN:         std_logic_vector(IOS-1 downto 0);
  signal IO_OUT:        std_logic_vector(IOS-1 downto 0);
  signal IO_EN:         std_logic_vector(IOS-1 downto 0);

  -- External JTAG signals
  signal TRST_N:        std_logic;
  signal TCK:           std_logic;
  signal TMS:           std_logic;
  signal TDI:           std_logic;
  signal TDO:           std_logic;

  -- On Arduino UNO D13 is also the on board LED
  -- On XLR8 it can be driven separately
  -- We use it to indicate JTAG/debug state
  signal S_PIN13LED:    std_logic;
  -- blinking LED
  signal blinkcounter:  unsigned(22 downto 0);
begin
  -- Alias/convert the inputs/outputs
  IO_IN(0) <= RX;
  RX <= IO_OUT(0) when IO_EN(0) = '1' else 'Z';
  IO_IN(1) <= TX;
  TX <= IO_OUT(1) when IO_EN(1) = '1' else 'Z';
  IO_IN(2) <= D2;
  D2 <= IO_OUT(2) when IO_EN(2) = '1' else 'Z';
  IO_IN(3) <= D3;
  D3 <= IO_OUT(3) when IO_EN(3) = '1' else 'Z';
  IO_IN(4) <= D4;
  D4 <= IO_OUT(4) when IO_EN(4) = '1' else 'Z';
  IO_IN(5) <= D5;
  D5 <= IO_OUT(5) when IO_EN(5) = '1' else 'Z';
  IO_IN(6) <= D6;
  D6 <= IO_OUT(6) when IO_EN(6) = '1' else 'Z';
  IO_IN(7) <= D7;
  D7 <= IO_OUT(7) when IO_EN(7) = '1' else 'Z';
  IO_IN(8) <= D8;
  D8 <= IO_OUT(8) when IO_EN(8) = '1' else 'Z';
  IO_IN(9) <= D9;
  D9 <= IO_OUT(9) when IO_EN(9) = '1' else 'Z';
  IO_IN(10) <= D10;
  D10 <= IO_OUT(10) when IO_EN(10) = '1' else 'Z';
  IO_IN(11) <= D11;
  D11 <= IO_OUT(11) when IO_EN(11) = '1' else 'Z';
  IO_IN(12) <= D12;
  D12 <= IO_OUT(12) when IO_EN(12) = '1' else 'Z';
  IO_IN(13) <= D13;
  D13 <= IO_OUT(13) when IO_EN(13) = '1' else 'Z';

  IO_IN(14) <= A0;
  A0 <= IO_OUT(14) when IO_EN(14) = '1' else 'Z';
  IO_IN(15) <= A1;
  A1 <= IO_OUT(15) when IO_EN(15) = '1' else 'Z';
  IO_IN(16) <= A2;
  A2 <= IO_OUT(16) when IO_EN(16) = '1' else 'Z';
  IO_IN(17) <= A3;
  A3 <= IO_OUT(17) when IO_EN(17) = '1' else 'Z';
  IO_IN(18) <= A4;
  A4 <= IO_OUT(18) when IO_EN(18) = '1' else 'Z';
  IO_IN(19) <= A5;
  A5 <= IO_OUT(19) when IO_EN(19) = '1' else 'Z';

  IO_IN(20) <= SCL;
  SCL <= IO_OUT(20) when IO_EN(20) = '1' else 'Z';
  IO_IN(21) <= SDA;
  SDA <= IO_OUT(21) when IO_EN(21) = '1' else 'Z';

  -- Forget about ADC for now
  DIG_IO_OE <= "000000";
  ANA_UP <= '0';

  -- Do not enable I2C pull-up
  I2C_ENABLE <= '0';
  
  JT1 <= 'Z';
  TCK <= JT1;
  JT3 <= TDO;
  JT5 <= 'Z';
  TMS <= JT5;
  JT6 <= 'Z';
  JT7 <= 'Z';
  JT9 <= 'Z';
  TDI <= JT9;
  -- No separate reset, use RESET_N as SRST of JTAG
  TRST_N <= RESET_N; 
  
  -- No SOIC connection
  SOIC1 <= 'Z';
  SOIC2 <= 'Z';
  SOIC3 <= 'Z';
  SOIC5 <= 'Z';
  SOIC6 <= 'Z';
  SOIC7 <= 'Z';

  -- Instantiate the Retro-uC top cell
  Top: entity work.Retro_uC_top
    generic map (
      IOS => 22,
      WITH_T65 => false
    )
    port map (
      Clock => Clock,
      RESET_N => RESET_N,

      M68K_Enable => A0,
      T80_Enable => A1,
      T65_Enable => A2,

      TRST_N => TRST_N,
      TCK => TCK,
      TMS => TMS,
      TDI => TDI,
      TDO => TDO,

      IO_IN => IO_IN,
      IO_OUT => IO_OUT,
      IO_EN => IO_EN
    );

  -- Blink LED13PIN @ 1Hz to indicate custom config
  process (Clock, RESET_N)
  begin
    if RESET_N = '0' then
      blinkcounter <= to_unsigned(1, 23);
      S_PIN13LED <= '0';
    elsif Rising_Edge(Clock) then
      blinkcounter <= blinkcounter + 1;
      if blinkcounter = 0 then
        S_PIN13LED <= not S_PIN13LED;
      end if;
    end if;
  end process;
  PIN13LED <= S_PIN13LED;
end rtl;
