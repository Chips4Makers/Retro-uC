library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_XLR8_tb is
end Retro_uC_XLR8_tb;

architecture rtl of Retro_uC_XLR8_tb is
  signal Clock:         std_logic;
  signal RESET_N:       std_logic;

  shared variable Running: boolean := true;
  constant Period:      time := 1us;
begin
  Top: entity work.Retro_uC_XLR8
    generic map (
      WITH_DUAL_CONFIG => false
    )
    port map (
      Clock => Clock,
      RESET_N => RESET_N
    );

  Reset: process
  begin
    RESET_N <= '0';
    wait for 1.75*Period;
    RESET_N <= '1';
    wait;
  end process;

  Clk: process
  begin
    if Running then
      Clock <= '0';
      wait for 0.5*Period;
      Clock <= '1';
      wait for 0.5*Period;
    else
      wait;
    end if;
  end process;

  Run: process
  begin
    wait for 10.25*Period;
    Running := false;
    wait;
  end process;
end architecture rtl;

