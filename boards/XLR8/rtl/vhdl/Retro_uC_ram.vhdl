-- The Retro_uC RAM for the XLR8 board

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Retro_uC_RAM is
  port (
    Clock:      in std_logic;
    CE:         in std_logic;
    WE:         in std_logic;
    Address:    in std_logic_vector(12 downto 0);
    Data_In:    in std_logic_vector(7 downto 0);
    Data_Out:   out std_logic_vector(7 downto 0)
  );
end entity Retro_uC_RAM;

architecture rtl of Retro_uC_RAM is
begin
  XLR8RAM: entity work.xlr8_ram
    port map (
      address => Address,
      clken => CE,
      clock => Clock,
      data => Data_In,
      wren => WE,
      q => Data_Out
    );
end architecture rtl;
