This folder is to generate the top level verilog RTL code to upload to efabless CloudV.
The top level cell for cloudv is in rtl/vhdl, the output verilog will be stored in
rtl/verilog and also commit to git.
This way people without Verific can synthesize from the verilog.
