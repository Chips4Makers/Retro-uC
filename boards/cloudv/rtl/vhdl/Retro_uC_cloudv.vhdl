-- Top Retro_uC block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.c4m_jtag.ALL;

entity Retro_uC_CloudV is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- Configuration
    ENZ80:      in std_logic;
    EN6502:     in std_logic;
    ENM68K:     in std_logic;
    I2CBOOT:    in std_logic;
    
    -- JTAG interface
    TRST_N:     in std_logic;
    TCK:        in std_logic;
    TMS:        in std_logic;
    TDI:        in std_logic;
    TDO:        out std_logic;

    -- 73 GPIOs
    IO_IN:     in std_logic_vector(72 downto 0);
    IO_OUT:    out std_logic_vector(72 downto 0);
    IO_EN:     out std_logic_vector(72 downto 0)
  );
end Retro_uC_CloudV;

architecture rtl of Retro_uC_CloudV is
begin
  -- Instantiate the Retro-uC top cell
  Top: entity work.Retro_uC_top
    generic map (
      IOS => 73
    )
    port map (
      Clock => Clock,
      RESET_N => RESET_N,

      T80_Enable => ENZ80,
      T65_Enable => EN6502,
      -- XXX => ENM68K,
      -- XXX => I2CBOOT,
      
      TRST_N => TRST_N,
      TCK => TCK,
      TMS => TMS,
      TDI => TDI,
      TDO => TDO,

      IO_IN => IO_IN,
      IO_OUT => IO_OUT,
      IO_EN => IO_EN
    );
end rtl;
