#!/bin/env python3
from migen import *
from migen.fhdl.verilog import convert
from misoc.interconnect import wishbone, csr_bus, wishbone2csr
from misoc.interconnect.csr import AutoCSR, CSRStorage

class CSRNode(Module, AutoCSR):
    def __init__(self):
        self._value = CSRStorage(4)

class Top(Module):
    def __init__(self):
        self.submodules.node1 = CSRNode()
        self.submodules.node2 = CSRNode()

        self.submodules.wishbone2csr = wishbone2csr.WB2CSR(
            bus_csr=csr_bus.Interface(8, 14)
        )
        self.submodules.csrbankarray = csr_bus.CSRBankArray(
            self, self.get_csr_dev_address,
            data_width=8, address_width=14
        )
        #self.buses = self.csrbankarray.get_buses()
        #self.submodules.csrcon = csr_bus.Interconnect(
        #    self.wishbone2csr.csr, self.buses
        #)

    def get_csr_dev_address(self, name, memory):
        if not hasattr(self, "csr_index"):
            self.csr_index = 0
        else:
            self.csr_index += 1
        return self.csr_index

    def verilog(self, filename="CSRTest.v", asic_syntax=True):
        convert(self, set(), name="CSRTest", asic_syntax=asic_syntax).write(filename)

if __name__ == "__main__":
    Top().verilog()
