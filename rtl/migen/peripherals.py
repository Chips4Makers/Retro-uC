#!/bin/env python3
from types import SimpleNamespace

from migen import *
from migen.fhdl.verilog import convert
from migen.fhdl.tracer import get_obj_var_name
from migen.fhdl.specials import Special

from misoc.cores.uart import RS232PHY, UART
from misoc.cores.spi2 import SPIInterface, SPIMaster
from misoc.cores.i2c import I2CMaster
from misoc.cores.timer import Timer
from misoc.cores.comparator import Comparator
from misoc.interconnect import wishbone, csr_bus, wishbone2csr
from misoc.integration.wb_slaves import WishboneSlaveManager

class DumpVCD(Special):
    @staticmethod
    def emit_verilog(dumpvcd, ns, add_data_file):
        r = 'initial begin\n' + \
            '\t$dumpfile("Peripherals.vcd");\n' + \
            '\t$dumpvars;\n' + \
            'end\n\n'
        return r

class TSRecord(Record):
    def __init__(self, fieldnames, ios):
        lay_ts = [("o", 1), ("oe", 1), ("i", 1)]
        lay = []
        for fieldname in fieldnames:
            lay.append((fieldname, lay_ts))
        Record.__init__(self, lay)
        for fieldname in fieldnames:
            ts = getattr(self, fieldname)
            ios |= {ts.i, ts.o, ts.oe}

class Peripherals(Module):
    def __init__(self, clk_frequency, data_width=8, address_width=16):
        self.data_width = data_width
        self.address_width = address_width

        self.specials += DumpVCD()

        ios = set()

        # Timer + PWM; 2 timers driving 4 comparators each
        pwm = Signal(8)
        for i in range(8):
            if (i % 4) == 0:
                timer = Timer(16,8)
                setattr(self.submodules, "timer{}".format(i//4), timer)
            _pwm = Comparator(timer.count)
            setattr(self.submodules, "pwm{}".format(i), _pwm)
            self.comb += pwm[i].eq(_pwm.comp)
        ios |= {pwm}

        # UART
        r = Record([("rx", 1), ("tx", 1)], "uart")
        phy = RS232PHY(r, clk_frequency)
        self.submodules.uart_phy = phy
        self.submodules.uart = UART(phy)
        ios |= {r.rx, r.tx}

        # SPI
        spi_port = TSRecord(["cs_n", "clk", "miso", "mosi"], ios)
        self.submodules.spi_interface = interface = SPIInterface(spi_port)
        self.submodules.spi = SPIMaster(interface, 8)

        # I2C
        i2c_port = TSRecord(["scl", "sda"], ios)
        wb2 = wishbone.Interface(data_width=32, address_width=address_width)
        self.submodules.i2c = i2c = I2CMaster(i2c_port, bus=wb2)
        # Temporary add I2C Wishbone interface to the ios
        ios |= {
            wb2.adr, wb2.dat_w, wb2.dat_r, wb2.sel,
            wb2.cyc, wb2.stb, wb2.ack, wb2.we,
            wb2.cti, wb2.bte
        }
        
        # CSR
        # Currently page size is hardcoded to 512 bytes
        # Make the addres 14 bits wide
        wb = wishbone.Interface(
            data_width=data_width, address_width=address_width
        )
        self.submodules.wishbone2csr = wishbone2csr.WB2CSR(
            bus_wishbone=wb,
            bus_csr=csr_bus.Interface(data_width, address_width)
        )
        self.submodules.csrbankarray = csr_bus.CSRBankArray(
            self, self.get_csr_dev_address,
            data_width=data_width, address_width=address_width
        )
        self.submodules.csrcon = csr_bus.Interconnect(
            self.wishbone2csr.csr, self.csrbankarray.get_buses()
        )
        ios |= {
            wb.adr, wb.dat_w, wb.dat_r, wb.sel,
            wb.cyc, wb.stb, wb.ack, wb.we,
            wb.cti, wb.bte
        }

        self.ios = ios

    def get_csr_dev_address(self, name, memory):
        if not hasattr(self, "csr_index"):
            # Put CSR in top half of memory; 9 bits are inside page
            self.csr_index = 2**(self.address_width - 9 - 1)
        else:
            self.csr_index += 1
        return self.csr_index

    def verilog(self, filename="Peripherals.v", asic_syntax=True):
        convert(self, self.ios, name="Peripherals", asic_syntax=asic_syntax).write(filename)

    def memmap(self, filename="Peripherals.map"):
        lines = []
        for bank in self.csrbankarray.banks:
            address = bank[2]*512
            lines.append("\n{} (0x{:X})\n".format(bank[0], address))
            for csr in bank[1]:
                lines.append(
                    "0x{:X}: {} ({} bit{})\n".format(
                        address, csr.name, csr.size,
                        "s" if csr.size > 1 else ""
                    )
                )
                try:
                    address += len(csr.simple_csrs)
                except:
                    address += 1

        f = open(filename, "w")
        f.writelines(lines)
        f.close()

if __name__ == "__main__":
    freq = 10*1000*1000
    ps = Peripherals(freq)
    #ps.verilog()
    ps.verilog(asic_syntax=False)
    ps.memmap()
