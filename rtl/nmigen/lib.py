# This is collection of local classes that are meant to be upstreamed or be replaced
# with better upstream implementation.

from nmigen import *

from nmigen_soc.wishbone.bus import Interface


class Wishbone2SRAM(Elaboratable):
    def __init__(self, *, data_width, addr_width, granularity=None):
        self.bus = bus = Interface(data_width=data_width, addr_width=addr_width, granularity=granularity,
                                   features=("stall", "lock"))

        self.ce = Signal()
        self.addr = Signal(addr_width)
        self.d = Record((("i", data_width), ("o", data_width)))
        self.we = Signal(len(self.bus.sel))


    def elaborate(self, platform):
        wb = self.bus

        m = Module()

        m.d.comb += [
            self.ce.eq(wb.cyc & wb.stb),
            self.addr.eq(wb.adr),
            wb.dat_r.eq(self.d.i),
            self.d.o.eq(wb.dat_w),
            self.we.eq(Cat(wb.cyc & wb.stb & wb.we & sel for sel in wb.sel)),
        ]

        return m
