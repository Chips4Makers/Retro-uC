from nmigen import *
from nmigen.lib.io import *

from nmigen_soc.wishbone.bus import Interface
from ao68000.nmigen import ao68000
from t80.nmigen import Z80_WB
from t65.nmigen import MOS6502_WB

__all__ = ["Arbiter", "Arbiter8", "MemoryMap", "Retro_uC"]


class Arbiter(Elaboratable):
    """The Retro-uC arbiter"""

    def __init__(self, ext_wb, m68k_wb, z80_wb, mos6502_wb, memmap_wb):
        self._ext_wb = ext_wb
        self._m68k_wb = m68k_wb
        self._z80_wb = z80_wb
        self._mos6502_wb = mos6502_wb
        self._memmap_wb = memmap_wb


    def elaborate(self, platform):
        def drive8(wb, wb8):
            dat_r_split = Array([wb.dat_r[i*8:(i+1)*8] for i in range(4)])
            stmnts_down = [
                wb.adr.eq(wb8.adr[2:]),
                wb.dat_w.eq(Cat(wb8.dat_w for _ in range(4))),
                wb.sel.eq(Const(1,4) << wb8.adr[:2]),
                wb.we.eq(wb8.we),
                wb.cyc.eq(wb8.cyc),
                wb.stb.eq(wb8.stb),
            ]
            if hasattr(wb8, "lock"):
                stmnts_down.append(wb.lock.eq(wb8.lock))
            stmnts_up = [
                wb8.ack.eq(wb.ack & ~wb.stall),
                wb8.dat_r.eq(dat_r_split[wb8.adr[:2]]),
                wb8.stall.eq(wb.stall),
            ]
            return stmnts_down, stmnts_up

        def drive32(wb, wb32):
            stmnts_down = [
                wb.adr.eq(wb32.adr),
                wb.dat_w.eq(wb32.dat_w),
                wb.sel.eq(wb32.sel),
                wb.we.eq(wb32.we),
                wb.cyc.eq(wb32.cyc),
                wb.stb.eq(wb32.stb),
                wb.lock.eq(wb32.lock),
            ]
            stmnts_up = [
                wb32.ack.eq(wb.ack & ~wb.stall),
                wb32.dat_r.eq(wb.dat_r),
                wb32.stall.eq(wb.stall),
            ]
            return stmnts_down, stmnts_up


        m = Module()

        ext_wb = self._ext_wb
        m68k_wb = self._m68k_wb
        z80_wb = self._z80_wb
        mos6502_wb = self._mos6502_wb
        memmap_wb = self._memmap_wb

        ext_incycle = Signal()
        m68k_incycle = Signal()
        z80_incycle = Signal()
        mos6502_incycle = Signal()
        m.d.comb += [
            ext_incycle.eq(ext_wb.cyc & ext_wb.stb),
            m68k_incycle.eq(m68k_wb.cyc & m68k_wb.stb),
            z80_incycle.eq(z80_wb.cyc & z80_wb.stb),
            mos6502_incycle.eq(mos6502_wb.cyc & mos6502_wb.stb),
        ]

        ext_down, ext_up = drive32(memmap_wb, ext_wb)
        ext_stall_cyc = [ext_wb.stall.eq(ext_incycle)]
        ext_stall_ack = [ext_wb.stall.eq(~memmap_wb.ack)]

        m68k_down, m68k_up = drive32(memmap_wb, m68k_wb)
        m68k_stall_cyc = [m68k_wb.stall.eq(m68k_incycle)]
        m68k_stall_ack = [m68k_wb.stall.eq(~memmap_wb.ack)]

        z80_down, z80_up = drive8(memmap_wb, z80_wb)
        z80_stall_cyc = [z80_wb.stall.eq(z80_incycle)]
        z80_stall_ack = [z80_wb.stall.eq(~memmap_wb.ack)]

        mos6502_down, mos6502_up = drive8(memmap_wb, mos6502_wb)
        mos6502_stall_cyc = [mos6502_wb.stall.eq(mos6502_incycle)]
        mos6502_stall_ack = [mos6502_wb.stall.eq(~memmap_wb.ack)]

        # We support only operation in flight
        with m.FSM():
            with m.State("IDLE"):
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_up + m68k_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "EXT"
                with m.Elif(m68k_incycle):
                    m.d.comb += m68k_down + m68k_up + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "M68K"
                with m.Elif(z80_incycle):
                    m.d.comb += z80_down + z80_up + mos6502_stall_cyc
                    m.next = "Z80"
                with m.Elif(mos6502_incycle):
                    m.d.comb += mos6502_down + mos6502_up
                    m.next = "MOS6502"
                with m.Else():
                    m.d.comb += [
                        memmap_wb.we.eq(0),
                        memmap_wb.cyc.eq(0),
                        memmap_wb.stb.eq(0),
                        memmap_wb.lock.eq(0),
                    ]
            with m.State("EXT"):
                m.d.comb += ext_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + m68k_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                with m.Elif(m68k_incycle):
                    m.d.comb += m68k_down + m68k_stall_ack + ext_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "M68K"
                with m.Elif(z80_incycle):
                    m.d.comb += z80_down + z80_stall_ack + ext_stall_cyc + m68k_stall_cyc + mos6502_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "Z80"
                with m.Elif(mos6502_incycle):
                    m.d.comb += mos6502_down + mos6502_stall_ack + ext_stall_cyc + m68k_stall_cyc + z80_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "MOS6502"
                with m.Else():
                    m.d.comb += ext_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"
            with m.State("M68K"):
                m.d.comb += m68k_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + m68k_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "EXT"
                with m.Elif(m68k_incycle):
                    m.d.comb += m68k_down + m68k_stall_ack + ext_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                with m.Elif(z80_incycle):
                    m.d.comb += z80_down + z80_stall_ack + ext_stall_cyc + m68k_stall_cyc + mos6502_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "Z80"
                with m.Elif(mos6502_incycle):
                    m.d.comb += mos6502_down + mos6502_stall_ack + ext_stall_cyc + m68k_stall_cyc + z80_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "MOS6502"
                with m.Else():
                    m.d.comb += m68k_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"
            with m.State("Z80"):
                m.d.comb += z80_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + m68k_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "EXT"
                with m.Elif(m68k_incycle):
                    m.d.comb += m68k_down + m68k_stall_ack + ext_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "M68K"
                with m.Elif(z80_incycle):
                    m.d.comb += z80_down + z80_stall_ack + ext_stall_cyc + m68k_stall_cyc + mos6502_stall_cyc
                with m.Elif(mos6502_incycle):
                    m.d.comb += mos6502_down + mos6502_stall_ack + ext_stall_cyc + m68k_stall_cyc + z80_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "MOS6502"
                with m.Else():
                    m.d.comb += z80_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"
            with m.State("MOS6502"):
                m.d.comb += mos6502_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + m68k_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    m.next = "EXT"
                with m.Elif(m68k_incycle):
                    m.d.comb += m68k_down + m68k_stall_ack + ext_stall_cyc + z80_stall_cyc + mos6502_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "M68K"
                with m.Elif(z80_incycle):
                    m.d.comb += z80_down + z80_stall_ack + ext_stall_cyc + m68k_stall_cyc + mos6502_stall_cyc
                    with m.If(memmap_wb.ack):
                        m.next = "Z80"
                with m.Elif(mos6502_incycle):
                    m.d.comb += mos6502_down + mos6502_stall_ack + ext_stall_cyc + m68k_stall_cyc + z80_stall_cyc
                with m.Else():
                    m.d.comb += mos6502_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"

        return m


class Arbiter8(Elaboratable):
    """The Retro-uC arbiter"""

    def __init__(self, ext_wb, cpu_wb, memmap_wb):
        self._ext_wb = ext_wb
        self._cpu_wb = cpu_wb
        self._memmap_wb = memmap_wb


    def elaborate(self, platform):
        def drive(wb_slave, wb_master):
            stmnts_down = [
                wb_slave.adr.eq(wb_master.adr),
                wb_slave.dat_w.eq(wb_master.dat_w),
                wb_slave.we.eq(wb_master.we),
                wb_slave.cyc.eq(wb_master.cyc),
                wb_slave.stb.eq(wb_master.stb),
                wb_slave.lock.eq(wb_master.lock),
                wb_slave.sel.eq(wb_master.sel),
            ]
            stmnts_up = [
                wb_master.ack.eq(wb_slave.ack & ~wb_slave.stall),
                wb_master.dat_r.eq(wb_slave.dat_r),
                wb_master.stall.eq(wb_slave.stall),
            ]
            return stmnts_down, stmnts_up


        m = Module()

        ext_wb = self._ext_wb
        cpu_wb = self._cpu_wb
        memmap_wb = self._memmap_wb

        ext_incycle = Signal()
        cpu_incycle = Signal()
        m.d.comb += [
            ext_incycle.eq(ext_wb.cyc & ext_wb.stb),
            cpu_incycle.eq(cpu_wb.cyc & cpu_wb.stb),
        ]

        ext_down, ext_up = drive(memmap_wb, ext_wb)
        ext_stall_cyc = [ext_wb.stall.eq(ext_incycle)]
        ext_stall_ack = [ext_wb.stall.eq(~memmap_wb.ack)]

        cpu_down, cpu_up = drive(memmap_wb, cpu_wb)
        cpu_stall_cyc = [cpu_wb.stall.eq(cpu_incycle)]
        cpu_stall_ack = [cpu_wb.stall.eq(~memmap_wb.ack)]

        # We support only operation in flight
        with m.FSM():
            with m.State("IDLE"):
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_up + cpu_stall_cyc
                    m.next = "EXT"
                with m.Elif(cpu_incycle):
                    m.d.comb += cpu_down + cpu_up
                    m.next = "CPU"
                with m.Else():
                    m.d.comb += [
                        memmap_wb.we.eq(0),
                        memmap_wb.cyc.eq(0),
                        memmap_wb.stb.eq(0),
                        memmap_wb.lock.eq(0),
                    ]
            with m.State("EXT"):
                m.d.comb += ext_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + cpu_stall_cyc
                with m.Elif(cpu_incycle):
                    m.d.comb += cpu_down + cpu_stall_ack + ext_stall_cyc
                    m.next = "CPU"
                with m.Else():
                    m.d.comb += ext_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"
            with m.State("CPU"):
                m.d.comb += cpu_up
                with m.If(ext_incycle):
                    m.d.comb += ext_down + ext_stall_ack + cpu_stall_cyc
                    m.next = "EXT"
                with m.Elif(cpu_incycle):
                    m.d.comb += cpu_down + cpu_stall_ack + ext_stall_cyc
                with m.Else():
                    m.d.comb += cpu_down
                    with m.If(memmap_wb.ack):
                        m.next = "IDLE"

        return m


class MemoryMap(Elaboratable):
    """The Retro-uC memory map

    Args:
        mem_addr_width: width of on-chip RAM address for 32 bit words.
           mem_addr_width of 10 gives 1024 words of 32 bit on-chip RAM
        mem_wb: wishbone for alternative implementation of on-chip SRAM.
           If given, mem_addr_width will be ignored and the length of
           the address in mem_wb will be used. mem_wb has to immediately
           assert ack a stb and may not generate err or rty signal.
    """
    # TODO: support wait states for mem_wb ? or not ?

    def __init__(self, ios, *, mem_addr_width=13, mem_wb=None):
        assert(len(ios) > 0 and len(ios) <= 256)

        self.bus = Interface(
            data_width=32, addr_width=14, granularity=8, features=("stall", "lock")
        )

        ##

        self._ios = ios
        self._mem_addr_width = mem_addr_width
        self._mem_wb = mem_wb

    def elaborate(self, platform):
        wb = self.bus

        m = Module()

        #
        # On chip memory
        # Location: 0b00000000000000-0b01111111111111
        #
        if self._mem_wb is None:
            mem_addr_width = self._mem_addr_width
        else:
            mem_addr_width = len(self._mem_wb.adr)

        mem_isaddr = Signal()
        mem_iscycle = Signal()
        m.d.comb += [
            mem_isaddr.eq(wb.adr[mem_addr_width:] == 0),
            mem_iscycle.eq(mem_isaddr & wb.cyc & wb.stb),
        ]

        if self._mem_wb is None:
            mem_depth = 2**mem_addr_width
            mems = [Memory(width=8, depth=mem_depth) for _ in range(4)]
            memrps = [mem.read_port() for mem in mems]
            m.submodules += memrps
            memwps = [mem.write_port() for mem in mems]
            m.submodules += memwps

            m.d.comb += [
                mem_iscycle.eq((wb.adr[mem_addr_width:] == 0) & wb.cyc & wb.stb),
                *[port.addr.eq(wb.adr[:mem_addr_width]) for port in memrps + memwps],
                Cat(port.data for port in memwps).eq(wb.dat_w),
                Cat(port.en for port in memwps).eq(Cat(mem_iscycle & wb.we & sel for sel in wb.sel)),
            ]
        else:
            m.d.comb += [
                self._mem_wb.adr.eq(wb.adr[:mem_addr_width]),
                self._mem_wb.dat_w.eq(wb.dat_w),
                wb.dat_r.eq(self._mem_wb.dat_r),
                self._mem_wb.we.eq(wb.we),
                self._mem_wb.cyc.eq(wb.cyc & mem_isaddr),
                self._mem_wb.stb.eq(mem_iscycle),
                self._mem_wb.lock.eq(wb.lock),
            ]

        #
        # IOs
        # Location: 0b10000000000000-0b10000000001111
        #
        io_iscycle = Signal()
        io_isoe = Signal()
        io_reg = Signal(3)
        m.d.comb += [
            io_iscycle.eq((wb.adr[4:] == 0b1000000000) & wb.cyc & wb.stb),
            io_isoe.eq(wb.adr[3]),
            io_reg.eq(wb.adr[:3]),
        ]
        regs = (len(self._ios)-1)//32 + 1
        io_o = Array(Signal(32, name="io_o_{}".format(i)) for i in range(regs))
        io_i = Array(Signal(32, name="io_i_{}".format(i)) for i in range(regs))
        io_oe = Array(Signal(32, name="io_oe_{}".format(i)) for i in range(regs))
        for i, io in enumerate(self._ios):
            reg = i//32
            bit = i % 32
            if hasattr(io, "i"):
                m.d.comb += io_i[reg][bit].eq(io.i)
            else:
                m.d.comb += io_i[reg][bit].eq(0)
            if hasattr(io, "o"):
                m.d.comb += io.o.eq(io_o[reg][bit])
            if hasattr(io, "oe"):
                m.d.comb += io.oe.eq(io_oe[reg][bit])
        io_dat_r = Signal(32)
        with m.If(io_iscycle):
            with m.If(~io_isoe):
                m.d.sync += io_dat_r.eq(io_i[io_reg])
                with m.If(wb.we):
                    m.d.sync += io_o[io_reg].eq(wb.dat_w)
            with m.Else(): # select oe register
                m.d.sync += io_dat_r.eq(io_oe[io_reg])
                with m.If(wb.we):
                    m.d.sync += io_oe[io_reg].eq(wb.dat_w)
        with m.If(io_iscycle):
            m.d.sync += io_dat_r.eq(io_i[io_reg])

        #
        # High
        # Location: 0b1111111111111X
        #
        high_iscycle = Signal()
        m.d.comb += high_iscycle.eq((wb.adr[1:] == 0b1111111111111) & wb.cyc & wb.stb)
        # Currently have 0 for all MOS6502 reset vectors
        high_dat_r = Const(0,8)

        mem_iscycle_hold = Signal()
        io_iscycle_hold = Signal()
        high_iscycle_hold = Signal()
        m.d.sync += [
            mem_iscycle_hold.eq(mem_iscycle),
            io_iscycle_hold.eq(io_iscycle),
            high_iscycle_hold.eq(high_iscycle),
        ]
        with m.If(mem_iscycle_hold):
            if self._mem_wb is None:
                m.d.comb += wb.dat_r.eq(Cat(port.data for port in memrps))
            else:
                m.d.comb += wb.dat_r.eq(self._mem_wb.dat_r)
        with m.Elif(io_iscycle_hold):
            m.d.comb += wb.dat_r.eq(io_dat_r)
        with m.Elif(high_iscycle_hold):
            m.d.comb += wb.dat_r.eq(high_dat_r)
        m.d.sync += wb.ack.eq(mem_iscycle | io_iscycle | high_iscycle)

        return m


class MemoryMap8(Elaboratable):
    """The Retro-uC memory map

    Args:
        mem_addr_width: width of on-chip RAM address for 32 bit words.
           mem_addr_width of 10 gives 1024 words of 32 bit on-chip RAM
        mem_wb: wishbone for alternative implementation of on-chip SRAM.
           If given it mem_addr_width will be ignored and the length of
           the address in mem_wb will be used. mem_wb has to immediately
           assert ack a stb and may not generate err or rty signal.
    """
    # TODO: support wait states for mem_wb ? or not ?

    def __init__(self, ios, *, mem_addr_width=13, mem_wb=None):
        assert(len(ios) > 0 and len(ios) <= 256)

        self.bus = Interface(
            data_width=8, addr_width=16, features=("stall", "lock")
        )

        ##

        self._ios = ios
        self._mem_addr_width = mem_addr_width
        self._mem_wb = mem_wb

    def elaborate(self, platform):
        wb = self.bus
        
        m = Module()

        # connect sel
        m.d.comb += self._mem_wb.sel.eq(self.bus.sel)

        #
        # On chip memory
        # Location: 0b00000000000000-0b01111111111111
        #
        if self._mem_wb is None:
            mem_addr_width = self._mem_addr_width
        else:
            mem_addr_width = len(self._mem_wb.adr)

        mem_isaddr = Signal()
        mem_iscycle = Signal()
        m.d.comb += [
            mem_isaddr.eq(wb.adr[mem_addr_width:] == 0),
            mem_iscycle.eq(mem_isaddr & wb.cyc & wb.stb),
        ]

        if self._mem_wb is None:
            mem_depth = 2**mem_addr_width
            mems = [Memory(8, mem_depth) for _ in range(4)]
            memrps = [mem.read_port() for mem in mems]
            m.submodules += memrps
            memwps = [mem.write_port() for mem in mems]
            m.submodules += memwps

            m.d.comb += [
                mem_iscycle.eq((wb.adr[mem_addr_width:] == 0) & wb.cyc & wb.stb),
                *[port.addr.eq(wb.adr[:mem_addr_width]) for port in memrps + memwps],
                Cat(port.data for port in memwps).eq(wb.dat_w),
                Cat(port.en for port in memwps).eq(Cat(mem_iscycle & wb.we & sel for sel in wb.sel)),
            ]
        else:
            m.d.comb += [
                self._mem_wb.adr.eq(wb.adr[:mem_addr_width]),
                self._mem_wb.dat_w.eq(wb.dat_w),
                wb.dat_r.eq(self._mem_wb.dat_r),
                self._mem_wb.we.eq(wb.we),
                self._mem_wb.cyc.eq(wb.cyc & mem_isaddr),
                self._mem_wb.stb.eq(mem_iscycle),
                self._mem_wb.lock.eq(wb.lock),
            ]

        #
        # IOs
        # Location: 0b10000000000000-0b10000000001111
        #
        io_iscycle = Signal()
        io_isoe = Signal()
        io_reg = Signal(3)
        m.d.comb += [
            io_iscycle.eq((wb.adr[4:] == 0b1000000000) & wb.cyc & wb.stb),
            io_isoe.eq(wb.adr[3]),
            io_reg.eq(wb.adr[:3]),
        ]
        regs = (len(self._ios)-1)//32 + 1
        io_o = Array(Signal(32, name="io_o_{}".format(i)) for i in range(regs))
        io_i = Array(Signal(32, name="io_i_{}".format(i)) for i in range(regs))
        io_oe = Array(Signal(32, name="io_oe_{}".format(i)) for i in range(regs))
        for i, io in enumerate(self._ios):
            reg = i//32
            bit = i % 32
            m.d.comb += [
                io.o.eq(io_o[reg][bit]),
                io.oe.eq(io_oe[reg][bit]),
            ]
            if hasattr(io, "i"):
                m.d.comb += io_i[reg][bit].eq(io.i)
            else:
                m.d.comb += io_i[reg][bit].eq(0)
            if hasattr(io, "o"):
                m.d.comb += io.o.eq(io_o[reg][bit])
            if hasattr(io, "oe"):
                m.d.comb += io.oe.eq(io_oe[reg][bit])
        io_dat_r = Signal(32)
        with m.If(io_iscycle):
            with m.If(~io_isoe):
                m.d.sync += io_dat_r.eq(io_i[io_reg])
                with m.If(wb.we):
                    m.d.sync += io_o[io_reg].eq(wb.dat_w)
            with m.Else(): # select oe register
                m.d.sync += io_dat_r.eq(io_oe[io_reg])
                with m.If(wb.we):
                    m.d.sync += io_oe[io_reg].eq(wb.dat_w)
        with m.If(io_iscycle):
            m.d.sync += io_dat_r.eq(io_i[io_reg])


        mem_iscycle_hold = Signal()
        io_iscycle_hold = Signal()
        m.d.sync += [
            mem_iscycle_hold.eq(mem_iscycle),
            io_iscycle_hold.eq(io_iscycle),
        ]
        with m.If(mem_iscycle_hold):
            if self._mem_wb is None:
                m.d.comb += wb.dat_r.eq(Cat(port.data for port in memrps))
            else:
                m.d.comb += wb.dat_r.eq(self._mem_wb.dat_r)
        with m.Elif(io_iscycle_hold):
            m.d.comb += wb.dat_r.eq(io_dat_r)
        with m.Else():
            m.d.comb += wb.dat_r.eq(0)

        # Always ack cycle
        m.d.sync += wb.ack.eq(wb.cyc & wb.stb)

        return m


class Retro_uC(Elaboratable):
    """The Retro-uC top cell

    Args:
        ios (iterable of Pin): see :arg:`ios` of :class:`MemoryMap`
        ext_wishbone (Wishbone, optional): see :arg:`ext_wb` of :class:`Arbiter`
        onchipmem_addr_width (int, default=13): see :arg:`mem_addr_width` of :class:`MemoryMap`
        onchipmem_wb (Wishbone, optional): see :arg:`mem_wb` of :class:`MemoryMap`
        en_m68k (Signal, optional): signal to enable Motorola 68000 CPU, if signal is low
            the CPU will be in reset state, if not given the CPU will be constantly enabled.
        en_mos6502 (Signal, optional): signal to enable MOS6502 CPU, if signal is low
            the CPU will be in reset state, if not given CPU will be constantly enabled.
        en_z80 (Signal, optional): signal to enable Z80 CPU, if signal is low
            the CPU will be in reset state, if not given CPU will be constantly enabled.
    """

    def __init__(self, ios, *, ext_bus=None, onchipmem_addr_width=13, onchipmem_wb=None,
                 en_m68k=None, en_mos6502=None, en_z80=None):
        if ext_bus is None:
            self.ext_bus = Interface(
                data_width=32, addr_width=14, granularity=8, features=("stall", "lock")
            )
        else:
            self.ext_bus = ext_bus

        self._en_m68k = en_m68k
        self._en_mos6502 = en_mos6502
        self._en_z80 = en_z80

        self._ios = ios
        self._mem_addr_width = onchipmem_addr_width
        self._mem_wb = onchipmem_wb


    def elaborate(self, platform):
        m = Module()

        m.submodules.m68k = m68k = ao68000(with_stall=True)
        m.submodules.z80 = z80 = Z80_WB()
        m.submodules.mos6502 = mos6502 = MOS6502_WB()
        m.submodules.memmap = memmap = MemoryMap(
            self._ios, mem_addr_width=self._mem_addr_width, mem_wb=self._mem_wb,
        )
        if self._en_m68k is not None:
            m.d.comb += m68k.en.eq(self._en_m68k)
        if self._en_mos6502 is not None:
            m.d.comb += mos6502.en.eq(self._en_mos6502)
        if self._en_z80 is not None:
            m.d.comb += z80.en.eq(self._en_z80)

        m.submodules.arbiter = arbiter = Arbiter(
            self.ext_bus, m68k.bus, z80.bus, mos6502.bus, memmap.bus
        )


        return m


class Retro_uC_NoM68K(Elaboratable):
    """The Retro-uC top cell"""

    def __init__(self, ios, *, ext_bus=None, onchipmem_addr_width=13, onchipmem_wb=None):
        if ext_bus is None:
            self.ext_bus = Interface(
                data_width=32, addr_width=14, granularity=8, features=("stall", "lock")
            )
        else:
            self.ext_bus = ext_bus
        self.m68k_enable = Signal(reset=1)
        self.z80_enable = Signal(reset=1)
        self.mos6502_enable = Signal(reset=1)

        self._ios = ios
        self._mem_addr_width = onchipmem_addr_width
        self._mem_wb = onchipmem_wb


    def elaborate(self, platform):
        m = Module()

        #m.submodules.m68k = m68k = ao68000(with_stall=True)
        m.submodules.z80 = z80 = Z80_WB()
        m.submodules.mos6502 = mos6502 = MOS6502_WB()
        m.submodules.memmap = memmap = MemoryMap(
            self._ios, mem_addr_width=self._mem_addr_width, mem_wb=self._mem_wb,
        )

        # Fake Wishbone
        m68k_bus = Interface(
            data_width=32, addr_width=14, granularity=8, features=("stall", "lock")
        )
        m.d.comb += [
            m68k_bus.cyc.eq(0),
            m68k_bus.stb.eq(0),
        ]

        m.submodules.arbiter = arbiter = Arbiter(
            self.ext_bus, m68k_bus, z80.bus, mos6502.bus, memmap.bus
        )


        return m
