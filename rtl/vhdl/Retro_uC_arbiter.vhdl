-- The arbiter will connect the different buses and also init the memory
-- after reset.
-- The non-selected cores will be kept in reset state. At the moment only one core
-- will be able to be active.
-- When JTAG wants to do a cycle it gets the next cycle on the WB bus and the
-- selected core is kept waiting.

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_WBArbiter is
  port (
    RESET_N:            in std_logic;
    Clock:              in std_logic;

    Ctrl_WB_Adr:        in std_logic_vector(15 downto 0);
    Ctrl_WB_Dat_To:     out std_logic_vector(31 downto 0);
    Ctrl_WB_Dat_From:   in std_logic_vector(31 downto 0);
    Ctrl_WB_WE:         in std_logic;
    Ctrl_WB_Cyc:        in std_logic;
    Ctrl_WB_Stb:        in std_logic;
    Ctrl_WB_Ack:        out std_logic;
    Ctrl_WB_Err:        out std_logic;

    M68K_Enable:        in std_logic;
    M68K_Clock:         out std_logic;
    M68K_WB_Adr:        in std_logic_vector(15 downto 0);
    M68K_WB_Sel:        in std_logic_vector(3 downto 0);
    M68K_WB_Dat_To:     out std_logic_vector(31 downto 0);
    M68K_WB_Dat_From:   in std_logic_vector(31 downto 0);
    M68K_WB_WE:         in std_logic;
    M68K_WB_Cyc:        in std_logic;
    M68K_WB_Stb:        in std_logic;
    M68K_WB_Ack:        out std_logic;
    M68K_WB_Err:        out std_logic;

    T80_Enable:         in std_logic;
    T80_Clock:          out std_logic;
    T80_WB_Adr:         in std_logic_vector(15 downto 0);
    T80_WB_Dat_To:      out std_logic_vector(7 downto 0);
    T80_WB_Dat_From:    in std_logic_vector(7 downto 0);
    T80_WB_WE:          in std_logic;
    T80_WB_Cyc:         in std_logic;
    T80_WB_Stb:         in std_logic;
    T80_WB_Ack:         out std_logic;
    T80_WB_Err:         out std_logic;

    T65_Enable:         in std_logic;
    T65_Clock:          out std_logic;
    T65_WB_Adr:         in std_logic_vector(15 downto 0);
    T65_WB_Dat_To:      out std_logic_vector(7 downto 0);
    T65_WB_Dat_From:    in std_logic_vector(7 downto 0);
    T65_WB_WE:          in std_logic;
    T65_WB_Cyc:         in std_logic;
    T65_WB_Stb:         in std_logic;
    T65_WB_Ack:         out std_logic;
    T65_WB_Err:         out std_logic;

    Bus_BusMode:        out std_logic;
    Bus_WB_Adr:         out std_logic_vector(15 downto 0);
    Bus_WB_Dat_To:      out std_logic_vector(31 downto 0);
    Bus_WB_Dat_From:    in std_logic_vector(31 downto 0);
    Bus_WB_WE:          out std_logic;
    Bus_WB_Cyc:         out std_logic;
    Bus_WB_Sel:         out std_logic_vector(3 downto 0);
    Bus_WB_Stb:         out std_logic;
    Bus_WB_Ack:         in std_logic;
    Bus_WB_Err:         in std_logic
  );
end entity Retro_uC_WBArbiter;

architecture rtl of Retro_uC_WBArbiter is
  signal jtag_cycle:    boolean;
  signal jtag_cycle_hold: boolean;
begin
  -- Currently clock is distributed to all connected CPUs
  M68K_Clock <= Clock;
  T80_Clock <= Clock;
  T65_Clock <= Clock;

  process (all)
  begin
    jtag_cycle <= (Ctrl_WB_Cyc = '1') and (Ctrl_WB_Stb = '1');
    if RESET_N = '0' then
      jtag_cycle_hold <= false;
    elsif rising_edge(Clock) then
      jtag_cycle_hold <= jtag_cycle;
    end if;

    -- Do the WB bus multiplexing
    -- Priority order: JTAG, M68K, T80, T65
    if jtag_cycle then
      Bus_BusMode <= '1'; -- 32 bit

      Bus_WB_Adr <= Ctrl_WB_Adr;
      Bus_WB_Dat_To <= Ctrl_WB_Dat_From;
      Bus_WB_Cyc <= Ctrl_WB_Cyc;
      Bus_WB_Sel <= "1111";
      if not jtag_cycle_hold then
        -- Insert a wait cycle when switching from other bus to JTAG bus
        Bus_WB_Stb <= '0';
      else
        Bus_WB_Stb <= Ctrl_WB_Stb;
      end if;
      Bus_WB_WE <= Ctrl_WB_WE;

      Ctrl_WB_Dat_To <= Bus_WB_Dat_From;
      Ctrl_WB_Ack <= Bus_WB_Ack;
      Ctrl_WB_Err <= Bus_WB_Err;

      M68K_WB_Dat_To <= (others => 'X');
      M68K_WB_Ack <= '0';
      M68K_WB_Err <= '0';

      T80_WB_Dat_To <= (others => 'X');
      T80_WB_Ack <= '0';
      T80_WB_Err <= '0';

      T65_WB_Dat_To <= (others => 'X');
      T65_WB_Ack <= '0';
      T65_WB_Err <= '0';
    elsif jtag_cycle_hold then -- Insert wait cycle after JTAG cycle has ended
      Bus_BusMode <= '1'; -- 32 bit

      Bus_WB_Adr <= Ctrl_WB_Adr;
      Bus_WB_Dat_To <= Ctrl_WB_Dat_From;
      Bus_WB_Cyc <= Ctrl_WB_Cyc;
      Bus_WB_Sel <= "1111";
      Bus_WB_Stb <= '0';
      Bus_WB_WE <= '0';

      Ctrl_WB_Dat_To <= Bus_WB_Dat_From;
      Ctrl_WB_Ack <= Bus_WB_Ack;
      Ctrl_WB_Err <= Bus_WB_Err;

      M68K_WB_Dat_To <= (others => 'X');
      M68K_WB_Ack <= '0';
      M68K_WB_Err <= '0';

      T80_WB_Dat_To <= (others => 'X');
      T80_WB_Ack <= '0';
      T80_WB_Err <= '0';

      T65_WB_Dat_To <= (others => 'X');
      T65_WB_Ack <= '0';
      T65_WB_Err <= '0';
    elsif M68K_Enable = '1' then
      Bus_BusMode <= '1'; -- 32 bit

      Bus_WB_Adr <= M68K_WB_Adr;
      Bus_WB_Dat_To <= M68K_WB_Dat_From;
      Bus_WB_Cyc <= M68K_WB_Cyc;
      Bus_WB_Sel <= M68K_WB_Sel;
      Bus_WB_Stb <= M68K_WB_Stb;
      Bus_WB_WE <= M68K_WB_WE;

      Ctrl_WB_Dat_To <= (others => 'X');
      Ctrl_WB_Ack <= '0';
      Ctrl_WB_Err <= '0';

      M68K_WB_Dat_To <= Bus_WB_Dat_From;
      M68K_WB_Ack <= Bus_WB_Ack;
      M68K_WB_Err <= Bus_WB_Err;

      T80_WB_Dat_To <= (others => 'X');
      T80_WB_Ack <= '0';
      T80_WB_Err <= '0';

      T65_WB_Dat_To <= (others => 'X');
      T65_WB_Ack <= '0';
      T65_WB_Err <= '0';
    elsif T80_Enable = '1' then
      Bus_BusMode <= '0'; -- 8 bit

      Bus_WB_Adr <= T80_WB_Adr;
      Bus_WB_Dat_To(7 downto 0) <= T80_WB_Dat_From;
      Bus_WB_Dat_To(31 downto 8) <= (others => 'X');
      Bus_WB_Cyc <= T80_WB_Cyc;
      Bus_WB_Sel <= "1111";
      Bus_WB_Stb <= T80_WB_Stb;
      Bus_WB_WE <= T80_WB_WE;

      Ctrl_WB_Dat_To <= (others => 'X');
      Ctrl_WB_Ack <= '0';
      Ctrl_WB_Err <= '0';

      M68K_WB_Dat_To <= (others => 'X');
      M68K_WB_Ack <= '0';
      M68K_WB_Err <= '0';

      T80_WB_Dat_To <= Bus_WB_Dat_From(7 downto 0);
      T80_WB_Ack <= Bus_WB_Ack;
      T80_WB_Err <= Bus_WB_Err;

      T65_WB_Dat_To <= (others => 'X');
      T65_WB_Ack <= '0';
      T65_WB_Err <= '0';
    elsif T65_Enable = '1' then
      Bus_BusMode <= '0'; -- 8 bit

      Bus_WB_Adr <= T65_WB_Adr;
      Bus_WB_Dat_To(7 downto 0) <= T65_WB_Dat_From;
      Bus_WB_Dat_To(31 downto 8) <= (others => 'X');
      Bus_WB_Cyc <= T65_WB_Cyc;
      Bus_WB_Sel <= "1111";
      Bus_WB_Stb <= T65_WB_Stb;
      Bus_WB_WE <= T65_WB_WE;

      Ctrl_WB_Dat_To <= (others => 'X');
      Ctrl_WB_Ack <= '0';
      Ctrl_WB_Err <= '0';

      M68K_WB_Dat_To <= (others => 'X');
      M68K_WB_Ack <= '0';
      M68K_WB_Err <= '0';

      T80_WB_Dat_To <= (others => 'X');
      T80_WB_Ack <= '0';
      T80_WB_Err <= '0';

      T65_WB_Dat_To <= Bus_WB_Dat_From(7 downto 0);
      T65_WB_Ack <= Bus_WB_Ack;
      T65_WB_Err <= Bus_WB_Err;
    else
      Bus_BusMode <= 'X';

      Bus_WB_Adr <= (others => 'X');
      Bus_WB_Dat_To <= (others => 'X');
      Bus_WB_Cyc <= '0';
      Bus_WB_Sel <= "0000";
      Bus_WB_Stb <= '0';
      Bus_WB_WE <= 'X';

      Ctrl_WB_Dat_To <= (others => 'X');
      Ctrl_WB_Ack <= '0';
      Ctrl_WB_Err <= '0';

      M68K_WB_Dat_To <= (others => 'X');
      M68K_WB_Ack <= '0';
      M68K_WB_Err <= '0';

      T80_WB_Dat_To <= (others => 'X');
      T80_WB_Ack <= '0';
      T80_WB_Err <= '0';

      T65_WB_Dat_To <= (others => 'X');
      T65_WB_Ack <= '0';
      T65_WB_Err <= '0';
    end if;
  end process;
end architecture rtl;
