-- The Retro_uC top control block. It contains a JTAG interface so the control
-- can be configured through this interface.
--
-- Next to the normal boundary scan it will also handle commands sepcific for
-- the Retro_uC like memory access.
--
-- This blocks works on the TCK clock. Cross domain signaling is tackled by the
-- WB interface protocol.

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

use work.c4m_jtag.ALL;

entity Retro_uC_Control is
  generic (
    IOS:        integer := 1024;
    WITH_M68K:  boolean := true;
    WITH_T80:   boolean := true;
    WITH_T65:   boolean := true;

    VERSION:    std_logic_vector(3 downto 0)
  );
  port (
    RESET_N:    in std_logic;
    Clock:      in std_logic; -- Clock for Wishbone bus

    -- The TAP signals
    TCK:        in std_logic;
    TMS:        in std_logic;
    TDI:        in std_logic;
    TDO:        out std_logic;
    TRST_N:     in std_logic;

    -- The I/O access ports
    CORE_IN:    out std_logic_vector(IOS-1 downto 0);
    CORE_EN:    in std_logic_vector(IOS-1 downto 0);
    CORE_OUT:   in std_logic_vector(IOS-1 downto 0);

    -- The pad connections
    PAD_IN:     in std_logic_vector(IOS-1 downto 0);
    PAD_EN:     out std_logic_vector(IOS-1 downto 0);
    PAD_OUT:    out std_logic_vector(IOS-1 downto 0);

    -- WB interface
    WB_Adr_o:   out std_logic_vector(15 downto 0);
    WB_Dat_o:   out std_logic_vector(31 downto 0);
    WB_Dat_i:   in std_logic_vector(31 downto 0);
    WB_WE_o:    out std_logic;
    WB_Stb_o:   out std_logic;
    WB_Cyc_o:   out std_logic;
    WB_Ack_i:   in std_logic;
    WB_Err_i:   in std_logic;

    -- Enabling cores, initialized during reset by the input signals
    -- Can be controlled later by JTAG commands
    M68K_Enable_In: in std_logic;
    M68K_Enable_Out: out std_logic;
    T80_Enable_In: in std_logic;
    T80_Enable_Out: out std_logic;
    T65_Enable_In: in std_logic;
    T65_Enable_Out: out std_logic
  );
end Retro_uC_Control;

architecture rtl of Retro_uC_Control is
  type INIT_STATE_TYPE is (
    Init_Reset,
    Init_MemCycle,
    Init_Run
  );
  signal Init_State:    INIT_STATE_TYPE;
  signal Init_Adr:      std_logic_vector(1 downto 0);
  signal ADDR_INIT:     std_logic_vector(15 downto 0);
  signal DATA_INIT:     std_logic_vector(31 downto 0);

  signal TAP_TDO:       std_logic;
  signal TAP_RESET:     std_logic;
  signal DRCAPTURE:     std_logic;
  signal DRSHIFT:       std_logic;
  signal DRUPDATE:      std_logic;
  constant IR_WIDTH:    integer := 3;
  signal IR:            std_logic_vector(IR_WIDTH-1 downto 0);
  constant CMD_MEMADDR: std_logic_vector(IR_WIDTH-1 downto 0) := "011";
  constant CMD_MEMRD:   std_logic_vector(IR_WIDTH-1 downto 0) := "101";
  constant CMD_MEMWR:   std_logic_vector(IR_WIDTH-1 downto 0) := "110";
  signal ADDR_SR:       unsigned(15 downto 0);
  signal ADDR_WR:       unsigned(15 downto 0);
  signal DATA_SR:       std_logic_vector(31 downto 0);
  signal DATA_RD:       std_logic_vector(31 downto 0);
  signal DATA_WR:       std_logic_vector(31 downto 0);
  signal ISADDRCMD:     boolean;
  signal ISREADCMD:     boolean;
  signal ISWRITECMD:    boolean;
  signal ISMEMCMD:      boolean;

  signal do_read:       boolean;
  signal done_read:     boolean;
  signal do_write:      boolean;
  signal done_write:    boolean;
  signal JTAG_Stb:      std_logic;

  constant CMD_ENABLE:  std_logic_vector(IR_WIDTH-1 downto 0) := "100";
  signal ISENABLECMD:   boolean;

  signal do_enable:     boolean;
  signal done_enable:   boolean;

  signal M68K_Enable:   std_logic;
  signal T80_Enable:    std_logic;
  signal T65_Enable:    std_logic;
  signal Enable_SR:     std_logic_vector(2 downto 0);
begin
  -- Handle init
  Init: process(RESET_N, Clock)
  begin
    if RESET_N = '0' then
      Init_State <= Init_Reset;
      Init_Adr <= "00";
    elsif rising_edge(Clock) then
      if Init_State = Init_Reset then
        Init_State <= Init_MemCycle;
      elsif Init_State = Init_MemCycle then
        if WB_Ack_i = '1' then
          if M68K_Enable = '1' then
            if Init_Adr = "00" then
              Init_Adr <= "01";
            elsif Init_Adr = "01" then
              Init_Adr <= "10";
            else
              Init_State <= Init_Run;
            end if;
          else
            Init_State <= Init_Run;
          end if;
        end if;
      elsif Init_State /= Init_Run then
        Init_State <= Init_Reset;
      end if;
    end if;
  end process;
  ADDR_INIT <= 14x"0000" & Init_Adr;
  DATA_INIT <= x"00000FFF" when M68K_Enable = '1' and Init_Adr = "00" else -- SP
               x"00000008" when M68K_Enable = '1' and Init_Adr = "01" else -- PC
               x"00004E72" when M68K_Enable = '1' and Init_Adr = "10" else -- STOP
               x"C3000000" when T80_Enable = '1' else -- JP $0000
               x"4C000000" when T65_Enable = '1' else -- JMP $0000
               x"XXXXXXXX";

  -- Handle enable signals
  Enable: process(Clock, RESET_N)
  begin
    if RESET_N = '0' then
      done_enable <= false;
    elsif rising_edge(Clock) then
      if do_enable then
        done_enable <= true;
      end if;
      if done_enable and not do_enable then
        done_enable <= false;
      end if;
    end if;
  end process;

  GEN_M68K_ENABLE: if WITH_M68K generate
    Enable_M68K: process(Clock)
    begin
      if rising_edge(Clock) then
        if RESET_N = '0' then
          M68K_Enable <= M68K_Enable_In;
        elsif do_enable then
          M68K_Enable <= Enable_SR(2);
        end if;
      end if;
    end process;
  end generate;
  GEN_M68K_NOTENABLE: if not WITH_M68K generate
    M68K_Enable <= '0';
  end generate;

  GEN_T80_ENABLE: if WITH_T80 generate
    Enable_T80: process(Clock)
    begin
      if rising_edge(Clock) then
        if RESET_N = '0' then
          T80_Enable <= T80_Enable_In;
        elsif do_enable then
          T80_Enable <= Enable_SR(1);
        end if;
      end if;
    end process;
  end generate;
  GEN_T80_NOTENABLE: if not WITH_T80 generate
    T80_Enable <= '0';
  end generate;

  GEN_T65_ENABLE: if WITH_T65 generate
    Enable_T65: process(Clock)
    begin
      if rising_edge(Clock) then
        if RESET_N = '0' then
          T65_Enable <= T65_Enable_In;
        elsif do_enable then
          T65_Enable <= Enable_SR(0);
        end if;
      end if;
    end process;
  end generate;
  GEN_T65_NOTENABLE: if not WITH_T65 generate
    T65_Enable <= '0';
  end generate;

  M68K_Enable_Out <= M68K_Enable when Init_state = Init_Run else '0';
  T80_Enable_Out <= T80_Enable when Init_State = Init_Run else '0';
  T65_Enable_Out <= T65_Enable when Init_State = Init_Run else '0';
  ISENABLECMD <= IR = CMD_ENABLE;
  process (TCK, RESET_N)
  begin
    if RESET_N = '0' then
      do_enable <= false;
    elsif rising_edge(TCK) then
      if ISENABLECMD then
        if TAP_RESET = '1' then
          do_enable <= false;
        elsif DRCAPTURE = '1' then
          Enable_SR(2) <= M68K_Enable;
          Enable_SR(1) <= T80_Enable;
          Enable_SR(0) <= T65_Enable;
        elsif DRSHIFT = '1' then
          Enable_SR(1 downto 0) <= Enable_SR(2 downto 1);
          Enable_SR(2) <= TDI;
        elsif DRUPDATE = '1' then
          do_enable <= true;
        end if;
      end if;
      if do_enable and done_enable then
        do_enable <= false;
      end if;
    end if;
  end process;

  -- The TAP controller
  JTAG: component c4m_jtag_tap_controller
    generic map (
      IR_WIDTH => IR_WIDTH,
      IOS => IOS,
      VERSION => "0101"
    )
    port map (
      TCK => TCK,
      TMS => TMS,
      TDI => TDI,
      TDO => TAP_TDO,
      TRST_N => TRST_N,
      RESET => TAP_RESET,
      DRCAPTURE => DRCAPTURE,
      DRSHIFT => DRSHIFT,
      DRUPDATE => DRUPDATE,
      IR => IR,
      CORE_IN => CORE_IN,
      CORE_EN => CORE_EN,
      CORE_OUT => CORE_OUT,
      PAD_IN => PAD_IN,
      PAD_EN => PAD_EN,
      PAD_OUT => PAD_OUT
    );

  -- Handle the memory access.
  -- Currently only handle read or write in 32 bits at a time. Always an update
  -- has to happen after every 32 bits more bits will be lost, less bits will
  -- result with old data in memory be partly shifted.
  -- For CMD_MEMWR also the data in the location will be read and shifted out.
  -- TODO: Allow streaming of longer length than 32 bits
  ISADDRCMD <= IR = CMD_MEMADDR;
  ISREADCMD <= IR = CMD_MEMRD or IR = CMD_MEMWR;
  ISWRITECMD <= IR = CMD_MEMWR;
  ISMEMCMD <= ISREADCMD or ISWRITECMD;

  -- Select the right TDO signals, local ones have priority over JTAG one
  TDO <= Enable_SR(0) when ISENABLECMD and DRSHIFT = '1' else
         ADDR_SR(0) when ISADDRCMD and DRSHIFT = '1' else
         DATA_SR(0) when ISMEMCMD and DRSHIFT = '1' else
         TAP_TDO;
  
  process (TCK, Clock, TRST_N)
  begin
    if TRST_N = '0' then
      ADDR_SR <= (others => '0');
      DATA_SR <= (others => '0');

      do_read <= false;
      done_read <= false;
      do_write <= false;
      done_write <= false;
      JTAG_Stb <= '0';
    else
      if rising_edge(TCK) then
        -- Shift address register
        if ISADDRCMD then
          if DRSHIFT = '1' then
            ADDR_SR(14 downto 0) <= ADDR_SR(15 downto 1);
            ADDR_SR(15) <= TDI;
          elsif DRUPDATE = '1' then
            do_read <= true; -- Start a read cycle
          end if;
        end if;

        -- Shift data register
        if ISMEMCMD and DRSHIFT = '1' then
          DATA_SR(30 downto 0) <= DATA_SR(31 downto 1);
          DATA_SR(31) <= TDI;
        end if;

        -- When read put data in DATA_SR during Capture
        if ISREADCMD and DRCAPTURE = '1' then
          DATA_SR <= DATA_RD;
        end if;

        -- Increase address when in Update and there was a write and/or a read
        if ISMEMCMD and DRUPDATE = '1' then
          if ISWRITECMD then
            do_write <= true;
            ADDR_WR <= ADDR_SR;
            DATA_WR <= DATA_SR;
          end if;
          ADDR_SR <= ADDR_SR + 1;
          do_read <= true;
        end if;

        if do_read and done_read then
          do_read <= false;
        end if;
        if do_write and done_write then
          do_write <= false;
        end if;
      end if;

      if rising_edge(Clock) then
        -- Do read or write cycle, do first read cycle
        -- TODO: See if we can get rid of extra TCK cycles for handling JTAG_Stb
        if do_read then
          if (JTAG_Stb = '0') and not done_read then
            JTAG_Stb <= '1';
          elsif WB_Ack_i = '1' or WB_Err_i = '1' then
            done_read <= true;
            if WB_Ack_i = '1' then
              DATA_RD <= WB_Dat_i;
            else
              DATA_RD <= (others => 'X');
            end if;
            JTAG_Stb <= '0';
          end if;
        elsif do_write then
          if (JTAG_Stb = '0') and not done_write then
            JTAG_Stb <= '1';
          elsif WB_Ack_i = '1' or WB_Err_i = '1' then
            done_write <= true;
            JTAG_Stb <= '0';
          end if;
        end if;

        if done_read and not do_read then
          done_read <= false;
        end if;
        if done_write and not do_write then
          done_write <= false;
        end if;
      end if;
    end if;
  end process;

  WBBUS: process (all)
  begin
    if Init_State = Init_MemCycle then
      WB_Adr_o <= ADDR_INIT;
      WB_Dat_o <= DATA_INIT;
      WB_WE_o <= '1';
      WB_Cyc_o <= '1';
      WB_Stb_o <= '1';
    else
      -- Do read has priority over do_write
      if do_read then
        WB_Adr_o <= std_logic_vector(ADDR_SR);
      else
        WB_Adr_o <= std_logic_vector(ADDR_WR);
      end if;
      if do_write then
        WB_Dat_o <= DATA_WR;
      else
        WB_Dat_o <= (others => 'X');
      end if;
      if (not do_read) and do_write then
        WB_WE_o <= '1';
      else
        WB_WE_o <= '0';
      end if;
      if do_read or do_write then
        WB_Cyc_o <= '1';
      else
        WB_Cyc_o <= '0';
      end if;
      WB_Stb_o <= JTAG_Stb;
    end if;
  end process;
end rtl;
