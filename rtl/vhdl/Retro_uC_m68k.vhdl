-- The Retro_uC_m68k sub-block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_m68k is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- Wishbone master interface
    WB_Adr_o:   out std_logic_vector(15 downto 0);
    WB_Sel_o:   out std_logic_vector(3 downto 0);
    WB_Dat_o:   out std_logic_vector(31 downto 0);
    WB_Dat_i:   in std_logic_vector(31 downto 0);
    WB_WE_o:    out std_logic;
    WB_Stb_o:   out std_logic;
    WB_Cyc_o:   out std_logic;
    WB_Ack_i:   in std_logic;
    WB_Err_i:   in std_logic
  );
end Retro_uC_m68k;

architecture rtl of Retro_uC_m68k is
  component ao68000 is
    port (
      CLK_I:    in std_logic;
      reset_n:  in std_logic;

      CYC_O:    out std_logic;
      ADR_O:    out std_logic_vector(31 downto 2);
      DAT_O:    out std_logic_vector(31 downto 0);
      DAT_I:    in std_logic_vector(31 downto 0);
      SEL_O:    out std_logic_vector(3 downto 0);
      STB_O:    out std_logic;
      WE_O:     out std_logic;

      ACK_I:    in std_logic;
      ERR_I:    in std_logic;
      RTY_I:    in std_logic;

      SGL_O:    out std_logic;
      BLK_O:    out std_logic;
      RMW_O:    out std_logic;

      CTI_O:    out std_logic_vector(2 downto 0);
      BTE_O:    out std_logic_vector(1 downto 0);

      fc_o:     out std_logic_vector(2 downto 0);

      ipl_i:    in std_logic_vector(2 downto 0);
      reset_o:  out std_logic;
      blocked_o: out std_logic
    );
  end component ao68000;

  signal Clock_n:       std_logic;

  signal adr:   std_logic_vector(31 downto 2);
begin
  Clock_n <= not(Clock);

  -- The M68K CPU
  Core: component ao68000
    port map (
      CLK_I => Clock_n,
      reset_n => RESET_N,
      CYC_O => WB_Cyc_o,
      ADR_O => adr,
      DAT_O => WB_Dat_o,
      DAT_I => WB_Dat_i,
      SEL_O => WB_Sel_o,
      STB_O => WB_Stb_o,
      WE_O => WB_WE_o,
      ACK_I => WB_Ack_i,
      ERR_I => WB_Err_i,
      RTY_I => '0',
      SGL_O => open,
      BLK_O => open,
      RMW_O => open, -- TODO: implement RMW
      CTI_O => open,
      BTE_O => open,
      fc_o => open,
      ipl_i => "000",
      reset_o => open,
      blocked_o => open
    );
  WB_Adr_o <= adr(17 downto 2);
end architecture rtl;
