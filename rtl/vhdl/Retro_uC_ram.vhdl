-- The Retro_uC generic RAM block

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Retro_uC_RAM is
  port (
    Clock:      in std_logic;
    CE:         in std_logic;
    WE:         in std_logic;
    -- Width of address will determine number of words in the RAM
    Address:    in std_logic_vector;
    -- Data_In and Data_Out have to have the same width
    Data_In:    in std_logic_vector;
    Data_Out:   out std_logic_vector
  );
end entity Retro_uC_RAM;

architecture rtl of Retro_uC_RAM is
  type ram_type is array (0 to (2**Address'length)-1) of std_logic_vector(Data_In'range);
  signal RAM:           ram_type;
  signal Address_hold:  std_logic_vector(Address'range);
  signal WE_hold:       std_logic;
begin
  process(Clock) is
  begin
    if (rising_edge(Clock) and CE = '1') then
      if WE = '0' then
        -- Read cycle
        Data_Out <= RAM(to_integer(unsigned(Address)));
      else
        -- Write cycle
        RAM(to_integer(unsigned(Address))) <= Data_In;
        Data_Out <= Data_In;
      end if;
    end if;
  end process;
end architecture rtl;

