-- The Retro_uC generic RAM block

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Retro_uC_RAM is
  port (
    Clock:      in std_logic;
    CE:         in std_logic;
    WE:         in std_logic;
    -- Width of address will determine number of words in the RAM
    Address:    in std_logic_vector;
    -- Data_In and Data_Out have to have the same width
    Data_In:    in std_logic_vector;
    Data_Out:   out std_logic_vector
  );
end entity Retro_uC_RAM;
