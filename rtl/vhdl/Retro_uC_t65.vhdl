-- The Retro_uC_t65 sub-block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_t65 is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- Wishbone master interface
    WB_Adr_o:   out std_logic_vector(15 downto 0);
    WB_Dat_o:   out std_logic_vector(7 downto 0);
    WB_Dat_i:   in std_logic_vector(7 downto 0);
    WB_WE_o:    out std_logic;
    WB_Stb_o:   out std_logic;
    WB_Cyc_o:   out std_logic;
    WB_Ack_i:   in std_logic;
    WB_Err_i:   in std_logic
  );
end Retro_uC_t65;

architecture rtl of Retro_uC_t65 is
  signal Clock_n:       std_logic;
  signal R_W_n:         std_logic;
  signal A:             std_logic_vector(23 downto 0);
begin
  Clock_n <= '1' when Clock = '0' else '0';

  -- The Z80 CPU
  CPU: entity work.T65
    port map (
      Mode => "00", -- Use 6502 mode for now; is the most tested
      Res_n => RESET_N,
      Enable => '1',
      Clk => Clock_n,
      Rdy => WB_Ack_i,
      Abort_n => '1',
      IRQ_n => '1',
      NMI_n => '1',
      SO_n => '1',
      R_W_n => R_W_n,
      Sync => open,
      EF => open,
      MF => open,
      XF => open,
      ML_n => open,
      VP_n => open,
      VDA => open,
      VPA => open,
      A => A,
      DI => WB_Dat_i,
      DO => WB_Dat_o,
      DEBUG => open
    );
  WB_Adr_o <= A(15 downto 0);
  WB_WE_o <= not(R_W_n);

  -- Handle WB_Stb_o and WB_Cyc_o
  WB_Stb_o <= '1';
  WB_Cyc_o <= '1';
end rtl;
