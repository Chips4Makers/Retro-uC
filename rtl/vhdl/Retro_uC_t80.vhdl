-- The Retro_uC_t80 sub-block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_t80 is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- Wishbone master interface
    WB_Adr_o:   out std_logic_vector(15 downto 0);
    WB_Dat_o:   out std_logic_vector(7 downto 0);
    WB_Dat_i:   in std_logic_vector(7 downto 0);
    WB_WE_o:    out std_logic;
    WB_Stb_o:   out std_logic;
    WB_Cyc_o:   out std_logic;
    WB_Ack_i:   in std_logic;
    WB_Err_i:   in std_logic
  );
end Retro_uC_t80;

architecture rtl of Retro_uC_t80 is
  signal Clock_n:       std_logic;

  signal WAIT_n:        std_logic;
  signal MREQ_n:        std_logic;
  signal IORQ_n:        std_logic;
  signal WR_n:          std_logic;
  signal RFSH_n:        std_logic;
  signal HALT_n:        std_logic;
  
  signal Is_WB_Cycle:   boolean;
  signal Cyc:           std_logic;
begin
  Clock_n <= '1' when Clock = '0' else '0';

  -- The Z80 CPU
  CPU: entity work.T80s
    generic map (
      Mode => 1 -- Fast Z80
    )
    port map (
      RESET_n => RESET_N,
      CLK_n => Clock_n,
      WAIT_n => WAIT_n,
      INT_n => '1',
      NMI_n => '1',
      BUSRQ_n => '1',
      M1_n => open,
      MREQ_n => MREQ_n,
      IORQ_n => IORQ_n,
      RD_n => open,
      WR_n => WR_n,
      RFSH_n => RFSH_n,
      HALT_n => HALT_n,
      BUSAK_n => open,
      A => WB_Adr_o,
      DI => WB_Dat_i,
      DO => WB_Dat_o
    );
  WB_WE_o <= not(WR_n);

  -- We don't make a distinction between memory or IO
  -- Both get the same address space
  Is_WB_Cycle <= (MREQ_n = '0' or IORQ_n = '0') and RFSH_n = '1' and HALT_n = '1';

  -- Handle WB_Stb_o and WB_Cyc_o
  WB_Stb_o <= Cyc;
  WB_Cyc_o <= Cyc;
  Cyc <= '1' when Is_WB_Cycle else
         '0';

  -- Let the CPU wait when needed
  WAIT_n <= '1' when not Is_WB_Cycle else
            WB_Ack_i;
end rtl;
