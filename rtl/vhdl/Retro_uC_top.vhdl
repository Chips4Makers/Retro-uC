-- Top Retro_uC block

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Retro_uC_top is
  generic (
    IOS:        integer := 1024;
    WITH_M68K:  boolean := true;
    WITH_T80:   boolean := true;
    WITH_T65:   boolean := true
  );
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- Configuration
    M68K_Enable: in std_logic;
    T80_Enable: in std_logic;
    T65_Enable: in std_logic;

    -- JTAG interface
    TRST_N:     in std_logic;
    TCK:        in std_logic;
    TMS:        in std_logic;
    TDI:        in std_logic;
    TDO:        out std_logic;

    -- GPIO
    IO_IN:     in std_logic_vector(IOS-1 downto 0);
    IO_OUT:    out std_logic_vector(IOS-1 downto 0);
    IO_EN:     out std_logic_vector(IOS-1 downto 0)
  );
end Retro_uC_top;

architecture rtl of Retro_uC_top is
  signal CORE_IN:       std_logic_vector(IOS-1 downto 0);
  signal CORE_OUT:      std_logic_vector(IOS-1 downto 0);
  signal CORE_EN:       std_logic_vector(IOS-1 downto 0);

  -- Control signals
  signal Ctrl_WB_Adr:   std_logic_vector(15 downto 0);
  signal Ctrl_WB_Dat_To: std_logic_vector(31 downto 0);
  signal Ctrl_WB_Dat_From: std_logic_vector(31 downto 0);
  signal Ctrl_WB_WE:    std_logic;
  signal Ctrl_WB_Stb:   std_logic;
  signal Ctrl_WB_Cyc:   std_logic;
  signal Ctrl_WB_Ack:   std_logic;
  signal Ctrl_WB_Err:   std_logic;
  signal Ctrl_T80_Enable: std_logic;
  signal Ctrl_T65_Enable: std_logic;
  signal Ctrl_M68K_Enable: std_logic;

  -- M68K signals
  signal M68K_Clock:    std_logic;
  signal M68K_WB_Adr:   std_logic_vector(15 downto 0);
  signal M68K_WB_Sel:   std_logic_vector(3 downto 0);
  signal M68K_WB_Dat_To: std_logic_vector(31 downto 0);
  signal M68K_WB_Dat_From: std_logic_vector(31 downto 0);
  signal M68K_WB_WE:    std_logic;
  signal M68K_WB_Stb:   std_logic;
  signal M68K_WB_Cyc:   std_logic;
  signal M68K_WB_Ack:   std_logic;
  signal M68K_WB_Err:   std_logic;

  -- T80 signals
  signal T80_Clock:     std_logic;
  signal T80_WB_Adr:    std_logic_vector(15 downto 0);
  signal T80_WB_Dat_To: std_logic_vector(7 downto 0);
  signal T80_WB_Dat_From: std_logic_vector(7 downto 0);
  signal T80_WB_WE:     std_logic;
  signal T80_WB_Stb:    std_logic;
  signal T80_WB_Cyc:    std_logic;
  signal T80_WB_Ack:    std_logic;
  signal T80_WB_Err:    std_logic;

  -- T65 signals
  signal T65_Clock:     std_logic;
  signal T65_WB_Adr:    std_logic_vector(15 downto 0);
  signal T65_WB_Dat_To: std_logic_vector(7 downto 0);
  signal T65_WB_Dat_From: std_logic_vector(7 downto 0);
  signal T65_WB_WE:     std_logic;
  signal T65_WB_Stb:    std_logic;
  signal T65_WB_Cyc:    std_logic;
  signal T65_WB_Ack:    std_logic;
  signal T65_WB_Err:    std_logic;

  -- WBBus signals
  signal Bus_BusMode:   std_logic;
  signal Bus_WB_Adr:    std_logic_vector(15 downto 0);
  signal Bus_WB_Dat_To: std_logic_vector(31 downto 0);
  signal Bus_WB_Dat_From: std_logic_vector(31 downto 0);
  signal Bus_WB_Cyc:    std_logic;
  signal Bus_WB_Sel:    std_logic_vector(3 downto 0);
  signal Bus_WB_Stb:    std_logic;
  signal Bus_WB_WE:     std_logic;
  signal Bus_WB_Ack:    std_logic;
  signal Bus_WB_Err:    std_logic;
  signal Bus_IOs_Out:   std_logic_vector(IOS-1 downto 0);
  signal Bus_IOs_En:    std_logic_vector(IOS-1 downto 0);
  signal Bus_IOs_In:    std_logic_vector(IOS-1 downto 0);
begin
  -- The Control block
  JTAG: entity work.Retro_uC_Control
    generic map (
      IOS => IOS,
      VERSION => "0101"
    )
    port map (
      RESET_N => RESET_N,
      Clock => Clock,
      TCK => TCK,
      TMS => TMS,
      TDI => TDI,
      TDO => TDO,
      TRST_N => TRST_N,
      CORE_IN => CORE_IN,
      CORE_EN => CORE_EN,
      CORE_OUT => CORE_OUT,
      PAD_IN => IO_IN,
      PAD_EN => IO_EN,
      PAD_OUT => IO_OUT,
      WB_Adr_o => Ctrl_WB_Adr,
      WB_Dat_o => Ctrl_WB_Dat_From,
      WB_Dat_i => Ctrl_WB_Dat_To,
      WB_WE_o => Ctrl_WB_WE,
      WB_Stb_o => Ctrl_WB_Stb,
      WB_Cyc_o => Ctrl_WB_Cyc,
      WB_Ack_i => Ctrl_WB_Ack,
      WB_Err_i => Ctrl_WB_Err,
      T80_Enable_In => T80_Enable,
      T80_Enable_Out => Ctrl_T80_Enable,
      T65_Enable_in => T65_Enable,
      T65_Enable_Out => Ctrl_T65_Enable,
      M68K_Enable_In => M68K_Enable,
      M68K_Enable_Out => Ctrl_M68K_Enable
    );

  -- The Arbiter
  Arbiter: entity work.Retro_uC_WBArbiter
    port map (
      RESET_N => RESET_N,
      Clock => Clock,
      
      Ctrl_WB_Adr => Ctrl_WB_Adr,
      Ctrl_WB_Dat_To => Ctrl_WB_Dat_To,
      Ctrl_WB_Dat_From => Ctrl_WB_Dat_From,
      Ctrl_WB_WE => Ctrl_WB_WE,
      Ctrl_WB_Stb => Ctrl_WB_Stb,
      Ctrl_WB_Cyc => Ctrl_WB_Cyc,
      Ctrl_WB_Ack => Ctrl_WB_Ack,
      Ctrl_WB_Err => Ctrl_WB_Err,

      M68K_Enable => Ctrl_M68K_Enable,
      M68K_Clock => M68K_Clock,
      M68K_WB_Adr => M68K_WB_Adr,
      M68K_WB_Sel => M68K_WB_Sel,
      M68K_WB_Dat_To => M68K_WB_Dat_To,
      M68K_WB_Dat_From => M68K_WB_Dat_From,
      M68K_WB_WE => M68K_WB_WE,
      M68K_WB_Cyc => M68K_WB_Cyc,
      M68K_WB_Stb => M68K_WB_Stb,
      M68K_WB_Ack => M68K_WB_Ack,
      M68K_WB_Err => M68K_WB_Err,

      T80_Enable => Ctrl_T80_Enable,
      T80_Clock => T80_Clock,
      T80_WB_Adr => T80_WB_Adr,
      T80_WB_Dat_To => T80_WB_Dat_To,
      T80_WB_Dat_From => T80_WB_Dat_From,
      T80_WB_WE => T80_WB_WE,
      T80_WB_Stb => T80_WB_Stb,
      T80_WB_Cyc => T80_WB_Cyc,
      T80_WB_Ack => T80_WB_Ack,
      T80_WB_Err => T80_WB_Err,

      T65_Enable => Ctrl_T65_Enable,
      T65_Clock => T65_Clock,
      T65_WB_Adr => T65_WB_Adr,
      T65_WB_Dat_To => T65_WB_Dat_To,
      T65_WB_Dat_From => T65_WB_Dat_From,
      T65_WB_WE => T65_WB_WE,
      T65_WB_Stb => T65_WB_Stb,
      T65_WB_Cyc => T65_WB_Cyc,
      T65_WB_Ack => T65_WB_Ack,
      T65_WB_Err => T65_WB_Err,

      Bus_BusMode => Bus_BusMode,
      Bus_WB_Adr => Bus_WB_Adr,
      Bus_WB_Dat_To => Bus_WB_Dat_To,
      Bus_WB_Dat_From => Bus_WB_Dat_From,
      Bus_WB_WE => Bus_WB_WE,
      Bus_WB_Cyc => Bus_WB_Cyc,
      Bus_WB_Sel => Bus_WB_Sel,
      Bus_WB_Stb => Bus_WB_Stb,
      Bus_WB_Ack => Bus_WB_Ack,
      Bus_WB_Err => Bus_WB_Err
    );

  -- Instantiate the microcontrollers
  GEN_M68K: if WITH_M68K generate
    M68KCore: entity work.Retro_uC_m68k
      port map (
        Clock => M68K_Clock,
        RESET_N => Ctrl_M68K_Enable,
        WB_Adr_o => M68K_WB_Adr,
        WB_Sel_o => M68K_WB_Sel,
        WB_Dat_o => M68K_WB_Dat_From,
        WB_Dat_i => M68K_WB_Dat_To,
        WB_WE_o => M68K_WB_WE,
        WB_Stb_o => M68K_WB_Stb,
        WB_Cyc_o => M68K_WB_Cyc,
        WB_Ack_i => M68K_WB_Ack,
        WB_Err_i => M68K_WB_Err
      );
  end generate;
  GEN_T80: if WITH_T80 generate
    T80Core: entity work.Retro_uC_t80
      port map (
        Clock => T80_Clock,
        RESET_N => Ctrl_T80_Enable,
        WB_Adr_o => T80_WB_Adr,
        WB_Dat_o => T80_WB_Dat_From,
        WB_Dat_i => T80_WB_Dat_To,
        WB_WE_o => T80_WB_WE,
        WB_Stb_o => T80_WB_Stb,
        WB_Cyc_o => T80_WB_Cyc,
        WB_Ack_i => T80_WB_Ack,
        WB_Err_i => T80_WB_Err
      );
  end generate;
  GEN_T65: if WITH_T65 generate
    T65Core: entity work.Retro_uC_t65
      port map (
        Clock => T65_Clock,
        RESET_N => Ctrl_T65_Enable,
        WB_Adr_o => T65_WB_Adr,
        WB_Dat_o => T65_WB_Dat_From,
        WB_Dat_i => T65_WB_Dat_To,
        WB_WE_o => T65_WB_WE,
        WB_Stb_o => T65_WB_Stb,
        WB_Cyc_o => T65_WB_Cyc,
        WB_Ack_i => T65_WB_Ack,
        WB_Err_i => T65_WB_Err
      );
  end generate;

  -- The Bus
  WBBus: entity work.Retro_uC_WBBus
    generic map (
      IOS => IOS
    )
    port map (
      Clock => Clock,
      RESET_N => RESET_N,
      BusMode => Bus_BusMode,
      WB_Adr_i => Bus_WB_Adr,
      WB_Dat_i => Bus_WB_Dat_To,
      WB_Dat_o => Bus_WB_Dat_From,
      WB_Cyc_i => Bus_WB_Cyc,
      WB_Sel_i => Bus_WB_Sel,
      WB_Stb_i => Bus_WB_Stb,
      WB_WE_i => Bus_WB_WE,
      WB_Ack_o => Bus_WB_Ack,
      WB_Stall_o => open,
      WB_Err_o => Bus_WB_Err,
      IOs_Out => CORE_OUT,
      IOs_En => CORE_EN,
      IOs_In => CORE_IN
    );
end rtl;
