-- The Retro_uC WB Bus and standard memory map
-- This is generic implementation with maximum
-- amount of RAM, IOs etc.
-- Specific implementations for may only have partially implemented
-- the memory map.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Retro_uC_WBBus is
  generic (
    IOS:        integer := 1024; -- 1024 is maximum number of IOS
    T65_Reset_Vector: std_logic_vector(15 downto 0) := "0000000000000000"
  );
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- WB Bus can operate in 8 bit mode or 32 bit mode
    -- '0': 8-bit
    -- '1': 32-bit
    -- TODO: explain implication of 
    BusMode:    in std_logic;

    ---------------------------------------------------------------------------
    -- Wishbone bus
    ---------------------------------------------------------------------------
    -- The total address width is 16 bits; in 32 bit mode the upper two bits of
    -- the address are ignored.
    WB_Adr_i:   in std_logic_vector(15 downto 0);
    -- Data width is 32 bits; in 8 bit upper 24 bits are ignored.
    WB_Dat_i:   in std_logic_vector(31 downto 0);
    WB_Dat_o:   out std_logic_vector(31 downto 0);
    WB_Cyc_i:   in std_logic;
    -- 4 byte selection bits; in 8 bit mode these are ignored; in 32 bit mode
    -- the implementation should obey these selection bits.
    WB_Sel_i:   in std_logic_vector(3 downto 0);
    WB_Stb_i:   in std_logic;
    WB_WE_i:    in std_logic;
    -- Implementation should try to ack immediately and no stall if possible.
    WB_Ack_o:   out std_logic;
    WB_Stall_o: out std_logic;
    WB_Err_o:   out std_logic;
    
    ---------------------------------------------------------------------------
    -- Memory Map
    ---------------------------------------------------------------------------
    -- Everything is supposed to be memory mapped
    -- There is 64KB adress space; in 8 bit through 64K words, in 32 bit with
    -- 16K words.
    -- Map:
    -- + 32KB: memory
    -- + 4KB: IO handling
    -- + 4KB: Standard peripherals
    -- + 16KB: User/custom peripherals

    ---------------------------------------------------------------------------
    -- IO handling
    ---------------------------------------------------------------------------
    -- In 8 bit mode the IO handling start @ address 2^15; in 32 bit mode @
    -- address 2^13. Each of the next registers are put 128 bytes further in the
    -- memory map independent of the number of IOs.
    -- This first address corresponds with bits 7 downto 0 in 8 bit and 31
    -- downto 0 in 32 bit mode. In 32 bit WB_Sel_i(0) correspond with bit
    -- 7 downt 0.
    IOs_Out:    out std_logic_vector(IOS-1 downto 0);
    -- If possible should be put in high impedance state if not enabled.
    -- If not possible it should be documented by the implementation.
    IOs_En:     out std_logic_vector(IOS-1 downto 0);
    IOs_In:     in std_logic_vector(IOS-1 downto 0)
  );
end entity Retro_uC_WBBus;

architecture rtl of Retro_uC_WBBus is
  type CYCLE_TYPE is (
    No_Cycle,
    Memory_Cycle,
    IOOut_Cycle,
    IOEn_Cycle,
    IOIn_Cycle,
    T65_Vector,
    Error_Cycle
  );

  ---------------------------------------------------------------------------
  -- Memory
  ---------------------------------------------------------------------------
  -- 32KB is reserved for RAM, this can be SRAM or NVM. The memory can be
  -- internal or external with or without cache.
  -- The bus connects to 4 RAM blocks with 8 bit word size.
  -- In 8 bit mode CE bits will enable one of the four; in 32 bit all four
  -- at once.

  signal is_cycle:      boolean;
  signal select_bits:   std_logic_vector(8 downto 0);
  signal cycle:         CYCLE_TYPE; -- immediate derived from address
  signal cycle_hold:    CYCLE_TYPE; -- hold for a clock cycle
  signal was_read_cycle: boolean;
  signal busmode_hold:  std_logic;
  -- To allow block read we already intiate a read for next address after
  -- a current read cycle. This always done and not based on a WB block
  -- signal.
  signal wb_adr_next: std_logic_vector(15 downto 0);
  signal wb_adr_block: std_logic_vector(15 downto 0);
  signal wb_adr_hold:  std_logic_vector(15 downto 0);

  signal mem_adr:      std_logic_vector(12 downto 0);
  signal mem_we:        std_logic;
  signal mem_sel_bits:  std_logic_vector(1 downto 0);
  signal mem_sel_bits_hold: std_logic_vector(1 downto 0);
  signal mem_ce:        std_logic_vector(3 downto 0);
  signal mem_data_o:    std_logic_vector(31 downto 0);
  signal mem_read_data: std_logic_vector(31 downto 0);
  signal mem_write_data: std_logic_vector(31 downto 0);
  signal is_io_cycle:   boolean;
  signal is_io_cycle_hold: boolean;
  signal io_data_o:     std_logic_vector(31 downto 0);
  signal t65_data_o:    std_logic_vector(7 downto 0);
begin
  -----------------------
  -- Determine cycle type
  -----------------------
  is_cycle <= (WB_Stb_i = '1') and (WB_Cyc_i = '1');
  -- Block read done by prefetching next address if previous cycle was a read cycle.
  -- We don't use WB block signal but always do it.
  wb_adr_next <= std_logic_vector(to_unsigned(to_integer(unsigned(WB_Adr_i)) + 1, 16));
  wb_adr_block <= wb_adr_next when (WB_WE_i = '0') and (WB_Ack_o = '1') and was_read_cycle else
                  WB_Adr_i;
  select_bits <= wb_adr_block(15 downto 7) when BusMode = '0'
                 else wb_adr_block(13 downto 5);
  cycle <= No_Cycle when not is_cycle
           else Memory_Cycle when select_bits(8) = '0'
           else IOOut_Cycle when select_bits(7 downto 0) = "00000000"
           else IOEn_Cycle when select_bits(7 downto 0) = "00000001"
           else IOIn_Cycle when select_bits(7 downto 0) = "00000010"
           else T65_Vector when select_bits(6 downto 0) = "1111111" and BusMode = '0'
           else Error_Cycle;
  is_io_cycle <= cycle = IOOut_Cycle or cycle = IOEn_Cycle or cycle = IOIn_Cycle;
  is_io_cycle_hold <= cycle_hold = IOOut_Cycle or cycle_hold = IOEn_Cycle or cycle_hold = IOIn_Cycle;
  mem_sel_bits <= WB_Adr_i(1 downto 0);
  hold: process (Clock)
  begin
    if Rising_Edge(Clock) then
      cycle_hold <= cycle;
      was_read_cycle <= is_cycle and (WB_WE_i = '0');
      busmode_hold <= BusMode;
      mem_sel_bits_hold <= mem_sel_bits;
      wb_adr_hold <= wb_adr_block;
    end if;
  end process;
  
  -- We currently never stall
  WB_Stall_o <= '0';

  -- Handle WB_Ack_o and WB_Err_o
  -- Currently all read cycles are acknowledged after one cycle and write
  -- cycles immediately.
  ack: process (all)
  begin
    if RESET_N = '0' then
      WB_Ack_o <= '0';
      WB_Err_o <= '0';
    else -- RESET_N = '1'
      if cycle = Error_Cycle then
        WB_Ack_o <= '0';
        WB_Err_o <= '1';
      else
        WB_Err_o <= '0';
        if is_cycle and WB_WE_i = '1' then
          -- We immediately acknowledge a write cycle
          WB_Ack_o <= '1';
        elsif is_cycle and was_read_cycle then
          if was_read_cycle and (wb_adr_hold = WB_Adr_i) then
            -- Acknowledge if previous cycle was a read cycle and we have read
            -- the right address
            WB_Ack_o <= '1';
          else
            WB_Ack_o <= '0';
          end if;
        else
          WB_Ack_o <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Connect the right data output of WB_Data_o
  -- For Vector_Cycle we directly handle it here
  WB_Dat_o <= mem_data_o when cycle_hold = Memory_Cycle else
              io_data_o when is_io_cycle_hold else
              "XXXXXXXXXXXXXXXXXXXXXXXX" & t65_data_o when cycle_hold = T65_Vector else
              "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    
  
  ---------
  -- Memory
  ---------

  -- Embedded SRAM
  RAM0: entity work.Retro_uC_RAM
    port map (
      Clock => Clock,
      CE => mem_ce(0),
      WE => mem_we,
      Address => mem_adr,
      Data_In => mem_write_data(31 downto 24),
      Data_Out => mem_read_data(31 downto 24)
    );
  RAM1: entity work.Retro_uC_RAM
    port map (
      Clock => Clock,
      CE => mem_ce(1),
      WE => mem_we,
      Address => mem_adr,
      Data_In => mem_write_data(23 downto 16),
      Data_Out => mem_read_data(23 downto 16)
    );
  RAM2: entity work.Retro_uC_RAM
    port map (
      Clock => Clock,
      CE => mem_ce(2),
      WE => mem_we,
      Address => mem_adr,
      Data_In => mem_write_data(15 downto 8),
      Data_Out => mem_read_data(15 downto 8)
    );
  RAM3: entity work.Retro_uC_RAM
    port map (
      Clock => Clock,
      CE => mem_ce(3),
      WE => mem_we,
      Address => mem_adr,
      Data_In => mem_write_data(7 downto 0),
      Data_Out => mem_read_data(7 downto 0)
    );

  mem_adr <= wb_adr_block(14 downto 2) when BusMode = '0'
              else wb_adr_block(12 downto 0);

  -- select the right memory blocks
  -- provide the write enable and data to the blocks
  mem_select_write: process (cycle, WB_WE_i, BusMode, WB_Dat_i, mem_sel_bits, RESET_N)
  begin
    if RESET_N = '0' then
      mem_ce <= "0000";
      mem_we <= '0';
      mem_write_data <= (others => '0');
    elsif cycle = Memory_Cycle then
      mem_we <= WB_WE_i;
      if BusMode = '1' then
        -- For 32 bit mode all RAMs are selected
        mem_ce <= "1111";
        mem_write_data <= WB_Dat_i;
      else
        case mem_sel_bits is
          when "00" => mem_ce <= "0001";
          when "01" => mem_ce <= "0010";
          when "10" => mem_ce <= "0100";
          when "11" => mem_ce <= "1000";
          when others => mem_ce <= "XXXX";
        end case;
        mem_write_data <= WB_Dat_i(7 downto 0) & WB_Dat_i(7 downto 0) & WB_Dat_i(7 downto 0) & WB_Dat_i(7 downto 0);
      end if;
    else
      mem_ce <= "0000";
      mem_we <= '0';
      mem_write_data <= (others => '0');
    end if;
  end process;

  -- read the data
  mem_read: process (busmode_hold, mem_sel_bits_hold, mem_read_data)
  begin
    -- Hold BusMode and last 2 bits of address for a clock cycle
    if busmode_hold = '1' then
      mem_data_o <= mem_read_data;
    else
      case mem_sel_bits_hold is
        when "00" => mem_data_o(7 downto 0) <= mem_read_data(31 downto 24);
        when "01" => mem_data_o(7 downto 0) <= mem_read_data(23 downto 16);
        when "10" => mem_data_o(7 downto 0) <= mem_read_data(15 downto 8);
        when "11" => mem_data_o(7 downto 0) <= mem_read_data(7 downto 0);
        when others => mem_data_o(7 downto 0) <= "XXXXXXXX";
      end case;
      mem_data_o(31 downto 8) <= (others => 'X');
    end if;
  end process;


  --------------
  -- IO Handling
  --------------
  -- read and write the data
  io: process (Clock, RESET_N)
    procedure check_io_bit(
      signal BusMode:           in std_logic;
      signal wb_adr:            in std_logic_vector(15 downto 0);
      signal wb_sel:            in std_logic_vector(3 downto 0);
      constant io:              in integer;
      variable enabled:         out boolean;
      variable bit_index:       out integer
    ) is
      variable u_io:            unsigned(9 downto 0) := to_unsigned(io, 10);
    begin
      -- Enabled if the address matches the top bits of the io number
      if BusMode = '0' then -- 8 bit mode
        enabled := wb_adr(6 downto 0) = std_logic_vector(u_io(9 downto 3));
      else -- 32 bit mode
        enabled := wb_adr(4 downto 0) = std_logic_vector(u_io(9 downto 5));
        if enabled then
          case u_io(4 downto 3) is
            when "00" => enabled := WB_Sel_i(3) = '1';
            when "01" => enabled := WB_Sel_i(2) = '1';
            when "10" => enabled := WB_Sel_i(1) = '1';
            when "11" => enabled := WB_Sel_i(0) = '1';
            when others => enabled := false;
          end case;
        end if;
      end if;

      bit_index := to_integer(u_io(2 downto 0));
      if BusMode = '1' then -- 32 bit mode; add 8 bit offset
        bit_index := bit_index + (3 - to_integer(u_io(4 downto 3))) * 8;
      end if;
    end procedure check_io_bit;

    variable enabled:   boolean;
    variable bit_index: integer;
    variable data_o:    std_logic_vector(31 downto 0);
  begin
    if RESET_N = '0' then
      io_data_o <= (others => 'X');
      IOs_Out <= (others => 'X');
      IOs_En <= (others => '0'); -- IOs disabled after reset
    elsif Rising_Edge(Clock) then
      -- Start with all outputs bits set to 'X'
      data_o := (others => 'X');

      if is_io_cycle then
        for io in 0 to IOS-1 loop
          check_io_bit(
            BusMode => BusMode,
            wb_adr => wb_adr_block,
            wb_sel => WB_Sel_i,
            io => io,
            enabled => enabled,
            bit_index => bit_index
          );

          if enabled then
            case cycle is
              when IOOut_Cycle =>
                -- Only support write
                if WB_WE_i = '1' then
                  IOs_Out(io) <= WB_Dat_i(bit_index);
                end if;

              when IOEn_Cycle =>
                -- Only support write
                if WB_WE_i = '1' then
                  IOs_En(io) <= WB_Dat_i(bit_index);
                end if;

              when IOIn_Cycle =>
                -- Ignore WB_WE_i
                data_o(bit_index) := IOs_In(bit_index);

              when others =>
                -- Keep data_o on 'X'
                null;
            end case;
          end if;
        end loop;
      end if;

      io_data_o <= data_o;
    end if;
  end process;

  --------------
  -- T65 vectors
  --------------
  T65Vec: process (Clock)
  begin
    if Rising_Edge(Clock) then
      if cycle = T65_Vector then
        case wb_adr_block(7 downto 0) is
          when x"FC" => t65_data_o <= T65_Reset_Vector(7 downto 0);
          when x"FD" => t65_data_o <= T65_Reset_Vector(15 downto 8);
          when others => t65_data_o <= "XXXXXXXX";
        end case;
      end if;
    end if;
  end process;
end architecture;
