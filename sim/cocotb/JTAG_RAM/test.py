import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

from c4m_jtag import JTAG_Master

@cocotb.test()
def test01_idcode(dut):
    """
    Test the IDCODE command
    """

    # Run @ 1MHz
    clk_period = get_sim_steps(1, "us")
    master = JTAG_Master(dut.tck, dut.tms, dut.tdi, dut.tdo, dut.trst_n, clk_period)
    master.IDCODE = [0, 0, 1]

    # Init
    dut.reset_n = 0
    dut.t65_enable = 0
    dut.t80_enable = 0
    dut.clock = 0

    dut._log.info("Trying to get IDCODE...")

    yield master.idcode()
    result1 = master.result
    dut._log.info("IDCODE1: {}".format(result1))

    yield master.idcode()
    result2 = master.result
    dut._log.info("IDCODE2: {}".format(result2))

    assert(result1 == result2)

@cocotb.test()
def test02_mem(dut):
    """
    Test writing/reading to memory
    """

    # Run core @ 10 MHz
    clk_period = get_sim_steps(300, "ns")
    cocotb.fork(Clock(dut.clock, clk_period).start())
    # Run JTAG @ 1MHz
    tclk_period = get_sim_steps(1, "us")
    
    master = JTAG_Master(dut.tck, dut.tms, dut.tdi, dut.tdo, dut.trst_n, tclk_period)

    MEMADDR = BinaryValue("011", 3)
    MEMRD = BinaryValue("101", 3)
    MEMWR = BinaryValue("110", 3)

    data0 = BinaryValue(0, 32)
    dataFF = BinaryValue("11111111111111111111111111111111", 32)
    data10 = BinaryValue("10101010101010101010101010101010", 32)
    data01 = BinaryValue("01010101010101010101010101010101", 32)

    addr = BinaryValue(256, 16, bigEndian=False)

    # Init, enable t65
    dut.t65_enable = 1
    dut.t80_enable = 0
    yield Timer(2*clk_period)
    dut.reset_n = 1

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # WRITE => 0 & FFFF
    dut._log.info("Writing data")
    yield master.load_ir(MEMWR)
    yield master.shift_data(data0)
    yield master.shift_data(dataFF)

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 0 & 255; WRITE => 10 & 01
    dut._log.info("Read & write data")
    yield master.load_ir(MEMWR)
    yield master.shift_data(data01)
    assert(master.result == data0)
    yield master.shift_data(data10)
    assert(master.result == dataFF)

    # Disable t65, enable t80
    dut.t65_enable = 0
    dut.t80_enable = 1
    dut.reset_n = 0
    yield Timer(2*clk_period)
    dut.reset_n = 1

    # MEMADDR => 256
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 66 & CC
    dut._log.info("Read data")
    yield master.load_ir(MEMRD)
    yield master.shift_data(data0)
    assert(master.result == data01)
    yield master.shift_data(data0)
    assert(master.result == data10)

    # MEMADDR => 0
    dut._log.info("Setting address to 256")
    yield master.load_ir(MEMADDR)
    yield master.shift_data(addr)

    # READ => 66 & CC
    dut._log.info("Read data")
    yield master.load_ir(MEMRD)
    yield master.shift_data(data0)
    assert(master.result == data01)
    yield master.shift_data(data0)
    assert(master.result == data10)
