import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, Edge
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue
from cocotbext.wishbone.driver import WishboneMaster, WBOp

@cocotb.coroutine
def pullup(wire, drive, enable):
    while True:
        if str(enable) == '1':
            wire <= drive.value
        else:
            wire <= 1
        yield [Edge(enable), Edge(drive)]
        

@cocotb.test()
def test01_uart(dut):
    """
    Test the UART interface
    """

    # Simulate @ 10MHz
    clk_period = get_sim_steps(100, "ns")
    cocotb.fork(Clock(dut.sys_clk, clk_period).start())

    # Pullups
    cocotb.fork(pullup(dut.spi_port_cs_n_i, dut.spi_port_cs_n_o, dut.spi_port_cs_n_oe))
    cocotb.fork(pullup(dut.spi_port_clk_i, dut.spi_port_clk_o, dut.spi_port_clk_oe))
    cocotb.fork(pullup(dut.spi_port_miso_i, dut.spi_port_miso_o, dut.spi_port_miso_oe))
    cocotb.fork(pullup(dut.spi_port_mosi_i, dut.spi_port_mosi_o, dut.spi_port_mosi_oe))
    cocotb.fork(pullup(dut.i2c_port_scl_i, dut.i2c_port_scl_o, dut.i2c_port_scl_oe))
    cocotb.fork(pullup(dut.i2c_port_sda_i, dut.i2c_port_sda_o, dut.i2c_port_sda_oe))

    master = WishboneMaster(
        entity=dut, name="wb",
        clock=dut.sys_clk, signals={"datwr": "dat_w", "datrd": "dat_r"}
    )
    master2 = WishboneMaster(
        entity=dut, name="wb2",
        clock=dut.sys_clk, signals={"datwr": "dat_w", "datrd": "dat_r"}
    )

    # Init
    dut.uart_rx <= 1

    # Reset for 4 clock cycles and also shift a quarter of the clock
    # period in time
    dut.sys_rst <= 1
    yield Timer(int(4.25*clk_period/4))
    dut.sys_rst <= 0

    dut._log.info("dut.wb_ack: {}".format(dut.wb_ack))
    dut._log.info("master.bus.ack: {}".format(master.bus.ack))

    cocotb.fork(master.send_cycle([
        WBOp(0x9605, 0b11111111),
        WBOp(0x9601),
        WBOp(0x9600, 0b01010101),
        WBOp(0x9004, 2), # spi div
        WBOp(0x9005, 0), # spi offline
        WBOp(0x9001, 7), # spi length
        WBOp(0x9000, 0b10101010), # spi data
    ]))

    cocotb.fork(master2.send_cycle([
        WBOp(0x0001, 4),
        WBOp(0x0000, 0b0100000000000), # start
    ]))

    yield Timer(25*clk_period)

    cocotb.fork(master2.send_cycle([
        WBOp(0x0000, 0b0010001010101), # write
    ]))

    yield Timer(250*clk_period)

    cocotb.fork(master.send_cycle([
        WBOp(0x9005, 1), # spi offline
    ]))

    cocotb.fork(master2.send_cycle([
        WBOp(0x0000, 0b1000000000000), # stop
    ]))

    yield Timer(500*clk_period)
