import cocotb
from cocotb.triggers import Timer, Edge
from cocotb.binary import BinaryValue
from cocotb.utils import get_sim_steps

class UARTException(Exception):
    pass

class UART_RX(object):
    """
    Class that implements an UART receiver, it return the received byte
    """
    def __init__(self, tx):
        self.tx = tx
        # We keep the 10 last changes of the tx signal
        self.events = [None, None, None, None, None, None, None, None, None, (get_sim_steps(), int(tx))]
        cocotb.fork(self.watch_tx)

    @cocotb.coroutine
    def watch_tx(self):
        while True:
            yield Edge(self.tx)
            self.events.pop(0)
            self.events.append((get_sim_steps(), int(tx)))
        
    @cocotb.coroutine
    def receive(self, cycles_per_bit, bits=8, parity=False):
        # Check nothing is being sent at the moment receive is called
        assert(self.tx == 1)

        # We assume this is the start bit
        yield Edge(self.tx)
        start = get_sim_steps()

        # Wait till number of bits and half stop bit
        tot_bits = bits
        if parity:
            tot_bits += 1

        yield Timer(int((tot_bits+0.5)*cycles_per_bit))

        print(self.events)

