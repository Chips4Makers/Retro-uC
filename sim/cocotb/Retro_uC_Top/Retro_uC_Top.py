#!/bin/env python3
import sys, subprocess, os

from nmigen import Signal, Cat, Elaboratable, Module, ClockDomain, ClockSignal, ResetSignal
from nmigen.build import Platform
from nmigen.lib.io import Pin
from nmigen_boards.atlys import AtlysPlatform
from nmigen.back.verilog import convert

from c4m.nmigen.jtag import TAP, PmodJTAGResource, IOType

from retro_uc import Retro_uC

class DummyPlatform(Platform):
    resources = []
    connectors = []
    required_tools = ["yosys"]

    def toolchain_prepare(self, fragment, name, **kwargs):
        raise NotImplementedError

class Retro_uC_Top(Elaboratable):
    """The top level implementation for Retro-uC on the Atlys board"""
    def __init__(self):
        self.tap = TAP(ir_width=3)

        self.leds = Signal(8)

        # These signals are latched during reset and won't affect
        # operation of Retro-uC if changed when not in reset.
        self.en_m68k = Signal(reset=1)
        self.en_z80 = Signal(reset=1)
        self.en_mos6502 = Signal(reset=1)

    def elaborate(self, platform):
        m = Module()

        m.submodules.tap = self.tap

        n_leds = len(self.leds)

        #
        # JTAG interface
        #
        conns = [self.tap.add_io(iotype=IOType.Out) for _ in range(n_leds)]
        # Add Wishbone interface
        jtag_wb = self.tap.add_wishbone(
            ircodes=[3, 4, 5], address_width=14, data_width=32, granularity=8,
        )

        # Connect IO
        m.d.comb += self.leds.eq(Cat(conn.pad.o for conn in conns[:n_leds]))

        # CPU enable signals
        en = Signal(3)
        sr = self.tap.add_shiftreg(ircode=6, length=3)
        # use clock domain without reset, latch external on reset
        m.domains.encd = encd = ClockDomain(reset_less=True)
        m.d.comb += encd.clk.eq(ClockSignal())
        with m.If(ResetSignal()):
            m.d.encd += en.eq(Cat((self.en_m68k, self.en_mos6502, self.en_z80)))
        with m.Elif(sr.oe):
            m.d.encd += en.eq(sr.o)
        m.d.comb += sr.i.eq(en)

        #
        # Retro-uC
        #
        m.submodules.ruc = ruc = Retro_uC(
            [conn.core for conn in conns], ext_bus=jtag_wb,
            en_m68k=en[0], en_mos6502=en[1], en_z80=en[2]
        )

        return m


p = DummyPlatform()
ruc = Retro_uC_Top()

ports = [
    ruc.tap.bus.tck, ruc.tap.bus.tms, ruc.tap.bus.tdi, ruc.tap.bus.tdo, ruc.leds,
    ruc.en_m68k, ruc.en_z80, ruc.en_mos6502,
]

top_code = convert(ruc, name="Retro_uC_Top", ports=ports, platform=p)
with open("code/Retro_uC_Top.v", "w") as f:
    f.write(top_code)

for filename, code in p.extra_files.items():
    with open("code"+ os.path.sep + filename, "w") as f:
        f.write(code)


