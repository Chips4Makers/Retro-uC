import os, cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, PythonTrigger
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

from c4m.cocotb.jtag.c4m_jtag import JTAG_Master
from c4m.cocotb.jtag.c4m_jtag_svfcocotb import SVF_Executor

# This simulation assumes the cores are working well, it's mainly to test
# the proper working of the enabling/disabling of the different cores

# Global command values
cmd_ENABLE = BinaryValue("110", 3)
cmd_MEMADDRESS = BinaryValue("011")
cmd_MEMREAD = BinaryValue("100")
cmd_MEMREADWRITE = BinaryValue("101")


@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test00_init(dut):
    """
    Initialize inputs; small test always included at start of simulation
    """
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 0
    dut.tap_bus__tck <= 0
    dut.rst <= 1
    dut.clk <= 0
    yield Timer(1)


@cocotb.test()
def test01_resetrun_t80(dut):
    """
    For T80 test just reset and run with unitialized memory and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 1
    dut.en_mos6502 <= 0
    dut.tap_bus__tck <= 0
    dut.rst <= 1
    dut.clk <= 0
    yield clk_cycle(dut.clk, quartperiod, 3)

    dut.rst <= 0

    yield clk_cycle(dut.clk, quartperiod, 40)


@cocotb.test()
def test02_resetrun_t65(dut):
    """
    For T65 test just reset and run with unitialized memory and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 1
    dut.tap_bus__tck <= 0
    dut.rst <= 1
    dut.clk <= 0
    yield clk_cycle(dut.clk, quartperiod, 3)

    dut.rst <= 0

    yield clk_cycle(dut.clk, quartperiod, 40)


@cocotb.test()
def test03_resetrun_m68k(dut):
    """
    For M68K test just reset and run with unitialized memory and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.en_m68k <= 1
    dut.en_z80 <= 0
    dut.en_mos6502 <= 0
    dut.tap_bus__tck <= 0
    dut.rst <= 1
    dut.clk <= 0
    yield clk_cycle(dut.clk, quartperiod, 3)

    dut.rst <= 0

    yield clk_cycle(dut.clk, quartperiod, 40)


@cocotb.test()
def test04_JTAG_idcode(dut):
    """
    Test the IDCODE command
    """
    dut.rst <= 0

    # Run @ 1MHz
    clk_period = get_sim_steps(1, "us")
    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=clk_period, ir_width=3,
    )

    dut._log.info("Trying to get IDCODE...")

    yield master.idcode()
    result1 = master.result
    dut._log.info("IDCODE1: {}".format(result1))

    yield master.idcode()
    result2 = master.result
    dut._log.info("IDCODE2: {}".format(result2))

    assert(result1 == result2)


@cocotb.test()
def test05_JTAG_enable(dut):
    """
    Test enabling/disabling of T80/T65 through JTAG interface
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.clk, clk_period).start())
    # Run JTAG @ 0.625MHz
    jtag_period = get_sim_steps(1.6, "us")
    
    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtag_period, ir_width=3,
    )

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 1
    dut.tap_bus__tck <= 0
    dut.rst <= 1

    yield Timer(3*clk_period)

    dut.rst <= 0

    yield Timer(clk_period)

    M68KENABLE = BinaryValue("001", 3)
    T80ENABLE = BinaryValue("010", 3)
    T65ENABLE = BinaryValue("100", 3)
    ALLENABLE = BinaryValue("111", 3)

    # Enable M68K through JTAG
    yield master.load_ir(cmd_ENABLE)
    yield master.shift_data(M68KENABLE)

    yield Timer(10*clk_period)
    assert dut.ruc.m68k.m68k.reset_n.value == 1
    #assert dut.ruc.mos6502.t65_.t65_.t65_.Res_n.value == 0
    #assert dut.ruc.z80.t80_.t80.reset_n.value == 0

    # Enable T80 through JTAG
    yield master.shift_data(T80ENABLE)

    yield Timer(10*clk_period)
    assert dut.ruc.m68k.m68k.reset_n.value == 0

    # Enable T65 through JTAG
    yield master.shift_data(T65ENABLE)

    yield Timer(10*clk_period)
    assert dut.ruc.m68k.m68k.reset_n.value == 0

    # Enable all through JTAG
    yield master.shift_data(ALLENABLE)

    yield Timer(10*clk_period)
    assert dut.ruc.m68k.m68k.reset_n.value == 1

    # Go to RunTestIdle
    yield master.change_to_run()

    yield Timer(2*clk_period)


@cocotb.test()
def test06_JTAG_memmap(dut):
    """
    Test of an added Wishbone interface
    """
    dat_0 = BinaryValue(32*'0')
    dat_F = BinaryValue(32*'1')
    dat_A = BinaryValue(16*'10')
    dat_5 = BinaryValue(16*'01')

    adr_0 = BinaryValue(14*'0')
    adr_2 = BinaryValue(12*'0'+'10')

    # Run main chip @ 10MHz; need to be clocked for Wishbone interface to function
    cocotb.fork(Clock(dut.clk, 100, "ns").start())

    dut.en_m68k <= 1
    dut.en_mos6502 <= 1
    dut.en_z80 <= 1
    dut.rst <= 1

    yield Timer(650, "ns")

    dut.rst <= 0

    # Run JTAG @ 1MHz
    jtagclk_period = get_sim_steps(1, "us")
    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtagclk_period, ir_width=3,
    )

    # Helpers
    @cocotb.coroutine
    def shift_data(data_in, data_out=None):
        dut._log.info("  input: {}".format(data_in))
        yield master.shift_data(data_in)
        dut._log.info("  output: {}".format(master.result.binstr))
        if data_out is not None and master.result != data_out:
            dut._log.info("ERROR: Expected output {}".format(data_out.binstr))
            raise Exception("Unexpected output")

    # Load the memory address
    yield master.load_ir(cmd_MEMADDRESS)
    dut._log.info("Loading address")
    yield shift_data(adr_0)

    # Do write
    yield master.load_ir(cmd_MEMREADWRITE)
    dut._log.info("Writing memory")

    yield shift_data(dat_5)
    yield shift_data(dat_A)

    # Load the memory address
    yield master.load_ir(cmd_MEMADDRESS)
    dut._log.info("Loading address")
    yield shift_data(adr_0, adr_2)

    # Do read and write
    yield master.load_ir(cmd_MEMREADWRITE)
    dut._log.info("Reading and writing memory")
    yield shift_data(dat_A, dat_5)
    yield shift_data(dat_5, dat_A)

    # Load the memory address
    yield master.load_ir(cmd_MEMADDRESS)
    dut._log.info("Loading address")
    yield shift_data(adr_0, adr_2)

    # Do read
    yield master.load_ir(cmd_MEMREAD)
    dut._log.info("Reading memory")
    yield shift_data(dat_0, dat_A)
    yield shift_data(dat_0, dat_5)

    # Load the memory address
    yield master.load_ir(cmd_MEMADDRESS)
    dut._log.info("Loading address")
    yield shift_data(adr_0, adr_2)

    # Do read
    yield master.load_ir(cmd_MEMREAD)
    dut._log.info("Reading memory")
    yield shift_data(dat_0, dat_A)
    yield shift_data(dat_0, dat_5)


@cocotb.test()
def test07_JTAG_leds(dut):
    """
    Test of an added Wishbone interface
    """
    dat_0 = BinaryValue(32*'0')
    dat_F = BinaryValue(32*'1')
    dat_A = BinaryValue(16*'10')
    dat_5 = BinaryValue(16*'01')

    adr_IO = BinaryValue('10000000000000')

    # Run main chip @ 10MHz; need to be clocked for Wishbone interface to function
    cocotb.fork(Clock(dut.clk, 100, "ns").start())

    dut.en_m68k <= 1
    dut.en_mos6502 <= 1
    dut.en_z80 <= 1
    dut.rst <= 1

    yield Timer(650, "ns")

    dut.rst <= 0

    # Run JTAG @ 1MHz
    jtagclk_period = get_sim_steps(1, "us")
    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtagclk_period, ir_width=3,
    )

    # Helpers
    @cocotb.coroutine
    def shift_data(data_in, data_out=None):
        dut._log.info("  input: {}".format(data_in))
        yield master.shift_data(data_in)
        dut._log.info("  output: {}".format(master.result.binstr))
        if data_out is not None and master.result != data_out:
            dut._log.info("ERROR: Expected output {}".format(data_out.binstr))
            raise Exception("Unexpected output")

    # Load the memory address
    yield master.load_ir(cmd_MEMADDRESS)
    dut._log.info("Loading address")
    yield shift_data(adr_IO)

    # Write to the IO

    # Do write
    dut._log.info("Leds at start: {}".format(dut.leds.value.integer))
    yield master.load_ir(cmd_MEMREADWRITE)
    dut._log.info("Writing memory")
    yield shift_data(dat_F)
    dut._log.info("Leds after: {}".format(dut.leds.value.integer))
    assert dut.leds.value.integer == 255
    yield shift_data(dat_0)
    dut._log.info("Leds after: {}".format(dut.leds.value.integer))
    assert dut.leds.value.integer == 0


@cocotb.test()
def test08_SVF_blink_boundaryscan(dut):
    """
    Play back svf that tests the t80 demo SVF file
    """
    TOPDIR = os.environ["TOPDIR"]
    f = open(TOPDIR + "/boards/XLR8/demo/blink_boundaryscan.svf", "r")
    t = f.read()
    f.close()

    #Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.clk, clk_period).start())
    #Run JTAG @ 333KHZ
    jtag_period = get_sim_steps(3, "us")

    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtag_period, ir_width=3
    )

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 0
    dut.tap_bus__tck <= 0
    dut.rst <= 1

    yield Timer(3*clk_period)

    dut.rst <= 0

    yield Timer(clk_period)

    jtag_svf = SVF_Executor(master)
    yield jtag_svf.run(t, p=dut._log.info)


@cocotb.test()
def test09_SVF_loop_z80(dut):
    """
    Play back svf that tests the t80 demo SVF file
    """

    CURDIR = os.environ["CURDIR"]
    f = open(CURDIR + "/z80_loop.svf", "r")
    t = f.read()
    f.close()

    #Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.clk, clk_period).start())
    #Run JTAG @ 333KHZ
    jtag_period = get_sim_steps(3, "us")

    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtag_period, ir_width=3,
    )

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 1
    dut.tap_bus__tck <= 0
    dut.rst <= 1

    yield Timer(3*clk_period)

    dut.rst <= 0

    yield Timer(clk_period)

    jtag_svf = SVF_Executor(master)
    yield jtag_svf.run(t, p=dut._log.info)

    # run for 100 clock cycles
    yield Timer(100*clk_period)


@cocotb.test()
def test10_SVF_loop_mos6502(dut):
    """
    Play back svf that tests the t80 demo SVF file
    """

    CURDIR = os.environ["CURDIR"]
    f = open(CURDIR + "/mos6502_loop.svf", "r")
    t = f.read()
    f.close()

    #Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.clk, clk_period).start())
    #Run JTAG @ 333KHZ
    jtag_period = get_sim_steps(3, "us")

    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtag_period, ir_width=3,
    )

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 1
    dut.en_mos6502 <= 0
    dut.tap_bus__tck <= 0
    dut.rst <= 1

    yield Timer(3*clk_period)

    dut.rst <= 0

    yield Timer(clk_period)

    jtag_svf = SVF_Executor(master)
    yield jtag_svf.run(t, p=dut._log.info)

    # run for 100 clock cycles
    yield Timer(100*clk_period)


@cocotb.test()
def test11_SVF_loop_m68k(dut):
    """
    Play back svf that tests the t80 demo SVF file
    """
    CURDIR = os.environ["CURDIR"]
    f = open(CURDIR + "/m68k_loop.svf", "r")
    t = f.read()
    f.close()

    #Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    cocotb.fork(Clock(dut.clk, clk_period).start())
    #Run JTAG @ 333KHZ
    jtag_period = get_sim_steps(3, "us")

    master = JTAG_Master(
        dut.tap_bus__tck, dut.tap_bus__tms, dut.tap_bus__tdi, dut.tap_bus__tdo,
        clk_period=jtag_period, ir_width=3,
    )

    # Init/Reset
    dut.en_m68k <= 0
    dut.en_z80 <= 0
    dut.en_mos6502 <= 1
    dut.tap_bus__tck <= 0
    dut.rst <= 1

    yield Timer(3*clk_period)

    dut.rst <= 0

    yield Timer(clk_period)

    jtag_svf = SVF_Executor(master)
    yield jtag_svf.run(t, p=dut._log.info)

    # run for 100 clock cycles
    yield Timer(100*clk_period)
