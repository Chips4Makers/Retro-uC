import cocotb
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test01_resetrun(dut):
    """
    Test just reset and perform the HALT instruction and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.reset_n <= 0
    dut.clock <= 0
    dut.wb_dat_i <= int("EA", 16) # Always read the NOP instuction
    dut.wb_ack_i <= 0
    dut.wb_err_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 3)

    dut.reset_n <= 1

    for i in range(20):
        yield clk_cycle(dut.clock, quartperiod)
        if dut.wb_stb_o == 1:
            dut.wb_ack_i <= 1
        else:
            dut.wb_ack_i <= 0


@cocotb.test()
def test02_jmp(dut):
    """
    Test just reset and perform the HALT instruction and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.reset_n <= 0
    dut.clock <= 0
    dut.wb_ack_i <= 0
    dut.wb_err_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 1)

    dut.reset_n <= 1

    for i in range(30):
        oldaddr = int(dut.wb_adr_o)
        yield clk_cycle(dut.clock, quartperiod)
        if dut.wb_stb_o == 1:
            # Use $0100 as start address and in $0100 do JMP $0100
            if dut.wb_adr_o == int("FFFC", 16):
                assert oldaddr == dut.wb_adr_o or oldaddr == int("0100", 16) 
                dut.wb_dat_i <= int("00", 16)
            elif dut.wb_adr_o == int("FFFD", 16):
                assert oldaddr == dut.wb_adr_o or oldaddr == int("FFFC", 16) 
                dut.wb_dat_i <= int("01", 16)
            elif dut.wb_adr_o == int("0100", 16):
                assert oldaddr == dut.wb_adr_o or oldaddr == 0 or oldaddr == int("FFFD", 16) or oldaddr == int("0102", 16)
                dut.wb_dat_i <= int("4C", 16) # JMP
            elif dut.wb_adr_o == int("0101", 16):
                assert oldaddr == dut.wb_adr_o or oldaddr == int("0100", 16) 
                dut.wb_dat_i <= int("00", 16)
            elif dut.wb_adr_o == int("0102", 16):
                assert oldaddr == dut.wb_adr_o or oldaddr == int("0101", 16) 
                dut.wb_dat_i <= int("01", 16)
            else:
                dut.wb_dat_i <= int("EA", 16) # Always read the NOP instuction
            dut.wb_ack_i <= 1
        else:
            dut.wb_ack_i <= 0
