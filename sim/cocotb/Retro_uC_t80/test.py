import cocotb
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test01_resethalt(dut):
    """
    Test just reset and perform the HALT instruction and run for a few clock cycles
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.reset_n <= 0
    dut.clock <= 0
    dut.wb_dat_i <= int("76", 16) # HALT
    dut.wb_ack_i <= 0
    dut.wb_err_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 3)

    dut.reset_n <= 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 1
    assert dut.wb_stb_o == 1
    assert dut.cpu.halt_n == 1
    dut.wb_ack_i <= 1
 
    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1
    dut.wb_ack_i <= 0
 
    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0
    dut.wb_ack_i <= 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0
    dut.wb_ack_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 10)

    # Chips should trying to read from address 0 but waiting because we don't ack
    assert dut.wb_adr_o == 1
    assert dut.cpu.halt_n == 0
    assert dut.wb_we_o == 0


@cocotb.test()
def test02_resethalt_waitstate(dut):
    """
    Test just reset and perform the HALT instruction and run for a few clock cycles
    Do one wait state when reading the HALT instruction from address 0
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.reset_n <= 0
    dut.clock <= 0
    dut.wb_dat_i <= 0x76 # HALT
    dut.wb_ack_i <= 0
    dut.wb_err_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 2)

    dut.reset_n <= 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 1
    assert dut.wb_stb_o == 1
    assert dut.cpu.halt_n == 1
 
    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 1
    assert dut.wb_stb_o == 1
    assert dut.cpu.halt_n == 1
    dut.wb_ack_i <= 1
 
    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1
    dut.wb_ack_i <= 0
 
    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0
    dut.wb_ack_i <= 1

    yield clk_cycle(dut.clock, quartperiod)
    assert dut.wb_cyc_o == 0
    assert dut.wb_stb_o == 0
    assert dut.cpu.halt_n == 0
    dut.wb_ack_i <= 0

    yield clk_cycle(dut.clock, quartperiod, 10)

    # Chips should trying to read from address 0 but waiting because we don't ack
    assert dut.wb_adr_o == 1
    assert dut.cpu.halt_n == 0
    assert dut.wb_we_o == 0


@cocotb.test()
def test03_write(dut):
    """
    Test a short program writing to memory and then halt
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init/Reset
    dut.reset_n <= 0
    dut.clock <= 0
    dut.wb_ack_i <= 0
    dut.wb_err_i <= 0

    yield clk_cycle(dut.clock, quartperiod)

    dut.reset_n <= 1

    ram = [
        0xF3,             # DI
        0x3E, 0x45,       # LD A, #69
        0x32, 0x00, 0x80, # LD (0x8000),A
        0x00,             # NOP
        0x76              # HALT
    ]

    for i in range(25):
        yield clk_cycle(dut.clock, quartperiod)
        if dut.wb_stb_o == 1:
            dut.wb_ack_i <= 1

            if dut.wb_we_o == 0:
                # Read
                assert int(dut.wb_adr_o) < len(ram)
                dut.wb_dat_i <= ram[int(dut.wb_adr_o)]
            else:
                # Write
                assert dut.wb_adr_o == 0x8000
                assert dut.wb_dat_o == 69

            yield clk_cycle(dut.clock, quartperiod)
            dut.wb_ack_i <= 0
        else:
            dut.wb_ack_i <= 0

    assert dut.cpu.halt_n == 0
