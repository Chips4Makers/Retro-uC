-- instantiation of Retro_uC_WBBus for 35 IOs.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity WBBus_Sim is
  port (
    Clock:      in std_logic;
    RESET_N:    in std_logic;

    -- WB Bus can operate in 8 bit mode or 32 bit mode
    -- '0': 8-bit
    -- '1': 32-bit
    -- TODO: explain implication of 
    BusMode:    in std_logic;

    ---------------------------------------------------------------------------
    -- Wishbone bus
    ---------------------------------------------------------------------------
    -- The total address width is 16 bits; in 32 bit mode the upper two bits of
    -- the address are ignored.
    WB_Adr_i:   in std_logic_vector(15 downto 0);
    -- Data width is 32 bits; in 8 bit upper 24 bits are ignored.
    WB_Dat_i:   in std_logic_vector(31 downto 0);
    WB_Dat_o:   out std_logic_vector(31 downto 0);
    WB_Cyc_i:   in std_logic;
    -- 4 byte selection bits; in 8 bit mode these are ignored; in 32 bit mode
    -- the implementation should obey these selection bits.
    WB_Sel_i:   in std_logic_vector(3 downto 0);
    WB_Stb_i:   in std_logic;
    WB_WE_i:    in std_logic;
    -- Implementation should try to ack immediately and no stall if possible.
    WB_Ack_o:   out std_logic;
    WB_Stall_o: out std_logic;
    WB_Err_o:   out std_logic;
    
    ---------------------------------------------------------------------------
    -- Memory Map
    ---------------------------------------------------------------------------
    -- Everything is supposed to be memory mapped
    -- There is 64KB adress space; in 8 bit through 64K words, in 32 bit with
    -- 16K words.
    -- Map:
    -- + 32KB: memory
    -- + 4KB: IO handling
    -- + 4KB: Standard peripherals
    -- + 16KB: User/custom peripherals

    ---------------------------------------------------------------------------
    -- IO handling
    ---------------------------------------------------------------------------
    -- In 8 bit mode the IO handling start @ address 2^15; in 32 bit mode @
    -- address 2^13. Each of the next registers are put 128 bytes further in the
    -- memory map independent of the number of IOs.
    -- This first address corresponds with bits 7 downto 0 in 8 bit and 31
    -- downto 0 in 32 bit mode. In 32 bit WB_Sel_i(0) correspond with bit
    -- 7 downt 0.
    IOs_Out:    out std_logic_vector(34 downto 0);
    -- If possible should be put in high impedance state is not enabled.
    -- If not possible it should be documented by the implementation.
    IOs_En:     out std_logic_vector(34 downto 0)
  );
end entity WBBus_Sim;

architecture rtl of WBBus_Sim is
  constant SIM_IOS:     integer := 35;
  signal s_IOs_Out:     std_logic_vector(SIM_IOS-1 downto 0);
  signal s_IOs_En:     std_logic_vector(SIM_IOS-1 downto 0);
  signal s_IOs_In:      std_logic_vector(SIM_IOS-1 downto 0);
begin
  IOs_Out <= s_IOs_Out;
  IOs_En <= s_IOs_En;
  -- Instantiate the Wishbone bus Connect IOs_Out to IOs_In
  WBBus: entity work.Retro_uC_WBBus
    generic map (SIM_IOS)
    port map(
      Clock, RESET_N, BusMode,
      WB_Adr_i, WB_Dat_i, WB_Dat_o, WB_Cyc_i, WB_Sel_i, WB_Stb_i, WB_WE_i, WB_Ack_o, WB_Stall_o, WB_Err_o,
      s_IOs_Out, s_IOs_En, s_IOs_In
    );

  Generate_IOs_In: for IOit in 0 to SIM_IOS-1 generate
  begin
    s_IOs_In(IOit) <= s_IOs_Out(IOit) when s_IOs_En(IOit) = '1' else 'X';
  end generate;
end architecture rtl;

  
