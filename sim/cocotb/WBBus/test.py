import cocotb
from cocotb.triggers import Timer
from cocotb.utils import get_sim_steps
from cocotb.binary import BinaryValue

@cocotb.coroutine
def clk_cycle(clk, quartperiod, cycles=1):
    for i in range(cycles):
        clk <= 0
        yield quartperiod
        clk <= 1
        yield quartperiod
        yield quartperiod
        clk <= 0
        yield quartperiod


@cocotb.test()
def test01_memory(dut):
    """
    Test the memory part of the Retro-uC Wishbone Bus
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init
    dut.wb_sel_i <= 0
    dut.clock <= 0
    dut.wb_dat_i <= BinaryValue("00011111000011110000011100000011")

    # Reset
    dut._log.info("Checking Reset")
    dut.reset_n <= 0
    yield quartperiod
    dut.reset_n <= 1

    # Don't do anything if stb_i == 0
    dut._log.info("Checking stb_i = 0")
    dut.wb_stb_i <= 0
    dut.wb_cyc_i <= 0
    dut.wb_adr_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    # From now on select the bus
    dut.wb_stb_i <= 1

    # Write to address 0 in 8 bit mode
    dut._log.info("Writing to address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 0
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to address 1 in 8 bit mode
    dut._log.info("Writing to address 1 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 1
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to address 2 in 8 bit mode
    dut._log.info("Writing to address 2 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 2
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to address 3 in 8 bit mode
    dut._log.info("Writing to address 3 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 3
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to address 1 in 32 bit mode
    dut._log.info("Writing to address 1 in 32 bit mode")
    dut.busmode <= 1
    dut.wb_adr_i <= 1
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Read from address 0 in 8 bit mode
    dut._log.info("Reading from address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 0
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    yield quartperiod
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    yield quartperiod
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

    # Read from address 1 in 8 bit mode
    dut._log.info("Reading from address 1 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 1
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

    # Read from address 2 in 8 bit mode
    dut._log.info("Reading from address 2 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 2
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

    # Read from address 3 in 8 bit mode
    dut._log.info("Reading from address 3 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 3
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

    # Read from address 7 in 8 bit mode
    dut._log.info("Reading from address 7 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= 7
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

    # Reading from address 1 in 32 bit mode
    dut._log.info("Reading from address 1 in 32 bit mode")
    dut.busmode <= 1
    dut.wb_adr_i <= 1
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert dut.wb_dat_o == BinaryValue("00011111000011110000011100000011")
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert dut.wb_dat_o == BinaryValue("00011111000011110000011100000011")
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0

@cocotb.test()
def test02_ios(dut):
    """
    Test the IO part of the Retro-uC Wishbone bus
    """

    # Simulate @ 1MHz
    clk_period = get_sim_steps(1, "us")
    quartperiod = Timer(clk_period/4)

    # Init
    dut.wb_sel_i <= 0
    dut.clock <= 0
    dut.wb_stb_i <= 1

    # Reset
    dut._log.info("Checking Reset")
    dut.reset_n <= 0
    yield quartperiod
    assert dut.ios_en == 0
    assert str(dut.ios_out) == "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    dut.reset_n <= 1

    # Write to IO out at address 0 in 8 bit mode
    dut._log.info("Writing to IO at address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= BinaryValue("1000000000000000")
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    dut.wb_dat_i <= BinaryValue("XXXXXXXXXXXXXXXXXXXXXXXX00000011")
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_out) == "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    assert dut.ios_en == 0
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_out) == "XXXXXXXXXXXXXXXXXXXXXXXXXXX00000011"
    assert dut.ios_en == 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_out) == "XXXXXXXXXXXXXXXXXXXXXXXXXXX00000011"
    assert dut.ios_en == 0
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_out) == "XXXXXXXXXXXXXXXXXXXXXXXXXXX00000011"
    assert dut.ios_en == 0

    # Read from IO in at adress 0 in 8 bit mode, should yield all X as IOs not enabled
    dut._log.info("Reading from IO in at address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= BinaryValue("1000000100000000")
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    dut.wb_cyc_i <= 0
    dut.clock <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to IO en at address 0 in 8 bit mode
    dut._log.info("Writing to IO at address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= BinaryValue("1000000010000000")
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 1
    dut.wb_dat_i <= BinaryValue("XXXXXXXXXXXXXXXXXXXXXXXX11111111")
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "00000000000000000000000000000000000"
    assert dut.ios_en == 0
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "00000000000000000000000000011111111"
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_en) == "00000000000000000000000000011111111"
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_en) == "00000000000000000000000000011111111"

    # Read from IO in at adress 0 in 8 bit mode
    dut._log.info("Writing to IO at address 0 in 8 bit mode")
    dut.busmode <= 0
    dut.wb_adr_i <= BinaryValue("1000000100000000")
    dut.wb_cyc_i <= 1
    dut.wb_sel_i <= BinaryValue("1111")
    dut.wb_we_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 0
    dut.wb_cyc_i <= 0
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.wb_dat_o) == "XXXXXXXXXXXXXXXXXXXXXXXX00000011"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 0
    dut.clock <= 0
    yield quartperiod

    # Write to IO en at address 0 & 1 in 32 bit mode
    dut._log.info("Writing to IO en at address 0 & 1 in 32 bit mode")
    dut.busmode <= 1
    dut.wb_adr_i <= BinaryValue("0010000000100000")
    dut.wb_cyc_i <= 1
    dut.wb_we_i <= 1
    dut.wb_dat_i <= BinaryValue("11111111111111111111111111111111")
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "00000000000000000000000000011111111"
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "00011111111111111111111111111111111"
    dut.clock <= 0
    yield quartperiod
    assert str(dut.ios_en) == "00011111111111111111111111111111111"
    dut.wb_adr_i <= BinaryValue("0010000000100001")
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "00011111111111111111111111111111111"
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_en) == "11111111111111111111111111111111111"
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_en) == "11111111111111111111111111111111111"
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_en) == "11111111111111111111111111111111111"

    # Write to IO out at address 0 & 1 in 32 bit mode
    dut._log.info("Writing to IO out at address 0 & 1 in 32 bit mode")
    dut.busmode <= 1
    dut.wb_adr_i <= BinaryValue("0010000000000000")
    dut.wb_cyc_i <= 1
    dut.wb_we_i <= 1
    dut.wb_dat_i <= BinaryValue("00011111000011110000011100000011")
    yield quartperiod
    assert dut.wb_ack_o == 1
    dut.clock <= 1
    yield quartperiod
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_out) == "XXX00000011000001110000111100011111"
    dut.clock <= 0
    yield quartperiod
    assert str(dut.ios_out) == "XXX00000011000001110000111100011111"
    dut.wb_adr_i <= BinaryValue("0010000000000001")
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_out) == "XXX00000011000001110000111100011111"
    dut.clock <= 1
    yield quartperiod
    assert dut.wb_ack_o == 1
    assert str(dut.ios_out) == "11100000011000001110000111100011111"
    dut.wb_cyc_i <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_out) == "11100000011000001110000111100011111"
    dut.clock <= 0
    yield quartperiod
    assert dut.wb_ack_o == 0
    assert str(dut.ios_out) == "11100000011000001110000111100011111"
