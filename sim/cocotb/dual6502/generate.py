#!/bin/env python3
from nmigen import *

from c4m_repo.nmigen.lib import *
from c4m_repo.nmigen.t65 import MOS6502_WB

# Set default memory block behavior
Memory.simulate = False
Memory.reset_less = True

class Top(Elaboratable):
    "Test for dual MOS6502 chips in a design."
    def __init__(self):
        self.en = Signal(2, reset=1) # Enable

    def elaborate(self, platform):
        m = Module()

        m.submodules.mos6502_a = mos6502_a = MOS6502_WB()
        m.submodules.mos6502_b = mos6502_b = MOS6502_WB()

        en_a = Signal(reset=1)
        en_b = Signal(reset=0)

        m.submodules.mem_a = mem_a = WishboneMemory(width=8, depth=4*1024)
        m.submodules.mem_b = mem_b = WishboneMemory(width=8, depth=4*1024)

        m.submodules.ext_a = ext_a = WishboneExtend(mem_a.wishbone, 16, mirror=True)
        m.submodules.ext_b = ext_b = WishboneExtend(mem_b.wishbone, 16, mirror=True)

        conn_a = ext_a.wishbone.connect(mos6502_a.wishbone)
        conn_b = ext_b.wishbone.connect(mos6502_b.wishbone)

        m.d.comb += [
            mos6502_a.ce.eq(en_a),
            mos6502_b.ce.eq(en_b),
            *conn_a,
            *conn_b,
        ]

        return m


t = Top()
p = RTLPlatform()

# from nmigen.back.rtlil import convert
# f = open("source/top.il", "w")
# f.write(convert(t, platform=p))
# f.close()

p.build(t, build_dir="source")

